﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SQLite4Unity3d.TableQuery`1<System.Object>
struct TableQuery_1_t4026329353;
// SQLite4Unity3d.SQLiteConnection
struct SQLiteConnection_t3529499386;
// SQLite4Unity3d.TableMapping
struct TableMapping_t3898710812;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Linq.Expressions.Expression`1<System.Func`2<System.Object,System.Boolean>>
struct Expression_1_t4002038744;
// System.Object
struct Il2CppObject;
// System.Linq.Expressions.Expression
struct Expression_t114864668;
// SQLite4Unity3d.SQLiteCommand
struct SQLiteCommand_t2935685145;
// System.String
struct String_t;
// SQLite4Unity3d.TableQuery`1/CompileResult<System.Object>
struct CompileResult_t4163790932;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t2058570427;
// System.Type
struct Type_t;
// System.Linq.Expressions.BinaryExpression
struct BinaryExpression_t2159924157;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t164973122;
// SQLite4Unity3d.BaseTableQuery/Ordering
struct Ordering_t1038862794;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_SQLite4Unity3d_SQLiteConnection3529499386.h"
#include "AssemblyU2DCSharp_SQLite4Unity3d_TableMapping3898710812.h"
#include "System_Core_System_Linq_Expressions_Expression114864668.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Type1303803226.h"
#include "System_Core_System_Linq_Expressions_BinaryExpressi2159924157.h"
#include "AssemblyU2DCSharp_SQLite4Unity3d_BaseTableQuery_Or1038862794.h"

// System.Void SQLite4Unity3d.TableQuery`1<System.Object>::.ctor(SQLite4Unity3d.SQLiteConnection,SQLite4Unity3d.TableMapping)
extern "C"  void TableQuery_1__ctor_m2525682756_gshared (TableQuery_1_t4026329353 * __this, SQLiteConnection_t3529499386 * ___conn0, TableMapping_t3898710812 * ___table1, const MethodInfo* method);
#define TableQuery_1__ctor_m2525682756(__this, ___conn0, ___table1, method) ((  void (*) (TableQuery_1_t4026329353 *, SQLiteConnection_t3529499386 *, TableMapping_t3898710812 *, const MethodInfo*))TableQuery_1__ctor_m2525682756_gshared)(__this, ___conn0, ___table1, method)
// System.Void SQLite4Unity3d.TableQuery`1<System.Object>::.ctor(SQLite4Unity3d.SQLiteConnection)
extern "C"  void TableQuery_1__ctor_m821176910_gshared (TableQuery_1_t4026329353 * __this, SQLiteConnection_t3529499386 * ___conn0, const MethodInfo* method);
#define TableQuery_1__ctor_m821176910(__this, ___conn0, method) ((  void (*) (TableQuery_1_t4026329353 *, SQLiteConnection_t3529499386 *, const MethodInfo*))TableQuery_1__ctor_m821176910_gshared)(__this, ___conn0, method)
// System.Collections.IEnumerator SQLite4Unity3d.TableQuery`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * TableQuery_1_System_Collections_IEnumerable_GetEnumerator_m2306269163_gshared (TableQuery_1_t4026329353 * __this, const MethodInfo* method);
#define TableQuery_1_System_Collections_IEnumerable_GetEnumerator_m2306269163(__this, method) ((  Il2CppObject * (*) (TableQuery_1_t4026329353 *, const MethodInfo*))TableQuery_1_System_Collections_IEnumerable_GetEnumerator_m2306269163_gshared)(__this, method)
// SQLite4Unity3d.SQLiteConnection SQLite4Unity3d.TableQuery`1<System.Object>::get_Connection()
extern "C"  SQLiteConnection_t3529499386 * TableQuery_1_get_Connection_m2018438234_gshared (TableQuery_1_t4026329353 * __this, const MethodInfo* method);
#define TableQuery_1_get_Connection_m2018438234(__this, method) ((  SQLiteConnection_t3529499386 * (*) (TableQuery_1_t4026329353 *, const MethodInfo*))TableQuery_1_get_Connection_m2018438234_gshared)(__this, method)
// System.Void SQLite4Unity3d.TableQuery`1<System.Object>::set_Connection(SQLite4Unity3d.SQLiteConnection)
extern "C"  void TableQuery_1_set_Connection_m3046933261_gshared (TableQuery_1_t4026329353 * __this, SQLiteConnection_t3529499386 * ___value0, const MethodInfo* method);
#define TableQuery_1_set_Connection_m3046933261(__this, ___value0, method) ((  void (*) (TableQuery_1_t4026329353 *, SQLiteConnection_t3529499386 *, const MethodInfo*))TableQuery_1_set_Connection_m3046933261_gshared)(__this, ___value0, method)
// SQLite4Unity3d.TableMapping SQLite4Unity3d.TableQuery`1<System.Object>::get_Table()
extern "C"  TableMapping_t3898710812 * TableQuery_1_get_Table_m2172867230_gshared (TableQuery_1_t4026329353 * __this, const MethodInfo* method);
#define TableQuery_1_get_Table_m2172867230(__this, method) ((  TableMapping_t3898710812 * (*) (TableQuery_1_t4026329353 *, const MethodInfo*))TableQuery_1_get_Table_m2172867230_gshared)(__this, method)
// System.Void SQLite4Unity3d.TableQuery`1<System.Object>::set_Table(SQLite4Unity3d.TableMapping)
extern "C"  void TableQuery_1_set_Table_m3580780791_gshared (TableQuery_1_t4026329353 * __this, TableMapping_t3898710812 * ___value0, const MethodInfo* method);
#define TableQuery_1_set_Table_m3580780791(__this, ___value0, method) ((  void (*) (TableQuery_1_t4026329353 *, TableMapping_t3898710812 *, const MethodInfo*))TableQuery_1_set_Table_m3580780791_gshared)(__this, ___value0, method)
// SQLite4Unity3d.TableQuery`1<T> SQLite4Unity3d.TableQuery`1<System.Object>::Where(System.Linq.Expressions.Expression`1<System.Func`2<T,System.Boolean>>)
extern "C"  TableQuery_1_t4026329353 * TableQuery_1_Where_m4178518972_gshared (TableQuery_1_t4026329353 * __this, Expression_1_t4002038744 * ___predExpr0, const MethodInfo* method);
#define TableQuery_1_Where_m4178518972(__this, ___predExpr0, method) ((  TableQuery_1_t4026329353 * (*) (TableQuery_1_t4026329353 *, Expression_1_t4002038744 *, const MethodInfo*))TableQuery_1_Where_m4178518972_gshared)(__this, ___predExpr0, method)
// SQLite4Unity3d.TableQuery`1<T> SQLite4Unity3d.TableQuery`1<System.Object>::Take(System.Int32)
extern "C"  TableQuery_1_t4026329353 * TableQuery_1_Take_m2215285898_gshared (TableQuery_1_t4026329353 * __this, int32_t ___n0, const MethodInfo* method);
#define TableQuery_1_Take_m2215285898(__this, ___n0, method) ((  TableQuery_1_t4026329353 * (*) (TableQuery_1_t4026329353 *, int32_t, const MethodInfo*))TableQuery_1_Take_m2215285898_gshared)(__this, ___n0, method)
// SQLite4Unity3d.TableQuery`1<T> SQLite4Unity3d.TableQuery`1<System.Object>::Skip(System.Int32)
extern "C"  TableQuery_1_t4026329353 * TableQuery_1_Skip_m2335687758_gshared (TableQuery_1_t4026329353 * __this, int32_t ___n0, const MethodInfo* method);
#define TableQuery_1_Skip_m2335687758(__this, ___n0, method) ((  TableQuery_1_t4026329353 * (*) (TableQuery_1_t4026329353 *, int32_t, const MethodInfo*))TableQuery_1_Skip_m2335687758_gshared)(__this, ___n0, method)
// T SQLite4Unity3d.TableQuery`1<System.Object>::ElementAt(System.Int32)
extern "C"  Il2CppObject * TableQuery_1_ElementAt_m4176579987_gshared (TableQuery_1_t4026329353 * __this, int32_t ___index0, const MethodInfo* method);
#define TableQuery_1_ElementAt_m4176579987(__this, ___index0, method) ((  Il2CppObject * (*) (TableQuery_1_t4026329353 *, int32_t, const MethodInfo*))TableQuery_1_ElementAt_m4176579987_gshared)(__this, ___index0, method)
// SQLite4Unity3d.TableQuery`1<T> SQLite4Unity3d.TableQuery`1<System.Object>::Deferred()
extern "C"  TableQuery_1_t4026329353 * TableQuery_1_Deferred_m2927110549_gshared (TableQuery_1_t4026329353 * __this, const MethodInfo* method);
#define TableQuery_1_Deferred_m2927110549(__this, method) ((  TableQuery_1_t4026329353 * (*) (TableQuery_1_t4026329353 *, const MethodInfo*))TableQuery_1_Deferred_m2927110549_gshared)(__this, method)
// System.Void SQLite4Unity3d.TableQuery`1<System.Object>::AddWhere(System.Linq.Expressions.Expression)
extern "C"  void TableQuery_1_AddWhere_m752477770_gshared (TableQuery_1_t4026329353 * __this, Expression_t114864668 * ___pred0, const MethodInfo* method);
#define TableQuery_1_AddWhere_m752477770(__this, ___pred0, method) ((  void (*) (TableQuery_1_t4026329353 *, Expression_t114864668 *, const MethodInfo*))TableQuery_1_AddWhere_m752477770_gshared)(__this, ___pred0, method)
// SQLite4Unity3d.SQLiteCommand SQLite4Unity3d.TableQuery`1<System.Object>::GenerateCommand(System.String)
extern "C"  SQLiteCommand_t2935685145 * TableQuery_1_GenerateCommand_m4193037006_gshared (TableQuery_1_t4026329353 * __this, String_t* ___selectionList0, const MethodInfo* method);
#define TableQuery_1_GenerateCommand_m4193037006(__this, ___selectionList0, method) ((  SQLiteCommand_t2935685145 * (*) (TableQuery_1_t4026329353 *, String_t*, const MethodInfo*))TableQuery_1_GenerateCommand_m4193037006_gshared)(__this, ___selectionList0, method)
// SQLite4Unity3d.TableQuery`1/CompileResult<T> SQLite4Unity3d.TableQuery`1<System.Object>::CompileExpr(System.Linq.Expressions.Expression,System.Collections.Generic.List`1<System.Object>)
extern "C"  CompileResult_t4163790932 * TableQuery_1_CompileExpr_m3818611973_gshared (TableQuery_1_t4026329353 * __this, Expression_t114864668 * ___expr0, List_1_t2058570427 * ___queryArgs1, const MethodInfo* method);
#define TableQuery_1_CompileExpr_m3818611973(__this, ___expr0, ___queryArgs1, method) ((  CompileResult_t4163790932 * (*) (TableQuery_1_t4026329353 *, Expression_t114864668 *, List_1_t2058570427 *, const MethodInfo*))TableQuery_1_CompileExpr_m3818611973_gshared)(__this, ___expr0, ___queryArgs1, method)
// System.Object SQLite4Unity3d.TableQuery`1<System.Object>::ConvertTo(System.Object,System.Type)
extern "C"  Il2CppObject * TableQuery_1_ConvertTo_m2602095610_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___obj0, Type_t * ___t1, const MethodInfo* method);
#define TableQuery_1_ConvertTo_m2602095610(__this /* static, unused */, ___obj0, ___t1, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, Type_t *, const MethodInfo*))TableQuery_1_ConvertTo_m2602095610_gshared)(__this /* static, unused */, ___obj0, ___t1, method)
// System.String SQLite4Unity3d.TableQuery`1<System.Object>::CompileNullBinaryExpression(System.Linq.Expressions.BinaryExpression,SQLite4Unity3d.TableQuery`1/CompileResult<T>)
extern "C"  String_t* TableQuery_1_CompileNullBinaryExpression_m4251067313_gshared (TableQuery_1_t4026329353 * __this, BinaryExpression_t2159924157 * ___expression0, CompileResult_t4163790932 * ___parameter1, const MethodInfo* method);
#define TableQuery_1_CompileNullBinaryExpression_m4251067313(__this, ___expression0, ___parameter1, method) ((  String_t* (*) (TableQuery_1_t4026329353 *, BinaryExpression_t2159924157 *, CompileResult_t4163790932 *, const MethodInfo*))TableQuery_1_CompileNullBinaryExpression_m4251067313_gshared)(__this, ___expression0, ___parameter1, method)
// System.String SQLite4Unity3d.TableQuery`1<System.Object>::GetSqlName(System.Linq.Expressions.Expression)
extern "C"  String_t* TableQuery_1_GetSqlName_m1128366698_gshared (TableQuery_1_t4026329353 * __this, Expression_t114864668 * ___expr0, const MethodInfo* method);
#define TableQuery_1_GetSqlName_m1128366698(__this, ___expr0, method) ((  String_t* (*) (TableQuery_1_t4026329353 *, Expression_t114864668 *, const MethodInfo*))TableQuery_1_GetSqlName_m1128366698_gshared)(__this, ___expr0, method)
// System.Int32 SQLite4Unity3d.TableQuery`1<System.Object>::Count()
extern "C"  int32_t TableQuery_1_Count_m3413050791_gshared (TableQuery_1_t4026329353 * __this, const MethodInfo* method);
#define TableQuery_1_Count_m3413050791(__this, method) ((  int32_t (*) (TableQuery_1_t4026329353 *, const MethodInfo*))TableQuery_1_Count_m3413050791_gshared)(__this, method)
// System.Int32 SQLite4Unity3d.TableQuery`1<System.Object>::Count(System.Linq.Expressions.Expression`1<System.Func`2<T,System.Boolean>>)
extern "C"  int32_t TableQuery_1_Count_m3405720028_gshared (TableQuery_1_t4026329353 * __this, Expression_1_t4002038744 * ___predExpr0, const MethodInfo* method);
#define TableQuery_1_Count_m3405720028(__this, ___predExpr0, method) ((  int32_t (*) (TableQuery_1_t4026329353 *, Expression_1_t4002038744 *, const MethodInfo*))TableQuery_1_Count_m3405720028_gshared)(__this, ___predExpr0, method)
// System.Collections.Generic.IEnumerator`1<T> SQLite4Unity3d.TableQuery`1<System.Object>::GetEnumerator()
extern "C"  Il2CppObject* TableQuery_1_GetEnumerator_m710809644_gshared (TableQuery_1_t4026329353 * __this, const MethodInfo* method);
#define TableQuery_1_GetEnumerator_m710809644(__this, method) ((  Il2CppObject* (*) (TableQuery_1_t4026329353 *, const MethodInfo*))TableQuery_1_GetEnumerator_m710809644_gshared)(__this, method)
// T SQLite4Unity3d.TableQuery`1<System.Object>::First()
extern "C"  Il2CppObject * TableQuery_1_First_m3018168943_gshared (TableQuery_1_t4026329353 * __this, const MethodInfo* method);
#define TableQuery_1_First_m3018168943(__this, method) ((  Il2CppObject * (*) (TableQuery_1_t4026329353 *, const MethodInfo*))TableQuery_1_First_m3018168943_gshared)(__this, method)
// T SQLite4Unity3d.TableQuery`1<System.Object>::FirstOrDefault()
extern "C"  Il2CppObject * TableQuery_1_FirstOrDefault_m3828705525_gshared (TableQuery_1_t4026329353 * __this, const MethodInfo* method);
#define TableQuery_1_FirstOrDefault_m3828705525(__this, method) ((  Il2CppObject * (*) (TableQuery_1_t4026329353 *, const MethodInfo*))TableQuery_1_FirstOrDefault_m3828705525_gshared)(__this, method)
// System.String SQLite4Unity3d.TableQuery`1<System.Object>::<GenerateCommand>m__11(SQLite4Unity3d.BaseTableQuery/Ordering)
extern "C"  String_t* TableQuery_1_U3CGenerateCommandU3Em__11_m1866588696_gshared (Il2CppObject * __this /* static, unused */, Ordering_t1038862794 * ___o0, const MethodInfo* method);
#define TableQuery_1_U3CGenerateCommandU3Em__11_m1866588696(__this /* static, unused */, ___o0, method) ((  String_t* (*) (Il2CppObject * /* static, unused */, Ordering_t1038862794 *, const MethodInfo*))TableQuery_1_U3CGenerateCommandU3Em__11_m1866588696_gshared)(__this /* static, unused */, ___o0, method)
// System.String SQLite4Unity3d.TableQuery`1<System.Object>::<CompileExpr>m__12(SQLite4Unity3d.TableQuery`1/CompileResult<T>)
extern "C"  String_t* TableQuery_1_U3CCompileExprU3Em__12_m1764005629_gshared (Il2CppObject * __this /* static, unused */, CompileResult_t4163790932 * ___a0, const MethodInfo* method);
#define TableQuery_1_U3CCompileExprU3Em__12_m1764005629(__this /* static, unused */, ___a0, method) ((  String_t* (*) (Il2CppObject * /* static, unused */, CompileResult_t4163790932 *, const MethodInfo*))TableQuery_1_U3CCompileExprU3Em__12_m1764005629_gshared)(__this /* static, unused */, ___a0, method)
