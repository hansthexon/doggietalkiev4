﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GameManager1/<ploiteloadingscreen>c__IteratorC
struct U3CploiteloadingscreenU3Ec__IteratorC_t1837974204;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void GameManager1/<ploiteloadingscreen>c__IteratorC::.ctor()
extern "C"  void U3CploiteloadingscreenU3Ec__IteratorC__ctor_m817374533 (U3CploiteloadingscreenU3Ec__IteratorC_t1837974204 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object GameManager1/<ploiteloadingscreen>c__IteratorC::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CploiteloadingscreenU3Ec__IteratorC_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m909018463 (U3CploiteloadingscreenU3Ec__IteratorC_t1837974204 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object GameManager1/<ploiteloadingscreen>c__IteratorC::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CploiteloadingscreenU3Ec__IteratorC_System_Collections_IEnumerator_get_Current_m760594519 (U3CploiteloadingscreenU3Ec__IteratorC_t1837974204 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GameManager1/<ploiteloadingscreen>c__IteratorC::MoveNext()
extern "C"  bool U3CploiteloadingscreenU3Ec__IteratorC_MoveNext_m1933122687 (U3CploiteloadingscreenU3Ec__IteratorC_t1837974204 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameManager1/<ploiteloadingscreen>c__IteratorC::Dispose()
extern "C"  void U3CploiteloadingscreenU3Ec__IteratorC_Dispose_m2016172782 (U3CploiteloadingscreenU3Ec__IteratorC_t1837974204 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameManager1/<ploiteloadingscreen>c__IteratorC::Reset()
extern "C"  void U3CploiteloadingscreenU3Ec__IteratorC_Reset_m3394677172 (U3CploiteloadingscreenU3Ec__IteratorC_t1837974204 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
