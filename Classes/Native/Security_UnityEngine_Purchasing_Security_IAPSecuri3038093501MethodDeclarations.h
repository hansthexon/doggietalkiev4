﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Purchasing.Security.IAPSecurityException
struct IAPSecurityException_t3038093501;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void UnityEngine.Purchasing.Security.IAPSecurityException::.ctor()
extern "C"  void IAPSecurityException__ctor_m4130362627 (IAPSecurityException_t3038093501 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Purchasing.Security.IAPSecurityException::.ctor(System.String)
extern "C"  void IAPSecurityException__ctor_m1626861665 (IAPSecurityException_t3038093501 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
