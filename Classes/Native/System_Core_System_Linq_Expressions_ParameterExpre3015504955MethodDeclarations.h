﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Linq.Expressions.ParameterExpression
struct ParameterExpression_t3015504955;
// System.Type
struct Type_t;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Type1303803226.h"
#include "mscorlib_System_String2029220233.h"

// System.Void System.Linq.Expressions.ParameterExpression::.ctor(System.Type,System.String)
extern "C"  void ParameterExpression__ctor_m270931457 (ParameterExpression_t3015504955 * __this, Type_t * ___type0, String_t* ___name1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Linq.Expressions.ParameterExpression::get_Name()
extern "C"  String_t* ParameterExpression_get_Name_m934260431 (ParameterExpression_t3015504955 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
