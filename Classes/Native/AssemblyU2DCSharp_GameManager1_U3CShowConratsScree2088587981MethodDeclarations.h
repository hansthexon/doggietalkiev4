﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GameManager1/<ShowConratsScreen>c__IteratorB
struct U3CShowConratsScreenU3Ec__IteratorB_t2088587981;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void GameManager1/<ShowConratsScreen>c__IteratorB::.ctor()
extern "C"  void U3CShowConratsScreenU3Ec__IteratorB__ctor_m1705590664 (U3CShowConratsScreenU3Ec__IteratorB_t2088587981 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object GameManager1/<ShowConratsScreen>c__IteratorB::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CShowConratsScreenU3Ec__IteratorB_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3349493248 (U3CShowConratsScreenU3Ec__IteratorB_t2088587981 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object GameManager1/<ShowConratsScreen>c__IteratorB::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CShowConratsScreenU3Ec__IteratorB_System_Collections_IEnumerator_get_Current_m412239880 (U3CShowConratsScreenU3Ec__IteratorB_t2088587981 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GameManager1/<ShowConratsScreen>c__IteratorB::MoveNext()
extern "C"  bool U3CShowConratsScreenU3Ec__IteratorB_MoveNext_m3325536164 (U3CShowConratsScreenU3Ec__IteratorB_t2088587981 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameManager1/<ShowConratsScreen>c__IteratorB::Dispose()
extern "C"  void U3CShowConratsScreenU3Ec__IteratorB_Dispose_m842315647 (U3CShowConratsScreenU3Ec__IteratorB_t2088587981 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameManager1/<ShowConratsScreen>c__IteratorB::Reset()
extern "C"  void U3CShowConratsScreenU3Ec__IteratorB_Reset_m874994601 (U3CShowConratsScreenU3Ec__IteratorB_t2088587981 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
