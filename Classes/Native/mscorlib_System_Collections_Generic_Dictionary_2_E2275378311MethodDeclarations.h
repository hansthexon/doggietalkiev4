﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>
struct Dictionary_2_t955353609;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E2275378311.h"
#include "mscorlib_System_Collections_DictionaryEntry3048875398.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23007666127.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl1947527974.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m725289720_gshared (Enumerator_t2275378311 * __this, Dictionary_2_t955353609 * ___dictionary0, const MethodInfo* method);
#define Enumerator__ctor_m725289720(__this, ___dictionary0, method) ((  void (*) (Enumerator_t2275378311 *, Dictionary_2_t955353609 *, const MethodInfo*))Enumerator__ctor_m725289720_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2736581469_gshared (Enumerator_t2275378311 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m2736581469(__this, method) ((  Il2CppObject * (*) (Enumerator_t2275378311 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2736581469_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3226745029_gshared (Enumerator_t2275378311 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m3226745029(__this, method) ((  void (*) (Enumerator_t2275378311 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3226745029_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C"  DictionaryEntry_t3048875398  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1706763446_gshared (Enumerator_t2275378311 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1706763446(__this, method) ((  DictionaryEntry_t3048875398  (*) (Enumerator_t2275378311 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1706763446_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m691547087_gshared (Enumerator_t2275378311 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m691547087(__this, method) ((  Il2CppObject * (*) (Enumerator_t2275378311 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m691547087_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2696031375_gshared (Enumerator_t2275378311 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2696031375(__this, method) ((  Il2CppObject * (*) (Enumerator_t2275378311 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2696031375_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m2540159901_gshared (Enumerator_t2275378311 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m2540159901(__this, method) ((  bool (*) (Enumerator_t2275378311 *, const MethodInfo*))Enumerator_MoveNext_m2540159901_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::get_Current()
extern "C"  KeyValuePair_2_t3007666127  Enumerator_get_Current_m2904646657_gshared (Enumerator_t2275378311 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m2904646657(__this, method) ((  KeyValuePair_2_t3007666127  (*) (Enumerator_t2275378311 *, const MethodInfo*))Enumerator_get_Current_m2904646657_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::get_CurrentKey()
extern "C"  int32_t Enumerator_get_CurrentKey_m1027350056_gshared (Enumerator_t2275378311 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentKey_m1027350056(__this, method) ((  int32_t (*) (Enumerator_t2275378311 *, const MethodInfo*))Enumerator_get_CurrentKey_m1027350056_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::get_CurrentValue()
extern "C"  TrackableResultData_t1947527974  Enumerator_get_CurrentValue_m602212904_gshared (Enumerator_t2275378311 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentValue_m602212904(__this, method) ((  TrackableResultData_t1947527974  (*) (Enumerator_t2275378311 *, const MethodInfo*))Enumerator_get_CurrentValue_m602212904_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::Reset()
extern "C"  void Enumerator_Reset_m326090910_gshared (Enumerator_t2275378311 * __this, const MethodInfo* method);
#define Enumerator_Reset_m326090910(__this, method) ((  void (*) (Enumerator_t2275378311 *, const MethodInfo*))Enumerator_Reset_m326090910_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::VerifyState()
extern "C"  void Enumerator_VerifyState_m669141279_gshared (Enumerator_t2275378311 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m669141279(__this, method) ((  void (*) (Enumerator_t2275378311 *, const MethodInfo*))Enumerator_VerifyState_m669141279_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::VerifyCurrent()
extern "C"  void Enumerator_VerifyCurrent_m2386527189_gshared (Enumerator_t2275378311 * __this, const MethodInfo* method);
#define Enumerator_VerifyCurrent_m2386527189(__this, method) ((  void (*) (Enumerator_t2275378311 *, const MethodInfo*))Enumerator_VerifyCurrent_m2386527189_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::Dispose()
extern "C"  void Enumerator_Dispose_m560967656_gshared (Enumerator_t2275378311 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m560967656(__this, method) ((  void (*) (Enumerator_t2275378311 *, const MethodInfo*))Enumerator_Dispose_m560967656_gshared)(__this, method)
