﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LipingShare.LCLib.Asn1Processor.Asn1Parser
struct Asn1Parser_t914015216;
// System.IO.Stream
struct Stream_t3255436806;
// LipingShare.LCLib.Asn1Processor.Asn1Node
struct Asn1Node_t1770761751;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IO_Stream3255436806.h"
#include "Security_LipingShare_LCLib_Asn1Processor_Asn1Node1770761751.h"

// System.Void LipingShare.LCLib.Asn1Processor.Asn1Parser::.ctor()
extern "C"  void Asn1Parser__ctor_m2929298044 (Asn1Parser_t914015216 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LipingShare.LCLib.Asn1Processor.Asn1Parser::LoadData(System.IO.Stream)
extern "C"  void Asn1Parser_LoadData_m2612632163 (Asn1Parser_t914015216 * __this, Stream_t3255436806 * ___stream0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// LipingShare.LCLib.Asn1Processor.Asn1Node LipingShare.LCLib.Asn1Processor.Asn1Parser::get_RootNode()
extern "C"  Asn1Node_t1770761751 * Asn1Parser_get_RootNode_m542146928 (Asn1Parser_t914015216 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String LipingShare.LCLib.Asn1Processor.Asn1Parser::GetNodeTextHeader(System.Int32)
extern "C"  String_t* Asn1Parser_GetNodeTextHeader_m3481486116 (Il2CppObject * __this /* static, unused */, int32_t ___lineLen0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String LipingShare.LCLib.Asn1Processor.Asn1Parser::ToString()
extern "C"  String_t* Asn1Parser_ToString_m3361830265 (Asn1Parser_t914015216 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String LipingShare.LCLib.Asn1Processor.Asn1Parser::GetNodeText(LipingShare.LCLib.Asn1Processor.Asn1Node,System.Int32)
extern "C"  String_t* Asn1Parser_GetNodeText_m742522949 (Il2CppObject * __this /* static, unused */, Asn1Node_t1770761751 * ___node0, int32_t ___lineLen1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
