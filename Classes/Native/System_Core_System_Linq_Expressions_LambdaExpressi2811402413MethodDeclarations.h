﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Linq.Expressions.LambdaExpression
struct LambdaExpression_t2811402413;
// System.Type
struct Type_t;
// System.Linq.Expressions.Expression
struct Expression_t114864668;
// System.Collections.ObjectModel.ReadOnlyCollection`1<System.Linq.Expressions.ParameterExpression>
struct ReadOnlyCollection_1_t3201290647;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Type1303803226.h"
#include "System_Core_System_Linq_Expressions_Expression114864668.h"

// System.Void System.Linq.Expressions.LambdaExpression::.ctor(System.Type,System.Linq.Expressions.Expression,System.Collections.ObjectModel.ReadOnlyCollection`1<System.Linq.Expressions.ParameterExpression>)
extern "C"  void LambdaExpression__ctor_m3786904971 (LambdaExpression_t2811402413 * __this, Type_t * ___delegateType0, Expression_t114864668 * ___body1, ReadOnlyCollection_1_t3201290647 * ___parameters2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Linq.Expressions.Expression System.Linq.Expressions.LambdaExpression::get_Body()
extern "C"  Expression_t114864668 * LambdaExpression_get_Body_m3261107648 (LambdaExpression_t2811402413 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.ObjectModel.ReadOnlyCollection`1<System.Linq.Expressions.ParameterExpression> System.Linq.Expressions.LambdaExpression::get_Parameters()
extern "C"  ReadOnlyCollection_1_t3201290647 * LambdaExpression_get_Parameters_m1872102406 (LambdaExpression_t2811402413 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
