﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LitJson.JsonMapper/<RegisterExporter>c__AnonStorey18`1<System.Object>
struct U3CRegisterExporterU3Ec__AnonStorey18_1_t1006285190;
// System.Object
struct Il2CppObject;
// LitJson.JsonWriter
struct JsonWriter_t1927598499;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_LitJson_JsonWriter1927598499.h"

// System.Void LitJson.JsonMapper/<RegisterExporter>c__AnonStorey18`1<System.Object>::.ctor()
extern "C"  void U3CRegisterExporterU3Ec__AnonStorey18_1__ctor_m772951007_gshared (U3CRegisterExporterU3Ec__AnonStorey18_1_t1006285190 * __this, const MethodInfo* method);
#define U3CRegisterExporterU3Ec__AnonStorey18_1__ctor_m772951007(__this, method) ((  void (*) (U3CRegisterExporterU3Ec__AnonStorey18_1_t1006285190 *, const MethodInfo*))U3CRegisterExporterU3Ec__AnonStorey18_1__ctor_m772951007_gshared)(__this, method)
// System.Void LitJson.JsonMapper/<RegisterExporter>c__AnonStorey18`1<System.Object>::<>m__32(System.Object,LitJson.JsonWriter)
extern "C"  void U3CRegisterExporterU3Ec__AnonStorey18_1_U3CU3Em__32_m3159422867_gshared (U3CRegisterExporterU3Ec__AnonStorey18_1_t1006285190 * __this, Il2CppObject * ___obj0, JsonWriter_t1927598499 * ___writer1, const MethodInfo* method);
#define U3CRegisterExporterU3Ec__AnonStorey18_1_U3CU3Em__32_m3159422867(__this, ___obj0, ___writer1, method) ((  void (*) (U3CRegisterExporterU3Ec__AnonStorey18_1_t1006285190 *, Il2CppObject *, JsonWriter_t1927598499 *, const MethodInfo*))U3CRegisterExporterU3Ec__AnonStorey18_1_U3CU3Em__32_m3159422867_gshared)(__this, ___obj0, ___writer1, method)
