﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_ValueType3507792607.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/$ArrayType$60
struct  U24ArrayTypeU2460_t540610917 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t U24ArrayTypeU2460_t540610917__padding[60];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for marshalling of: <PrivateImplementationDetails>/$ArrayType$60
struct U24ArrayTypeU2460_t540610917_marshaled_pinvoke
{
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t U24ArrayTypeU2460_t540610917__padding[60];
	};
};
// Native definition for marshalling of: <PrivateImplementationDetails>/$ArrayType$60
struct U24ArrayTypeU2460_t540610917_marshaled_com
{
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t U24ArrayTypeU2460_t540610917__padding[60];
	};
};
