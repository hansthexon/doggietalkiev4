﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LitJson.JsonInclude
struct JsonInclude_t3368190666;

#include "codegen/il2cpp-codegen.h"

// System.Void LitJson.JsonInclude::.ctor()
extern "C"  void JsonInclude__ctor_m2185056170 (JsonInclude_t3368190666 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
