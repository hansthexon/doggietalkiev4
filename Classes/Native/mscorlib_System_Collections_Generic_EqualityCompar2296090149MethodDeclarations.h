﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<SQLite4Unity3d.SQLiteConnection/IndexedColumn>
struct DefaultComparer_t2296090149;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_SQLite4Unity3d_SQLiteConnection_I674159988.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::.ctor()
extern "C"  void DefaultComparer__ctor_m3919468970_gshared (DefaultComparer_t2296090149 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m3919468970(__this, method) ((  void (*) (DefaultComparer_t2296090149 *, const MethodInfo*))DefaultComparer__ctor_m3919468970_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m3909433461_gshared (DefaultComparer_t2296090149 * __this, IndexedColumn_t674159988  ___obj0, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m3909433461(__this, ___obj0, method) ((  int32_t (*) (DefaultComparer_t2296090149 *, IndexedColumn_t674159988 , const MethodInfo*))DefaultComparer_GetHashCode_m3909433461_gshared)(__this, ___obj0, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m2342386701_gshared (DefaultComparer_t2296090149 * __this, IndexedColumn_t674159988  ___x0, IndexedColumn_t674159988  ___y1, const MethodInfo* method);
#define DefaultComparer_Equals_m2342386701(__this, ___x0, ___y1, method) ((  bool (*) (DefaultComparer_t2296090149 *, IndexedColumn_t674159988 , IndexedColumn_t674159988 , const MethodInfo*))DefaultComparer_Equals_m2342386701_gshared)(__this, ___x0, ___y1, method)
