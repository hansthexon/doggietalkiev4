﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// <PrivateImplementationDetails>
struct U3CPrivateImplementationDetailsU3E_t1486305144;
// IAPDemo
struct IAPDemo_t823941185;
// UnityEngine.Purchasing.IStoreController
struct IStoreController_t92554892;
// UnityEngine.Purchasing.IExtensionProvider
struct IExtensionProvider_t2460996543;
// UnityEngine.Purchasing.IAppleExtensions
struct IAppleExtensions_t1627764765;
// System.Object
struct Il2CppObject;
// UnityEngine.Purchasing.PurchaseEventArgs
struct PurchaseEventArgs_t547992434;
// UnityEngine.Purchasing.Product
struct Product_t1203687971;
// UnityEngine.Purchasing.IMicrosoftConfiguration
struct IMicrosoftConfiguration_t1212838845;
// UnityEngine.Purchasing.IGooglePlayConfiguration
struct IGooglePlayConfiguration_t2615679878;
// UnityEngine.Purchasing.IAmazonConfiguration
struct IAmazonConfiguration_t3016942165;
// System.Collections.Generic.IEnumerable`1<UnityEngine.Purchasing.Product>
struct IEnumerable_1_t1495815016;
// UnityEngine.UI.Dropdown
struct Dropdown_t1985816271;
// UnityEngine.UI.Button
struct Button_t2872111280;
// UnityEngine.UI.Text
struct Text_t356221433;
// UnityEngine.Purchasing.Security.AppleTangle
struct AppleTangle_t53875121;
// System.Byte[]
struct ByteU5BU5D_t3397334013;
// UnityEngine.Purchasing.Security.GooglePlayTangle
struct GooglePlayTangle_t2749524914;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "AssemblyU2DCSharpU2Dfirstpass_U3CModuleU3E3783534214.h"
#include "AssemblyU2DCSharpU2Dfirstpass_U3CModuleU3E3783534214MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_U3CPrivateImplementa1486305137.h"
#include "AssemblyU2DCSharpU2Dfirstpass_U3CPrivateImplementa1486305137MethodDeclarations.h"
#include "mscorlib_System_Void1841601450.h"
#include "mscorlib_System_Object2689449295MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_U3CPrivateImplementa3604436897.h"
#include "AssemblyU2DCSharpU2Dfirstpass_U3CPrivateImplementa3604436897MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_U3CPrivateImplementat540610917.h"
#include "AssemblyU2DCSharpU2Dfirstpass_U3CPrivateImplementat540610917MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_IAPDemo823941185.h"
#include "AssemblyU2DCSharpU2Dfirstpass_IAPDemo823941185MethodDeclarations.h"
#include "UnityEngine_UnityEngine_MonoBehaviour1158329972MethodDeclarations.h"
#include "mscorlib_System_Int322071877448.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Prod3600019299MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen1005487353MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Debug1368543263MethodDeclarations.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Prod1203687971MethodDeclarations.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Prod1573242544MethodDeclarations.h"
#include "mscorlib_System_String2029220233MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_Dropdown1985816271MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_Dropdown_OptionData2420267500MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1789388632MethodDeclarations.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Prod1203687971.h"
#include "UnityEngine.Purchasing_ArrayTypes.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Decimal724701077.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Prod3600019299.h"
#include "mscorlib_System_Action_1_gen1005487353.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_Boolean3825574718.h"
#include "mscorlib_ArrayTypes.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Prod1573242544.h"
#include "mscorlib_System_Decimal724701077MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_Dropdown1985816271.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1789388632.h"
#include "UnityEngine_UI_UnityEngine_UI_Dropdown_OptionData2420267500.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Purc2407199463.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Purch547992434.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Purch547992434MethodDeclarations.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Prod1942475268MethodDeclarations.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Prod1942475268.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Purc1322959839.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Init2954032642.h"
#include "Stores_UnityEngine_Purchasing_StandardPurchasingMo4003664591MethodDeclarations.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Conf1298400415MethodDeclarations.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_IDs3808979560MethodDeclarations.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Unit3301441281MethodDeclarations.h"
#include "Stores_UnityEngine_Purchasing_StandardPurchasingMo4003664591.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Conf1298400415.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_IDs3808979560.h"
#include "Stores_UnityEngine_Purchasing_FakeStoreUIMode2321492887.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Prod2754455291.h"
#include "System_Core_System_Collections_Generic_HashSet_1_ge275936122.h"
#include "UnityEngine_UnityEngine_Application354826772MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Component3819376471MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GameObject1756533147MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen3438463199MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityEvent_1_gen2110227463MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_Button2872111280MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityAction4025899511MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityEvent408735097MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Object1021602117MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_Selectable1490392188.h"
#include "UnityEngine_UnityEngine_RuntimePlatform1869584967.h"
#include "UnityEngine_UI_UnityEngine_UI_Button2872111280.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"
#include "UnityEngine_UI_UnityEngine_UI_Dropdown_DropdownEve2203087800.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen3438463199.h"
#include "UnityEngine_UI_UnityEngine_UI_Button_ButtonClicked2455055323.h"
#include "UnityEngine_UnityEngine_Events_UnityAction4025899511.h"
#include "UnityEngine_UnityEngine_Object1021602117.h"
#include "mscorlib_System_Boolean3825574718MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_Text356221433.h"
#include "UnityEngine_UI_UnityEngine_UI_Text356221433MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_Selectable1490392188MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen3627374100MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen3627374100.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityEngine_Purchasing53875121.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityEngine_Purchasing53875121MethodDeclarations.h"
#include "mscorlib_System_Convert2607082565MethodDeclarations.h"
#include "mscorlib_System_Runtime_CompilerServices_RuntimeHel266230107MethodDeclarations.h"
#include "mscorlib_System_Byte3683104436.h"
#include "mscorlib_System_RuntimeFieldHandle2331729674.h"
#include "Security_UnityEngine_Purchasing_Security_Obfuscato2878230988MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityEngine_Purchasi2749524914.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityEngine_Purchasi2749524914MethodDeclarations.h"

// !!0 UnityEngine.Purchasing.IExtensionProvider::GetExtension<System.Object>()
extern "C"  Il2CppObject * IExtensionProvider_GetExtension_TisIl2CppObject_m925367407_gshared (Il2CppObject * __this, const MethodInfo* method);
#define IExtensionProvider_GetExtension_TisIl2CppObject_m925367407(__this, method) ((  Il2CppObject * (*) (Il2CppObject *, const MethodInfo*))IExtensionProvider_GetExtension_TisIl2CppObject_m925367407_gshared)(__this, method)
// !!0 UnityEngine.Purchasing.IExtensionProvider::GetExtension<UnityEngine.Purchasing.IAppleExtensions>()
#define IExtensionProvider_GetExtension_TisIAppleExtensions_t1627764765_m3353400141(__this, method) ((  Il2CppObject * (*) (Il2CppObject *, const MethodInfo*))IExtensionProvider_GetExtension_TisIl2CppObject_m925367407_gshared)(__this, method)
// !!0 UnityEngine.Purchasing.ConfigurationBuilder::Configure<System.Object>()
extern "C"  Il2CppObject * ConfigurationBuilder_Configure_TisIl2CppObject_m4102053310_gshared (ConfigurationBuilder_t1298400415 * __this, const MethodInfo* method);
#define ConfigurationBuilder_Configure_TisIl2CppObject_m4102053310(__this, method) ((  Il2CppObject * (*) (ConfigurationBuilder_t1298400415 *, const MethodInfo*))ConfigurationBuilder_Configure_TisIl2CppObject_m4102053310_gshared)(__this, method)
// !!0 UnityEngine.Purchasing.ConfigurationBuilder::Configure<UnityEngine.Purchasing.IMicrosoftConfiguration>()
#define ConfigurationBuilder_Configure_TisIMicrosoftConfiguration_t1212838845_m2434552418(__this, method) ((  Il2CppObject * (*) (ConfigurationBuilder_t1298400415 *, const MethodInfo*))ConfigurationBuilder_Configure_TisIl2CppObject_m4102053310_gshared)(__this, method)
// !!0 UnityEngine.Purchasing.ConfigurationBuilder::Configure<UnityEngine.Purchasing.IGooglePlayConfiguration>()
#define ConfigurationBuilder_Configure_TisIGooglePlayConfiguration_t2615679878_m3137646553(__this, method) ((  Il2CppObject * (*) (ConfigurationBuilder_t1298400415 *, const MethodInfo*))ConfigurationBuilder_Configure_TisIl2CppObject_m4102053310_gshared)(__this, method)
// !!0 UnityEngine.Purchasing.ConfigurationBuilder::Configure<UnityEngine.Purchasing.IAmazonConfiguration>()
#define ConfigurationBuilder_Configure_TisIAmazonConfiguration_t3016942165_m4264701324(__this, method) ((  Il2CppObject * (*) (ConfigurationBuilder_t1298400415 *, const MethodInfo*))ConfigurationBuilder_Configure_TisIl2CppObject_m4102053310_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<System.Object>()
extern "C"  Il2CppObject * GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared (GameObject_t1756533147 * __this, const MethodInfo* method);
#define GameObject_GetComponent_TisIl2CppObject_m2812611596(__this, method) ((  Il2CppObject * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.UI.Dropdown>()
#define GameObject_GetComponent_TisDropdown_t1985816271_m1750975685(__this, method) ((  Dropdown_t1985816271 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.UI.Button>()
#define GameObject_GetComponent_TisButton_t2872111280_m3862106414(__this, method) ((  Button_t2872111280 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.UI.Text>()
#define GameObject_GetComponent_TisText_t356221433_m4280536079(__this, method) ((  Text_t356221433 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared)(__this, method)
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void <PrivateImplementationDetails>::.ctor()
extern "C"  void U3CPrivateImplementationDetailsU3E__ctor_m3719613238 (U3CPrivateImplementationDetailsU3E_t1486305144 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// Conversion methods for marshalling of: <PrivateImplementationDetails>/$ArrayType$244
extern "C" void U24ArrayTypeU24244_t3604436897_marshal_pinvoke(const U24ArrayTypeU24244_t3604436897& unmarshaled, U24ArrayTypeU24244_t3604436897_marshaled_pinvoke& marshaled)
{
}
extern "C" void U24ArrayTypeU24244_t3604436897_marshal_pinvoke_back(const U24ArrayTypeU24244_t3604436897_marshaled_pinvoke& marshaled, U24ArrayTypeU24244_t3604436897& unmarshaled)
{
}
// Conversion method for clean up from marshalling of: <PrivateImplementationDetails>/$ArrayType$244
extern "C" void U24ArrayTypeU24244_t3604436897_marshal_pinvoke_cleanup(U24ArrayTypeU24244_t3604436897_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: <PrivateImplementationDetails>/$ArrayType$244
extern "C" void U24ArrayTypeU24244_t3604436897_marshal_com(const U24ArrayTypeU24244_t3604436897& unmarshaled, U24ArrayTypeU24244_t3604436897_marshaled_com& marshaled)
{
}
extern "C" void U24ArrayTypeU24244_t3604436897_marshal_com_back(const U24ArrayTypeU24244_t3604436897_marshaled_com& marshaled, U24ArrayTypeU24244_t3604436897& unmarshaled)
{
}
// Conversion method for clean up from marshalling of: <PrivateImplementationDetails>/$ArrayType$244
extern "C" void U24ArrayTypeU24244_t3604436897_marshal_com_cleanup(U24ArrayTypeU24244_t3604436897_marshaled_com& marshaled)
{
}
// Conversion methods for marshalling of: <PrivateImplementationDetails>/$ArrayType$60
extern "C" void U24ArrayTypeU2460_t540610917_marshal_pinvoke(const U24ArrayTypeU2460_t540610917& unmarshaled, U24ArrayTypeU2460_t540610917_marshaled_pinvoke& marshaled)
{
}
extern "C" void U24ArrayTypeU2460_t540610917_marshal_pinvoke_back(const U24ArrayTypeU2460_t540610917_marshaled_pinvoke& marshaled, U24ArrayTypeU2460_t540610917& unmarshaled)
{
}
// Conversion method for clean up from marshalling of: <PrivateImplementationDetails>/$ArrayType$60
extern "C" void U24ArrayTypeU2460_t540610917_marshal_pinvoke_cleanup(U24ArrayTypeU2460_t540610917_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: <PrivateImplementationDetails>/$ArrayType$60
extern "C" void U24ArrayTypeU2460_t540610917_marshal_com(const U24ArrayTypeU2460_t540610917& unmarshaled, U24ArrayTypeU2460_t540610917_marshaled_com& marshaled)
{
}
extern "C" void U24ArrayTypeU2460_t540610917_marshal_com_back(const U24ArrayTypeU2460_t540610917_marshaled_com& marshaled, U24ArrayTypeU2460_t540610917& unmarshaled)
{
}
// Conversion method for clean up from marshalling of: <PrivateImplementationDetails>/$ArrayType$60
extern "C" void U24ArrayTypeU2460_t540610917_marshal_com_cleanup(U24ArrayTypeU2460_t540610917_marshaled_com& marshaled)
{
}
// System.Void IAPDemo::.ctor()
extern "C"  void IAPDemo__ctor_m3267573714 (IAPDemo_t823941185 * __this, const MethodInfo* method)
{
	{
		__this->set_m_SelectedItemIndex_4((-1));
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void IAPDemo::OnInitialized(UnityEngine.Purchasing.IStoreController,UnityEngine.Purchasing.IExtensionProvider)
extern Il2CppClass* IStoreController_t92554892_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_1_t1005487353_il2cpp_TypeInfo_var;
extern Il2CppClass* IAppleExtensions_t1627764765_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppClass* StringU5BU5D_t1642385972_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* OptionData_t2420267500_il2cpp_TypeInfo_var;
extern const MethodInfo* IExtensionProvider_GetExtension_TisIAppleExtensions_t1627764765_m3353400141_MethodInfo_var;
extern const MethodInfo* IAPDemo_OnDeferred_m801123048_MethodInfo_var;
extern const MethodInfo* Action_1__ctor_m2354061435_MethodInfo_var;
extern const MethodInfo* List_1_set_Item_m288507185_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral820171765;
extern Il2CppCodeGenString* _stringLiteral1220271277;
extern Il2CppCodeGenString* _stringLiteral4265067804;
extern const uint32_t IAPDemo_OnInitialized_m2628601748_MetadataUsageId;
extern "C"  void IAPDemo_OnInitialized_m2628601748 (IAPDemo_t823941185 * __this, Il2CppObject * ___controller0, Il2CppObject * ___extensions1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (IAPDemo_OnInitialized_m2628601748_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Product_t1203687971 * V_0 = NULL;
	ProductU5BU5D_t728099314* V_1 = NULL;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	Product_t1203687971 * V_4 = NULL;
	String_t* V_5 = NULL;
	Decimal_t724701077  V_6;
	memset(&V_6, 0, sizeof(V_6));
	{
		Il2CppObject * L_0 = ___controller0;
		__this->set_m_Controller_2(L_0);
		Il2CppObject * L_1 = ___extensions1;
		NullCheck(L_1);
		Il2CppObject * L_2 = GenericInterfaceFuncInvoker0< Il2CppObject * >::Invoke(IExtensionProvider_GetExtension_TisIAppleExtensions_t1627764765_m3353400141_MethodInfo_var, L_1);
		__this->set_m_AppleExtensions_3(L_2);
		Il2CppObject * L_3 = ___controller0;
		NullCheck(L_3);
		ProductCollection_t3600019299 * L_4 = InterfaceFuncInvoker0< ProductCollection_t3600019299 * >::Invoke(0 /* UnityEngine.Purchasing.ProductCollection UnityEngine.Purchasing.IStoreController::get_products() */, IStoreController_t92554892_il2cpp_TypeInfo_var, L_3);
		NullCheck(L_4);
		ProductU5BU5D_t728099314* L_5 = ProductCollection_get_all_m3364167965(L_4, /*hidden argument*/NULL);
		IAPDemo_InitUI_m3350388155(__this, (Il2CppObject*)(Il2CppObject*)L_5, /*hidden argument*/NULL);
		Il2CppObject * L_6 = __this->get_m_AppleExtensions_3();
		IntPtr_t L_7;
		L_7.set_m_value_0((void*)(void*)IAPDemo_OnDeferred_m801123048_MethodInfo_var);
		Action_1_t1005487353 * L_8 = (Action_1_t1005487353 *)il2cpp_codegen_object_new(Action_1_t1005487353_il2cpp_TypeInfo_var);
		Action_1__ctor_m2354061435(L_8, __this, L_7, /*hidden argument*/Action_1__ctor_m2354061435_MethodInfo_var);
		NullCheck(L_6);
		InterfaceActionInvoker1< Action_1_t1005487353 * >::Invoke(1 /* System.Void UnityEngine.Purchasing.IAppleExtensions::RegisterPurchaseDeferredListener(System.Action`1<UnityEngine.Purchasing.Product>) */, IAppleExtensions_t1627764765_il2cpp_TypeInfo_var, L_6, L_8);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, _stringLiteral820171765, /*hidden argument*/NULL);
		Il2CppObject * L_9 = ___controller0;
		NullCheck(L_9);
		ProductCollection_t3600019299 * L_10 = InterfaceFuncInvoker0< ProductCollection_t3600019299 * >::Invoke(0 /* UnityEngine.Purchasing.ProductCollection UnityEngine.Purchasing.IStoreController::get_products() */, IStoreController_t92554892_il2cpp_TypeInfo_var, L_9);
		NullCheck(L_10);
		ProductU5BU5D_t728099314* L_11 = ProductCollection_get_all_m3364167965(L_10, /*hidden argument*/NULL);
		V_1 = L_11;
		V_2 = 0;
		goto IL_00cf;
	}

IL_0058:
	{
		ProductU5BU5D_t728099314* L_12 = V_1;
		int32_t L_13 = V_2;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, L_13);
		int32_t L_14 = L_13;
		Product_t1203687971 * L_15 = (L_12)->GetAt(static_cast<il2cpp_array_size_t>(L_14));
		V_0 = L_15;
		Product_t1203687971 * L_16 = V_0;
		NullCheck(L_16);
		bool L_17 = Product_get_availableToPurchase_m3285924692(L_16, /*hidden argument*/NULL);
		if (!L_17)
		{
			goto IL_00cb;
		}
	}
	{
		StringU5BU5D_t1642385972* L_18 = ((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)5));
		Product_t1203687971 * L_19 = V_0;
		NullCheck(L_19);
		ProductMetadata_t1573242544 * L_20 = Product_get_metadata_m263398044(L_19, /*hidden argument*/NULL);
		NullCheck(L_20);
		String_t* L_21 = ProductMetadata_get_localizedTitle_m1010599344(L_20, /*hidden argument*/NULL);
		NullCheck(L_18);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_18, 0);
		ArrayElementTypeCheck (L_18, L_21);
		(L_18)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)L_21);
		StringU5BU5D_t1642385972* L_22 = L_18;
		Product_t1203687971 * L_23 = V_0;
		NullCheck(L_23);
		ProductMetadata_t1573242544 * L_24 = Product_get_metadata_m263398044(L_23, /*hidden argument*/NULL);
		NullCheck(L_24);
		String_t* L_25 = ProductMetadata_get_localizedDescription_m3665253400(L_24, /*hidden argument*/NULL);
		NullCheck(L_22);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_22, 1);
		ArrayElementTypeCheck (L_22, L_25);
		(L_22)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)L_25);
		StringU5BU5D_t1642385972* L_26 = L_22;
		Product_t1203687971 * L_27 = V_0;
		NullCheck(L_27);
		ProductMetadata_t1573242544 * L_28 = Product_get_metadata_m263398044(L_27, /*hidden argument*/NULL);
		NullCheck(L_28);
		String_t* L_29 = ProductMetadata_get_isoCurrencyCode_m2098971654(L_28, /*hidden argument*/NULL);
		NullCheck(L_26);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_26, 2);
		ArrayElementTypeCheck (L_26, L_29);
		(L_26)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)L_29);
		StringU5BU5D_t1642385972* L_30 = L_26;
		Product_t1203687971 * L_31 = V_0;
		NullCheck(L_31);
		ProductMetadata_t1573242544 * L_32 = Product_get_metadata_m263398044(L_31, /*hidden argument*/NULL);
		NullCheck(L_32);
		Decimal_t724701077  L_33 = ProductMetadata_get_localizedPrice_m158316127(L_32, /*hidden argument*/NULL);
		V_6 = L_33;
		String_t* L_34 = Decimal_ToString_m759431975((&V_6), /*hidden argument*/NULL);
		NullCheck(L_30);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_30, 3);
		ArrayElementTypeCheck (L_30, L_34);
		(L_30)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)L_34);
		StringU5BU5D_t1642385972* L_35 = L_30;
		Product_t1203687971 * L_36 = V_0;
		NullCheck(L_36);
		ProductMetadata_t1573242544 * L_37 = Product_get_metadata_m263398044(L_36, /*hidden argument*/NULL);
		NullCheck(L_37);
		String_t* L_38 = ProductMetadata_get_localizedPriceString_m2216629954(L_37, /*hidden argument*/NULL);
		NullCheck(L_35);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_35, 4);
		ArrayElementTypeCheck (L_35, L_38);
		(L_35)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)L_38);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_39 = String_Join_m1966872927(NULL /*static, unused*/, _stringLiteral1220271277, L_35, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_39, /*hidden argument*/NULL);
	}

IL_00cb:
	{
		int32_t L_40 = V_2;
		V_2 = ((int32_t)((int32_t)L_40+(int32_t)1));
	}

IL_00cf:
	{
		int32_t L_41 = V_2;
		ProductU5BU5D_t728099314* L_42 = V_1;
		NullCheck(L_42);
		if ((((int32_t)L_41) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_42)->max_length)))))))
		{
			goto IL_0058;
		}
	}
	{
		Il2CppObject * L_43 = __this->get_m_Controller_2();
		NullCheck(L_43);
		ProductCollection_t3600019299 * L_44 = InterfaceFuncInvoker0< ProductCollection_t3600019299 * >::Invoke(0 /* UnityEngine.Purchasing.ProductCollection UnityEngine.Purchasing.IStoreController::get_products() */, IStoreController_t92554892_il2cpp_TypeInfo_var, L_43);
		NullCheck(L_44);
		ProductU5BU5D_t728099314* L_45 = ProductCollection_get_all_m3364167965(L_44, /*hidden argument*/NULL);
		NullCheck(L_45);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_45)->max_length))))) <= ((int32_t)0)))
		{
			goto IL_00f7;
		}
	}
	{
		__this->set_m_SelectedItemIndex_4(0);
	}

IL_00f7:
	{
		V_3 = 0;
		goto IL_0152;
	}

IL_00fe:
	{
		Il2CppObject * L_46 = __this->get_m_Controller_2();
		NullCheck(L_46);
		ProductCollection_t3600019299 * L_47 = InterfaceFuncInvoker0< ProductCollection_t3600019299 * >::Invoke(0 /* UnityEngine.Purchasing.ProductCollection UnityEngine.Purchasing.IStoreController::get_products() */, IStoreController_t92554892_il2cpp_TypeInfo_var, L_46);
		NullCheck(L_47);
		ProductU5BU5D_t728099314* L_48 = ProductCollection_get_all_m3364167965(L_47, /*hidden argument*/NULL);
		int32_t L_49 = V_3;
		NullCheck(L_48);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_48, L_49);
		int32_t L_50 = L_49;
		Product_t1203687971 * L_51 = (L_48)->GetAt(static_cast<il2cpp_array_size_t>(L_50));
		V_4 = L_51;
		Product_t1203687971 * L_52 = V_4;
		NullCheck(L_52);
		ProductMetadata_t1573242544 * L_53 = Product_get_metadata_m263398044(L_52, /*hidden argument*/NULL);
		NullCheck(L_53);
		String_t* L_54 = ProductMetadata_get_localizedTitle_m1010599344(L_53, /*hidden argument*/NULL);
		Product_t1203687971 * L_55 = V_4;
		NullCheck(L_55);
		ProductMetadata_t1573242544 * L_56 = Product_get_metadata_m263398044(L_55, /*hidden argument*/NULL);
		NullCheck(L_56);
		String_t* L_57 = ProductMetadata_get_localizedPriceString_m2216629954(L_56, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_58 = String_Format_m1811873526(NULL /*static, unused*/, _stringLiteral4265067804, L_54, L_57, /*hidden argument*/NULL);
		V_5 = L_58;
		Dropdown_t1985816271 * L_59 = IAPDemo_GetDropdown_m1658389538(__this, /*hidden argument*/NULL);
		NullCheck(L_59);
		List_1_t1789388632 * L_60 = Dropdown_get_options_m2669836220(L_59, /*hidden argument*/NULL);
		int32_t L_61 = V_3;
		String_t* L_62 = V_5;
		OptionData_t2420267500 * L_63 = (OptionData_t2420267500 *)il2cpp_codegen_object_new(OptionData_t2420267500_il2cpp_TypeInfo_var);
		OptionData__ctor_m743450704(L_63, L_62, /*hidden argument*/NULL);
		NullCheck(L_60);
		List_1_set_Item_m288507185(L_60, L_61, L_63, /*hidden argument*/List_1_set_Item_m288507185_MethodInfo_var);
		int32_t L_64 = V_3;
		V_3 = ((int32_t)((int32_t)L_64+(int32_t)1));
	}

IL_0152:
	{
		int32_t L_65 = V_3;
		Il2CppObject * L_66 = __this->get_m_Controller_2();
		NullCheck(L_66);
		ProductCollection_t3600019299 * L_67 = InterfaceFuncInvoker0< ProductCollection_t3600019299 * >::Invoke(0 /* UnityEngine.Purchasing.ProductCollection UnityEngine.Purchasing.IStoreController::get_products() */, IStoreController_t92554892_il2cpp_TypeInfo_var, L_66);
		NullCheck(L_67);
		ProductU5BU5D_t728099314* L_68 = ProductCollection_get_all_m3364167965(L_67, /*hidden argument*/NULL);
		NullCheck(L_68);
		if ((((int32_t)L_65) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_68)->max_length)))))))
		{
			goto IL_00fe;
		}
	}
	{
		Dropdown_t1985816271 * L_69 = IAPDemo_GetDropdown_m1658389538(__this, /*hidden argument*/NULL);
		NullCheck(L_69);
		Dropdown_RefreshShownValue_m3113581237(L_69, /*hidden argument*/NULL);
		IAPDemo_UpdateHistoryUI_m2607563357(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Purchasing.PurchaseProcessingResult IAPDemo::ProcessPurchase(UnityEngine.Purchasing.PurchaseEventArgs)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1546350049;
extern Il2CppCodeGenString* _stringLiteral1530557140;
extern const uint32_t IAPDemo_ProcessPurchase_m3949646312_MetadataUsageId;
extern "C"  int32_t IAPDemo_ProcessPurchase_m3949646312 (IAPDemo_t823941185 * __this, PurchaseEventArgs_t547992434 * ___e0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (IAPDemo_ProcessPurchase_m3949646312_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		PurchaseEventArgs_t547992434 * L_0 = ___e0;
		NullCheck(L_0);
		Product_t1203687971 * L_1 = PurchaseEventArgs_get_purchasedProduct_m3327797079(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		ProductDefinition_t1942475268 * L_2 = Product_get_definition_m2035415516(L_1, /*hidden argument*/NULL);
		NullCheck(L_2);
		String_t* L_3 = ProductDefinition_get_id_m264072292(L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral1546350049, L_3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		PurchaseEventArgs_t547992434 * L_5 = ___e0;
		NullCheck(L_5);
		Product_t1203687971 * L_6 = PurchaseEventArgs_get_purchasedProduct_m3327797079(L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		String_t* L_7 = Product_get_receipt_m1045158498(L_6, /*hidden argument*/NULL);
		String_t* L_8 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral1530557140, L_7, /*hidden argument*/NULL);
		Debug_Log_m920475918(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		__this->set_m_PurchaseInProgress_5((bool)0);
		IAPDemo_UpdateHistoryUI_m2607563357(__this, /*hidden argument*/NULL);
		return (int32_t)(0);
	}
}
// System.Void IAPDemo::OnPurchaseFailed(UnityEngine.Purchasing.Product,UnityEngine.Purchasing.PurchaseFailureReason)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppClass* PurchaseFailureReason_t1322959839_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral830689752;
extern const uint32_t IAPDemo_OnPurchaseFailed_m2053066303_MetadataUsageId;
extern "C"  void IAPDemo_OnPurchaseFailed_m2053066303 (IAPDemo_t823941185 * __this, Product_t1203687971 * ___item0, int32_t ___r1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (IAPDemo_OnPurchaseFailed_m2053066303_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Product_t1203687971 * L_0 = ___item0;
		NullCheck(L_0);
		ProductDefinition_t1942475268 * L_1 = Product_get_definition_m2035415516(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		String_t* L_2 = ProductDefinition_get_id_m264072292(L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral830689752, L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		int32_t L_4 = ___r1;
		int32_t L_5 = L_4;
		Il2CppObject * L_6 = Box(PurchaseFailureReason_t1322959839_il2cpp_TypeInfo_var, &L_5);
		Debug_Log_m920475918(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		__this->set_m_PurchaseInProgress_5((bool)0);
		return;
	}
}
// System.Void IAPDemo::OnInitializeFailed(UnityEngine.Purchasing.InitializationFailureReason)
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1657966450;
extern Il2CppCodeGenString* _stringLiteral1153205404;
extern Il2CppCodeGenString* _stringLiteral3815334426;
extern Il2CppCodeGenString* _stringLiteral417931313;
extern const uint32_t IAPDemo_OnInitializeFailed_m1037377075_MetadataUsageId;
extern "C"  void IAPDemo_OnInitializeFailed_m1037377075 (IAPDemo_t823941185 * __this, int32_t ___error0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (IAPDemo_OnInitializeFailed_m1037377075_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, _stringLiteral1657966450, /*hidden argument*/NULL);
		int32_t L_0 = ___error0;
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0032;
		}
		if (L_1 == 1)
		{
			goto IL_0041;
		}
		if (L_1 == 2)
		{
			goto IL_0023;
		}
	}
	{
		goto IL_0050;
	}

IL_0023:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogError_m3715728798(NULL /*static, unused*/, _stringLiteral1153205404, /*hidden argument*/NULL);
		goto IL_0050;
	}

IL_0032:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, _stringLiteral3815334426, /*hidden argument*/NULL);
		goto IL_0050;
	}

IL_0041:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, _stringLiteral417931313, /*hidden argument*/NULL);
		goto IL_0050;
	}

IL_0050:
	{
		return;
	}
}
// System.Void IAPDemo::Awake()
extern Il2CppClass* IPurchasingModuleU5BU5D_t4128245854_il2cpp_TypeInfo_var;
extern Il2CppClass* IMicrosoftConfiguration_t1212838845_il2cpp_TypeInfo_var;
extern Il2CppClass* IGooglePlayConfiguration_t2615679878_il2cpp_TypeInfo_var;
extern Il2CppClass* IDs_t3808979560_il2cpp_TypeInfo_var;
extern Il2CppClass* StringU5BU5D_t1642385972_il2cpp_TypeInfo_var;
extern Il2CppClass* IAmazonConfiguration_t3016942165_il2cpp_TypeInfo_var;
extern const MethodInfo* ConfigurationBuilder_Configure_TisIMicrosoftConfiguration_t1212838845_m2434552418_MethodInfo_var;
extern const MethodInfo* ConfigurationBuilder_Configure_TisIGooglePlayConfiguration_t2615679878_m3137646553_MethodInfo_var;
extern const MethodInfo* ConfigurationBuilder_Configure_TisIAmazonConfiguration_t3016942165_m4264701324_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2453939300;
extern Il2CppCodeGenString* _stringLiteral1830547213;
extern Il2CppCodeGenString* _stringLiteral2946374258;
extern Il2CppCodeGenString* _stringLiteral2700534657;
extern Il2CppCodeGenString* _stringLiteral968850961;
extern Il2CppCodeGenString* _stringLiteral862026094;
extern Il2CppCodeGenString* _stringLiteral1070925405;
extern Il2CppCodeGenString* _stringLiteral1346993374;
extern Il2CppCodeGenString* _stringLiteral2315729507;
extern Il2CppCodeGenString* _stringLiteral784127634;
extern const uint32_t IAPDemo_Awake_m1872671099_MetadataUsageId;
extern "C"  void IAPDemo_Awake_m1872671099 (IAPDemo_t823941185 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (IAPDemo_Awake_m1872671099_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	StandardPurchasingModule_t4003664591 * V_0 = NULL;
	ConfigurationBuilder_t1298400415 * V_1 = NULL;
	IDs_t3808979560 * V_2 = NULL;
	{
		StandardPurchasingModule_t4003664591 * L_0 = StandardPurchasingModule_Instance_m2889845773(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		StandardPurchasingModule_t4003664591 * L_1 = V_0;
		NullCheck(L_1);
		StandardPurchasingModule_set_useFakeStoreUIMode_m3278247934(L_1, 1, /*hidden argument*/NULL);
		StandardPurchasingModule_t4003664591 * L_2 = V_0;
		ConfigurationBuilder_t1298400415 * L_3 = ConfigurationBuilder_Instance_m4243979412(NULL /*static, unused*/, L_2, ((IPurchasingModuleU5BU5D_t4128245854*)SZArrayNew(IPurchasingModuleU5BU5D_t4128245854_il2cpp_TypeInfo_var, (uint32_t)0)), /*hidden argument*/NULL);
		V_1 = L_3;
		ConfigurationBuilder_t1298400415 * L_4 = V_1;
		NullCheck(L_4);
		Il2CppObject * L_5 = ConfigurationBuilder_Configure_TisIMicrosoftConfiguration_t1212838845_m2434552418(L_4, /*hidden argument*/ConfigurationBuilder_Configure_TisIMicrosoftConfiguration_t1212838845_m2434552418_MethodInfo_var);
		NullCheck(L_5);
		InterfaceActionInvoker1< bool >::Invoke(0 /* System.Void UnityEngine.Purchasing.IMicrosoftConfiguration::set_useMockBillingSystem(System.Boolean) */, IMicrosoftConfiguration_t1212838845_il2cpp_TypeInfo_var, L_5, (bool)1);
		ConfigurationBuilder_t1298400415 * L_6 = V_1;
		NullCheck(L_6);
		Il2CppObject * L_7 = ConfigurationBuilder_Configure_TisIGooglePlayConfiguration_t2615679878_m3137646553(L_6, /*hidden argument*/ConfigurationBuilder_Configure_TisIGooglePlayConfiguration_t2615679878_m3137646553_MethodInfo_var);
		NullCheck(L_7);
		InterfaceActionInvoker1< String_t* >::Invoke(0 /* System.Void UnityEngine.Purchasing.IGooglePlayConfiguration::SetPublicKey(System.String) */, IGooglePlayConfiguration_t2615679878_il2cpp_TypeInfo_var, L_7, _stringLiteral2453939300);
		ConfigurationBuilder_t1298400415 * L_8 = V_1;
		IDs_t3808979560 * L_9 = (IDs_t3808979560 *)il2cpp_codegen_object_new(IDs_t3808979560_il2cpp_TypeInfo_var);
		IDs__ctor_m2525416339(L_9, /*hidden argument*/NULL);
		V_2 = L_9;
		IDs_t3808979560 * L_10 = V_2;
		StringU5BU5D_t1642385972* L_11 = ((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)1));
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, 0);
		ArrayElementTypeCheck (L_11, _stringLiteral2700534657);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral2700534657);
		NullCheck(L_10);
		IDs_Add_m217445704(L_10, _stringLiteral2946374258, L_11, /*hidden argument*/NULL);
		IDs_t3808979560 * L_12 = V_2;
		NullCheck(L_8);
		ConfigurationBuilder_AddProduct_m918244722(L_8, _stringLiteral1830547213, 0, L_12, /*hidden argument*/NULL);
		ConfigurationBuilder_t1298400415 * L_13 = V_1;
		IDs_t3808979560 * L_14 = (IDs_t3808979560 *)il2cpp_codegen_object_new(IDs_t3808979560_il2cpp_TypeInfo_var);
		IDs__ctor_m2525416339(L_14, /*hidden argument*/NULL);
		V_2 = L_14;
		IDs_t3808979560 * L_15 = V_2;
		StringU5BU5D_t1642385972* L_16 = ((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)1));
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, 0);
		ArrayElementTypeCheck (L_16, _stringLiteral2700534657);
		(L_16)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral2700534657);
		NullCheck(L_15);
		IDs_Add_m217445704(L_15, _stringLiteral862026094, L_16, /*hidden argument*/NULL);
		IDs_t3808979560 * L_17 = V_2;
		NullCheck(L_13);
		ConfigurationBuilder_AddProduct_m918244722(L_13, _stringLiteral968850961, 0, L_17, /*hidden argument*/NULL);
		ConfigurationBuilder_t1298400415 * L_18 = V_1;
		IDs_t3808979560 * L_19 = (IDs_t3808979560 *)il2cpp_codegen_object_new(IDs_t3808979560_il2cpp_TypeInfo_var);
		IDs__ctor_m2525416339(L_19, /*hidden argument*/NULL);
		V_2 = L_19;
		IDs_t3808979560 * L_20 = V_2;
		StringU5BU5D_t1642385972* L_21 = ((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)1));
		NullCheck(L_21);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_21, 0);
		ArrayElementTypeCheck (L_21, _stringLiteral2700534657);
		(L_21)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral2700534657);
		NullCheck(L_20);
		IDs_Add_m217445704(L_20, _stringLiteral1346993374, L_21, /*hidden argument*/NULL);
		IDs_t3808979560 * L_22 = V_2;
		NullCheck(L_18);
		ConfigurationBuilder_AddProduct_m918244722(L_18, _stringLiteral1070925405, 1, L_22, /*hidden argument*/NULL);
		ConfigurationBuilder_t1298400415 * L_23 = V_1;
		IDs_t3808979560 * L_24 = (IDs_t3808979560 *)il2cpp_codegen_object_new(IDs_t3808979560_il2cpp_TypeInfo_var);
		IDs__ctor_m2525416339(L_24, /*hidden argument*/NULL);
		V_2 = L_24;
		IDs_t3808979560 * L_25 = V_2;
		StringU5BU5D_t1642385972* L_26 = ((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)1));
		NullCheck(L_26);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_26, 0);
		ArrayElementTypeCheck (L_26, _stringLiteral2700534657);
		(L_26)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral2700534657);
		NullCheck(L_25);
		IDs_Add_m217445704(L_25, _stringLiteral784127634, L_26, /*hidden argument*/NULL);
		IDs_t3808979560 * L_27 = V_2;
		NullCheck(L_23);
		ConfigurationBuilder_AddProduct_m918244722(L_23, _stringLiteral2315729507, 2, L_27, /*hidden argument*/NULL);
		ConfigurationBuilder_t1298400415 * L_28 = V_1;
		NullCheck(L_28);
		Il2CppObject * L_29 = ConfigurationBuilder_Configure_TisIAmazonConfiguration_t3016942165_m4264701324(L_28, /*hidden argument*/ConfigurationBuilder_Configure_TisIAmazonConfiguration_t3016942165_m4264701324_MethodInfo_var);
		ConfigurationBuilder_t1298400415 * L_30 = V_1;
		NullCheck(L_30);
		HashSet_1_t275936122 * L_31 = ConfigurationBuilder_get_products_m3201377931(L_30, /*hidden argument*/NULL);
		NullCheck(L_29);
		InterfaceActionInvoker1< HashSet_1_t275936122 * >::Invoke(0 /* System.Void UnityEngine.Purchasing.IAmazonConfiguration::WriteSandboxJSON(System.Collections.Generic.HashSet`1<UnityEngine.Purchasing.ProductDefinition>) */, IAmazonConfiguration_t3016942165_il2cpp_TypeInfo_var, L_29, L_31);
		ConfigurationBuilder_t1298400415 * L_32 = V_1;
		UnityPurchasing_Initialize_m1012234273(NULL /*static, unused*/, __this, L_32, /*hidden argument*/NULL);
		return;
	}
}
// System.Void IAPDemo::OnTransactionsRestored(System.Boolean)
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1900125737;
extern const uint32_t IAPDemo_OnTransactionsRestored_m2633548551_MetadataUsageId;
extern "C"  void IAPDemo_OnTransactionsRestored_m2633548551 (IAPDemo_t823941185 * __this, bool ___success0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (IAPDemo_OnTransactionsRestored_m2633548551_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, _stringLiteral1900125737, /*hidden argument*/NULL);
		return;
	}
}
// System.Void IAPDemo::OnDeferred(UnityEngine.Purchasing.Product)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1205201122;
extern const uint32_t IAPDemo_OnDeferred_m801123048_MetadataUsageId;
extern "C"  void IAPDemo_OnDeferred_m801123048 (IAPDemo_t823941185 * __this, Product_t1203687971 * ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (IAPDemo_OnDeferred_m801123048_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Product_t1203687971 * L_0 = ___item0;
		NullCheck(L_0);
		ProductDefinition_t1942475268 * L_1 = Product_get_definition_m2035415516(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		String_t* L_2 = ProductDefinition_get_id_m264072292(L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral1205201122, L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void IAPDemo::InitUI(System.Collections.Generic.IEnumerable`1<UnityEngine.Purchasing.Product>)
extern Il2CppClass* IEnumerable_1_t1495815016_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_1_t2974179094_il2cpp_TypeInfo_var;
extern Il2CppClass* ProductType_t2754455291_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* OptionData_t2420267500_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_t1466026749_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern Il2CppClass* UnityAction_1_t3438463199_il2cpp_TypeInfo_var;
extern Il2CppClass* UnityAction_t4025899511_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_Add_m3486116920_MethodInfo_var;
extern const MethodInfo* IAPDemo_U3CInitUIU3Em__0_m3848246332_MethodInfo_var;
extern const MethodInfo* UnityAction_1__ctor_m4197675336_MethodInfo_var;
extern const MethodInfo* UnityEvent_1_AddListener_m404084168_MethodInfo_var;
extern const MethodInfo* IAPDemo_U3CInitUIU3Em__1_m905417126_MethodInfo_var;
extern const MethodInfo* IAPDemo_U3CInitUIU3Em__2_m905417093_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral4265067804;
extern const uint32_t IAPDemo_InitUI_m3350388155_MetadataUsageId;
extern "C"  void IAPDemo_InitUI_m3350388155 (IAPDemo_t823941185 * __this, Il2CppObject* ___items0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (IAPDemo_InitUI_m3350388155_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Product_t1203687971 * V_0 = NULL;
	Il2CppObject* V_1 = NULL;
	String_t* V_2 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Dropdown_t1985816271 * L_0 = IAPDemo_GetDropdown_m1658389538(__this, /*hidden argument*/NULL);
		__this->set_m_InteractableSelectable_6(L_0);
		int32_t L_1 = Application_get_platform_m3989224144(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((int32_t)L_1) == ((int32_t)8)))
		{
			goto IL_0033;
		}
	}
	{
		int32_t L_2 = Application_get_platform_m3989224144(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((int32_t)L_2) == ((int32_t)1)))
		{
			goto IL_0033;
		}
	}
	{
		Button_t2872111280 * L_3 = IAPDemo_GetRestoreButton_m3830435552(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		GameObject_t1756533147 * L_4 = Component_get_gameObject_m3105766835(L_3, /*hidden argument*/NULL);
		NullCheck(L_4);
		GameObject_SetActive_m2887581199(L_4, (bool)0, /*hidden argument*/NULL);
	}

IL_0033:
	{
		Il2CppObject* L_5 = ___items0;
		NullCheck(L_5);
		Il2CppObject* L_6 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<UnityEngine.Purchasing.Product>::GetEnumerator() */, IEnumerable_1_t1495815016_il2cpp_TypeInfo_var, L_5);
		V_1 = L_6;
	}

IL_003a:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0082;
		}

IL_003f:
		{
			Il2CppObject* L_7 = V_1;
			NullCheck(L_7);
			Product_t1203687971 * L_8 = InterfaceFuncInvoker0< Product_t1203687971 * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<UnityEngine.Purchasing.Product>::get_Current() */, IEnumerator_1_t2974179094_il2cpp_TypeInfo_var, L_7);
			V_0 = L_8;
			Product_t1203687971 * L_9 = V_0;
			NullCheck(L_9);
			ProductDefinition_t1942475268 * L_10 = Product_get_definition_m2035415516(L_9, /*hidden argument*/NULL);
			NullCheck(L_10);
			String_t* L_11 = ProductDefinition_get_id_m264072292(L_10, /*hidden argument*/NULL);
			Product_t1203687971 * L_12 = V_0;
			NullCheck(L_12);
			ProductDefinition_t1942475268 * L_13 = Product_get_definition_m2035415516(L_12, /*hidden argument*/NULL);
			NullCheck(L_13);
			int32_t L_14 = ProductDefinition_get_type_m590914665(L_13, /*hidden argument*/NULL);
			int32_t L_15 = L_14;
			Il2CppObject * L_16 = Box(ProductType_t2754455291_il2cpp_TypeInfo_var, &L_15);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_17 = String_Format_m1811873526(NULL /*static, unused*/, _stringLiteral4265067804, L_11, L_16, /*hidden argument*/NULL);
			V_2 = L_17;
			Dropdown_t1985816271 * L_18 = IAPDemo_GetDropdown_m1658389538(__this, /*hidden argument*/NULL);
			NullCheck(L_18);
			List_1_t1789388632 * L_19 = Dropdown_get_options_m2669836220(L_18, /*hidden argument*/NULL);
			String_t* L_20 = V_2;
			OptionData_t2420267500 * L_21 = (OptionData_t2420267500 *)il2cpp_codegen_object_new(OptionData_t2420267500_il2cpp_TypeInfo_var);
			OptionData__ctor_m743450704(L_21, L_20, /*hidden argument*/NULL);
			NullCheck(L_19);
			List_1_Add_m3486116920(L_19, L_21, /*hidden argument*/List_1_Add_m3486116920_MethodInfo_var);
		}

IL_0082:
		{
			Il2CppObject* L_22 = V_1;
			NullCheck(L_22);
			bool L_23 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, L_22);
			if (L_23)
			{
				goto IL_003f;
			}
		}

IL_008d:
		{
			IL2CPP_LEAVE(0x9D, FINALLY_0092);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0092;
	}

FINALLY_0092:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_24 = V_1;
			if (L_24)
			{
				goto IL_0096;
			}
		}

IL_0095:
		{
			IL2CPP_END_FINALLY(146)
		}

IL_0096:
		{
			Il2CppObject* L_25 = V_1;
			NullCheck(L_25);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, L_25);
			IL2CPP_END_FINALLY(146)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(146)
	{
		IL2CPP_JUMP_TBL(0x9D, IL_009d)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_009d:
	{
		Dropdown_t1985816271 * L_26 = IAPDemo_GetDropdown_m1658389538(__this, /*hidden argument*/NULL);
		NullCheck(L_26);
		Dropdown_RefreshShownValue_m3113581237(L_26, /*hidden argument*/NULL);
		Dropdown_t1985816271 * L_27 = IAPDemo_GetDropdown_m1658389538(__this, /*hidden argument*/NULL);
		NullCheck(L_27);
		DropdownEvent_t2203087800 * L_28 = Dropdown_get_onValueChanged_m3334401942(L_27, /*hidden argument*/NULL);
		IntPtr_t L_29;
		L_29.set_m_value_0((void*)(void*)IAPDemo_U3CInitUIU3Em__0_m3848246332_MethodInfo_var);
		UnityAction_1_t3438463199 * L_30 = (UnityAction_1_t3438463199 *)il2cpp_codegen_object_new(UnityAction_1_t3438463199_il2cpp_TypeInfo_var);
		UnityAction_1__ctor_m4197675336(L_30, __this, L_29, /*hidden argument*/UnityAction_1__ctor_m4197675336_MethodInfo_var);
		NullCheck(L_28);
		UnityEvent_1_AddListener_m404084168(L_28, L_30, /*hidden argument*/UnityEvent_1_AddListener_m404084168_MethodInfo_var);
		Button_t2872111280 * L_31 = IAPDemo_GetBuyButton_m514831370(__this, /*hidden argument*/NULL);
		NullCheck(L_31);
		ButtonClickedEvent_t2455055323 * L_32 = Button_get_onClick_m1595880935(L_31, /*hidden argument*/NULL);
		IntPtr_t L_33;
		L_33.set_m_value_0((void*)(void*)IAPDemo_U3CInitUIU3Em__1_m905417126_MethodInfo_var);
		UnityAction_t4025899511 * L_34 = (UnityAction_t4025899511 *)il2cpp_codegen_object_new(UnityAction_t4025899511_il2cpp_TypeInfo_var);
		UnityAction__ctor_m2649891629(L_34, __this, L_33, /*hidden argument*/NULL);
		NullCheck(L_32);
		UnityEvent_AddListener_m1596810379(L_32, L_34, /*hidden argument*/NULL);
		Button_t2872111280 * L_35 = IAPDemo_GetRestoreButton_m3830435552(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_36 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_35, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_36)
		{
			goto IL_010d;
		}
	}
	{
		Button_t2872111280 * L_37 = IAPDemo_GetRestoreButton_m3830435552(__this, /*hidden argument*/NULL);
		NullCheck(L_37);
		ButtonClickedEvent_t2455055323 * L_38 = Button_get_onClick_m1595880935(L_37, /*hidden argument*/NULL);
		IntPtr_t L_39;
		L_39.set_m_value_0((void*)(void*)IAPDemo_U3CInitUIU3Em__2_m905417093_MethodInfo_var);
		UnityAction_t4025899511 * L_40 = (UnityAction_t4025899511 *)il2cpp_codegen_object_new(UnityAction_t4025899511_il2cpp_TypeInfo_var);
		UnityAction__ctor_m2649891629(L_40, __this, L_39, /*hidden argument*/NULL);
		NullCheck(L_38);
		UnityEvent_AddListener_m1596810379(L_38, L_40, /*hidden argument*/NULL);
	}

IL_010d:
	{
		return;
	}
}
// System.Void IAPDemo::UpdateHistoryUI()
extern Il2CppClass* IStoreController_t92554892_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral676257233;
extern Il2CppCodeGenString* _stringLiteral1576255139;
extern Il2CppCodeGenString* _stringLiteral2162321594;
extern const uint32_t IAPDemo_UpdateHistoryUI_m2607563357_MetadataUsageId;
extern "C"  void IAPDemo_UpdateHistoryUI_m2607563357 (IAPDemo_t823941185 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (IAPDemo_UpdateHistoryUI_m2607563357_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	String_t* V_1 = NULL;
	int32_t V_2 = 0;
	Product_t1203687971 * V_3 = NULL;
	bool V_4 = false;
	{
		Il2CppObject * L_0 = __this->get_m_Controller_2();
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		V_0 = _stringLiteral676257233;
		V_1 = _stringLiteral1576255139;
		V_2 = 0;
		goto IL_0068;
	}

IL_001f:
	{
		Il2CppObject * L_1 = __this->get_m_Controller_2();
		NullCheck(L_1);
		ProductCollection_t3600019299 * L_2 = InterfaceFuncInvoker0< ProductCollection_t3600019299 * >::Invoke(0 /* UnityEngine.Purchasing.ProductCollection UnityEngine.Purchasing.IStoreController::get_products() */, IStoreController_t92554892_il2cpp_TypeInfo_var, L_1);
		NullCheck(L_2);
		ProductU5BU5D_t728099314* L_3 = ProductCollection_get_all_m3364167965(L_2, /*hidden argument*/NULL);
		int32_t L_4 = V_2;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		Product_t1203687971 * L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		V_3 = L_6;
		String_t* L_7 = V_0;
		Product_t1203687971 * L_8 = V_3;
		NullCheck(L_8);
		ProductDefinition_t1942475268 * L_9 = Product_get_definition_m2035415516(L_8, /*hidden argument*/NULL);
		NullCheck(L_9);
		String_t* L_10 = ProductDefinition_get_id_m264072292(L_9, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_11 = String_Concat_m612901809(NULL /*static, unused*/, L_7, _stringLiteral2162321594, L_10, /*hidden argument*/NULL);
		V_0 = L_11;
		String_t* L_12 = V_1;
		Product_t1203687971 * L_13 = V_3;
		NullCheck(L_13);
		bool L_14 = Product_get_hasReceipt_m617723237(L_13, /*hidden argument*/NULL);
		V_4 = L_14;
		String_t* L_15 = Boolean_ToString_m1253164328((&V_4), /*hidden argument*/NULL);
		String_t* L_16 = String_Concat_m612901809(NULL /*static, unused*/, L_12, _stringLiteral2162321594, L_15, /*hidden argument*/NULL);
		V_1 = L_16;
		int32_t L_17 = V_2;
		V_2 = ((int32_t)((int32_t)L_17+(int32_t)1));
	}

IL_0068:
	{
		int32_t L_18 = V_2;
		Il2CppObject * L_19 = __this->get_m_Controller_2();
		NullCheck(L_19);
		ProductCollection_t3600019299 * L_20 = InterfaceFuncInvoker0< ProductCollection_t3600019299 * >::Invoke(0 /* UnityEngine.Purchasing.ProductCollection UnityEngine.Purchasing.IStoreController::get_products() */, IStoreController_t92554892_il2cpp_TypeInfo_var, L_19);
		NullCheck(L_20);
		ProductU5BU5D_t728099314* L_21 = ProductCollection_get_all_m3364167965(L_20, /*hidden argument*/NULL);
		NullCheck(L_21);
		if ((((int32_t)L_18) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_21)->max_length)))))))
		{
			goto IL_001f;
		}
	}
	{
		Text_t356221433 * L_22 = IAPDemo_GetText_m3382056873(__this, (bool)0, /*hidden argument*/NULL);
		String_t* L_23 = V_0;
		NullCheck(L_22);
		VirtActionInvoker1< String_t* >::Invoke(74 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_22, L_23);
		Text_t356221433 * L_24 = IAPDemo_GetText_m3382056873(__this, (bool)1, /*hidden argument*/NULL);
		String_t* L_25 = V_1;
		NullCheck(L_24);
		VirtActionInvoker1< String_t* >::Invoke(74 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_24, L_25);
		return;
	}
}
// System.Void IAPDemo::UpdateInteractable()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t IAPDemo_UpdateInteractable_m2228859499_MetadataUsageId;
extern "C"  void IAPDemo_UpdateInteractable_m2228859499 (IAPDemo_t823941185 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (IAPDemo_UpdateInteractable_m2228859499_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	{
		Selectable_t1490392188 * L_0 = __this->get_m_InteractableSelectable_6();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0012;
		}
	}
	{
		return;
	}

IL_0012:
	{
		Il2CppObject * L_2 = __this->get_m_Controller_2();
		V_0 = (bool)((((int32_t)((((Il2CppObject*)(Il2CppObject *)L_2) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_3 = V_0;
		Selectable_t1490392188 * L_4 = __this->get_m_InteractableSelectable_6();
		NullCheck(L_4);
		bool L_5 = Selectable_get_interactable_m1725029500(L_4, /*hidden argument*/NULL);
		if ((((int32_t)L_3) == ((int32_t)L_5)))
		{
			goto IL_0065;
		}
	}
	{
		Button_t2872111280 * L_6 = IAPDemo_GetRestoreButton_m3830435552(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_7 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_6, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		Button_t2872111280 * L_8 = IAPDemo_GetRestoreButton_m3830435552(__this, /*hidden argument*/NULL);
		bool L_9 = V_0;
		NullCheck(L_8);
		Selectable_set_interactable_m63718297(L_8, L_9, /*hidden argument*/NULL);
	}

IL_004d:
	{
		Button_t2872111280 * L_10 = IAPDemo_GetBuyButton_m514831370(__this, /*hidden argument*/NULL);
		bool L_11 = V_0;
		NullCheck(L_10);
		Selectable_set_interactable_m63718297(L_10, L_11, /*hidden argument*/NULL);
		Dropdown_t1985816271 * L_12 = IAPDemo_GetDropdown_m1658389538(__this, /*hidden argument*/NULL);
		bool L_13 = V_0;
		NullCheck(L_12);
		Selectable_set_interactable_m63718297(L_12, L_13, /*hidden argument*/NULL);
	}

IL_0065:
	{
		return;
	}
}
// System.Void IAPDemo::Update()
extern "C"  void IAPDemo_Update_m412123535 (IAPDemo_t823941185 * __this, const MethodInfo* method)
{
	{
		IAPDemo_UpdateInteractable_m2228859499(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.UI.Dropdown IAPDemo::GetDropdown()
extern const MethodInfo* GameObject_GetComponent_TisDropdown_t1985816271_m1750975685_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1625601915;
extern const uint32_t IAPDemo_GetDropdown_m1658389538_MetadataUsageId;
extern "C"  Dropdown_t1985816271 * IAPDemo_GetDropdown_m1658389538 (IAPDemo_t823941185 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (IAPDemo_GetDropdown_m1658389538_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral1625601915, /*hidden argument*/NULL);
		NullCheck(L_0);
		Dropdown_t1985816271 * L_1 = GameObject_GetComponent_TisDropdown_t1985816271_m1750975685(L_0, /*hidden argument*/GameObject_GetComponent_TisDropdown_t1985816271_m1750975685_MethodInfo_var);
		return L_1;
	}
}
// UnityEngine.UI.Button IAPDemo::GetBuyButton()
extern const MethodInfo* GameObject_GetComponent_TisButton_t2872111280_m3862106414_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2309168590;
extern const uint32_t IAPDemo_GetBuyButton_m514831370_MetadataUsageId;
extern "C"  Button_t2872111280 * IAPDemo_GetBuyButton_m514831370 (IAPDemo_t823941185 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (IAPDemo_GetBuyButton_m514831370_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral2309168590, /*hidden argument*/NULL);
		NullCheck(L_0);
		Button_t2872111280 * L_1 = GameObject_GetComponent_TisButton_t2872111280_m3862106414(L_0, /*hidden argument*/GameObject_GetComponent_TisButton_t2872111280_m3862106414_MethodInfo_var);
		return L_1;
	}
}
// UnityEngine.UI.Button IAPDemo::GetRestoreButton()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisButton_t2872111280_m3862106414_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1335657580;
extern const uint32_t IAPDemo_GetRestoreButton_m3830435552_MetadataUsageId;
extern "C"  Button_t2872111280 * IAPDemo_GetRestoreButton_m3830435552 (IAPDemo_t823941185 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (IAPDemo_GetRestoreButton_m3830435552_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t1756533147 * V_0 = NULL;
	{
		GameObject_t1756533147 * L_0 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral1335657580, /*hidden argument*/NULL);
		V_0 = L_0;
		GameObject_t1756533147 * L_1 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_1, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_001e;
		}
	}
	{
		GameObject_t1756533147 * L_3 = V_0;
		NullCheck(L_3);
		Button_t2872111280 * L_4 = GameObject_GetComponent_TisButton_t2872111280_m3862106414(L_3, /*hidden argument*/GameObject_GetComponent_TisButton_t2872111280_m3862106414_MethodInfo_var);
		return L_4;
	}

IL_001e:
	{
		return (Button_t2872111280 *)NULL;
	}
}
// UnityEngine.UI.Text IAPDemo::GetText(System.Boolean)
extern const MethodInfo* GameObject_GetComponent_TisText_t356221433_m4280536079_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3611362783;
extern Il2CppCodeGenString* _stringLiteral3611362761;
extern const uint32_t IAPDemo_GetText_m3382056873_MetadataUsageId;
extern "C"  Text_t356221433 * IAPDemo_GetText_m3382056873 (IAPDemo_t823941185 * __this, bool ___right0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (IAPDemo_GetText_m3382056873_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	String_t* G_B3_0 = NULL;
	{
		bool L_0 = ___right0;
		if (!L_0)
		{
			goto IL_0010;
		}
	}
	{
		G_B3_0 = _stringLiteral3611362783;
		goto IL_0015;
	}

IL_0010:
	{
		G_B3_0 = _stringLiteral3611362761;
	}

IL_0015:
	{
		V_0 = G_B3_0;
		String_t* L_1 = V_0;
		GameObject_t1756533147 * L_2 = GameObject_Find_m836511350(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		NullCheck(L_2);
		Text_t356221433 * L_3 = GameObject_GetComponent_TisText_t356221433_m4280536079(L_2, /*hidden argument*/GameObject_GetComponent_TisText_t356221433_m4280536079_MethodInfo_var);
		return L_3;
	}
}
// System.Void IAPDemo::<InitUI>m__0(System.Int32)
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral253251037;
extern const uint32_t IAPDemo_U3CInitUIU3Em__0_m3848246332_MetadataUsageId;
extern "C"  void IAPDemo_U3CInitUIU3Em__0_m3848246332 (IAPDemo_t823941185 * __this, int32_t ___selectedItem0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (IAPDemo_U3CInitUIU3Em__0_m3848246332_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___selectedItem0;
		int32_t L_1 = L_0;
		Il2CppObject * L_2 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_1);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = String_Concat_m56707527(NULL /*static, unused*/, _stringLiteral253251037, L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		int32_t L_4 = ___selectedItem0;
		__this->set_m_SelectedItemIndex_4(L_4);
		return;
	}
}
// System.Void IAPDemo::<InitUI>m__1()
extern Il2CppClass* IStoreController_t92554892_il2cpp_TypeInfo_var;
extern const uint32_t IAPDemo_U3CInitUIU3Em__1_m905417126_MetadataUsageId;
extern "C"  void IAPDemo_U3CInitUIU3Em__1_m905417126 (IAPDemo_t823941185 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (IAPDemo_U3CInitUIU3Em__1_m905417126_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = __this->get_m_PurchaseInProgress_5();
		if (!L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		Il2CppObject * L_1 = __this->get_m_Controller_2();
		Il2CppObject * L_2 = __this->get_m_Controller_2();
		NullCheck(L_2);
		ProductCollection_t3600019299 * L_3 = InterfaceFuncInvoker0< ProductCollection_t3600019299 * >::Invoke(0 /* UnityEngine.Purchasing.ProductCollection UnityEngine.Purchasing.IStoreController::get_products() */, IStoreController_t92554892_il2cpp_TypeInfo_var, L_2);
		NullCheck(L_3);
		ProductU5BU5D_t728099314* L_4 = ProductCollection_get_all_m3364167965(L_3, /*hidden argument*/NULL);
		int32_t L_5 = __this->get_m_SelectedItemIndex_4();
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		int32_t L_6 = L_5;
		Product_t1203687971 * L_7 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		NullCheck(L_1);
		InterfaceActionInvoker1< Product_t1203687971 * >::Invoke(1 /* System.Void UnityEngine.Purchasing.IStoreController::InitiatePurchase(UnityEngine.Purchasing.Product) */, IStoreController_t92554892_il2cpp_TypeInfo_var, L_1, L_7);
		__this->set_m_PurchaseInProgress_5((bool)1);
		return;
	}
}
// System.Void IAPDemo::<InitUI>m__2()
extern Il2CppClass* Action_1_t3627374100_il2cpp_TypeInfo_var;
extern Il2CppClass* IAppleExtensions_t1627764765_il2cpp_TypeInfo_var;
extern const MethodInfo* IAPDemo_OnTransactionsRestored_m2633548551_MethodInfo_var;
extern const MethodInfo* Action_1__ctor_m782011149_MethodInfo_var;
extern const uint32_t IAPDemo_U3CInitUIU3Em__2_m905417093_MetadataUsageId;
extern "C"  void IAPDemo_U3CInitUIU3Em__2_m905417093 (IAPDemo_t823941185 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (IAPDemo_U3CInitUIU3Em__2_m905417093_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = __this->get_m_AppleExtensions_3();
		IntPtr_t L_1;
		L_1.set_m_value_0((void*)(void*)IAPDemo_OnTransactionsRestored_m2633548551_MethodInfo_var);
		Action_1_t3627374100 * L_2 = (Action_1_t3627374100 *)il2cpp_codegen_object_new(Action_1_t3627374100_il2cpp_TypeInfo_var);
		Action_1__ctor_m782011149(L_2, __this, L_1, /*hidden argument*/Action_1__ctor_m782011149_MethodInfo_var);
		NullCheck(L_0);
		InterfaceActionInvoker1< Action_1_t3627374100 * >::Invoke(0 /* System.Void UnityEngine.Purchasing.IAppleExtensions::RestoreTransactions(System.Action`1<System.Boolean>) */, IAppleExtensions_t1627764765_il2cpp_TypeInfo_var, L_0, L_2);
		return;
	}
}
// System.Void UnityEngine.Purchasing.Security.AppleTangle::.ctor()
extern "C"  void AppleTangle__ctor_m1226795583 (AppleTangle_t53875121 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Purchasing.Security.AppleTangle::.cctor()
extern Il2CppClass* Convert_t2607082565_il2cpp_TypeInfo_var;
extern Il2CppClass* AppleTangle_t53875121_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32U5BU5D_t3030399641_il2cpp_TypeInfo_var;
extern FieldInfo* U3CPrivateImplementationDetailsU3E_t1486305144____U24U24fieldU2D0_0_FieldInfo_var;
extern Il2CppCodeGenString* _stringLiteral3538107748;
extern const uint32_t AppleTangle__cctor_m3030347248_MetadataUsageId;
extern "C"  void AppleTangle__cctor_m3030347248 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AppleTangle__cctor_m3030347248_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t2607082565_il2cpp_TypeInfo_var);
		ByteU5BU5D_t3397334013* L_0 = Convert_FromBase64String_m3629466114(NULL /*static, unused*/, _stringLiteral3538107748, /*hidden argument*/NULL);
		((AppleTangle_t53875121_StaticFields*)AppleTangle_t53875121_il2cpp_TypeInfo_var->static_fields)->set_data_0(L_0);
		Int32U5BU5D_t3030399641* L_1 = ((Int32U5BU5D_t3030399641*)SZArrayNew(Int32U5BU5D_t3030399641_il2cpp_TypeInfo_var, (uint32_t)((int32_t)61)));
		RuntimeHelpers_InitializeArray_m3920580167(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_1, LoadFieldToken(U3CPrivateImplementationDetailsU3E_t1486305144____U24U24fieldU2D0_0_FieldInfo_var), /*hidden argument*/NULL);
		((AppleTangle_t53875121_StaticFields*)AppleTangle_t53875121_il2cpp_TypeInfo_var->static_fields)->set_order_1(L_1);
		((AppleTangle_t53875121_StaticFields*)AppleTangle_t53875121_il2cpp_TypeInfo_var->static_fields)->set_key_2(((int32_t)154));
		return;
	}
}
// System.Byte[] UnityEngine.Purchasing.Security.AppleTangle::Data()
extern Il2CppClass* AppleTangle_t53875121_il2cpp_TypeInfo_var;
extern const uint32_t AppleTangle_Data_m3366264679_MetadataUsageId;
extern "C"  ByteU5BU5D_t3397334013* AppleTangle_Data_m3366264679 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AppleTangle_Data_m3366264679_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(AppleTangle_t53875121_il2cpp_TypeInfo_var);
		ByteU5BU5D_t3397334013* L_0 = ((AppleTangle_t53875121_StaticFields*)AppleTangle_t53875121_il2cpp_TypeInfo_var->static_fields)->get_data_0();
		Int32U5BU5D_t3030399641* L_1 = ((AppleTangle_t53875121_StaticFields*)AppleTangle_t53875121_il2cpp_TypeInfo_var->static_fields)->get_order_1();
		int32_t L_2 = ((AppleTangle_t53875121_StaticFields*)AppleTangle_t53875121_il2cpp_TypeInfo_var->static_fields)->get_key_2();
		ByteU5BU5D_t3397334013* L_3 = Obfuscator_DeObfuscate_m4253116256(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Void UnityEngine.Purchasing.Security.GooglePlayTangle::.ctor()
extern "C"  void GooglePlayTangle__ctor_m1761951308 (GooglePlayTangle_t2749524914 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Purchasing.Security.GooglePlayTangle::.cctor()
extern Il2CppClass* Convert_t2607082565_il2cpp_TypeInfo_var;
extern Il2CppClass* GooglePlayTangle_t2749524914_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32U5BU5D_t3030399641_il2cpp_TypeInfo_var;
extern FieldInfo* U3CPrivateImplementationDetailsU3E_t1486305144____U24U24fieldU2D1_1_FieldInfo_var;
extern Il2CppCodeGenString* _stringLiteral1844317718;
extern const uint32_t GooglePlayTangle__cctor_m4030200525_MetadataUsageId;
extern "C"  void GooglePlayTangle__cctor_m4030200525 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GooglePlayTangle__cctor_m4030200525_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t2607082565_il2cpp_TypeInfo_var);
		ByteU5BU5D_t3397334013* L_0 = Convert_FromBase64String_m3629466114(NULL /*static, unused*/, _stringLiteral1844317718, /*hidden argument*/NULL);
		((GooglePlayTangle_t2749524914_StaticFields*)GooglePlayTangle_t2749524914_il2cpp_TypeInfo_var->static_fields)->set_data_0(L_0);
		Int32U5BU5D_t3030399641* L_1 = ((Int32U5BU5D_t3030399641*)SZArrayNew(Int32U5BU5D_t3030399641_il2cpp_TypeInfo_var, (uint32_t)((int32_t)15)));
		RuntimeHelpers_InitializeArray_m3920580167(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_1, LoadFieldToken(U3CPrivateImplementationDetailsU3E_t1486305144____U24U24fieldU2D1_1_FieldInfo_var), /*hidden argument*/NULL);
		((GooglePlayTangle_t2749524914_StaticFields*)GooglePlayTangle_t2749524914_il2cpp_TypeInfo_var->static_fields)->set_order_1(L_1);
		((GooglePlayTangle_t2749524914_StaticFields*)GooglePlayTangle_t2749524914_il2cpp_TypeInfo_var->static_fields)->set_key_2(((int32_t)95));
		return;
	}
}
// System.Byte[] UnityEngine.Purchasing.Security.GooglePlayTangle::Data()
extern Il2CppClass* GooglePlayTangle_t2749524914_il2cpp_TypeInfo_var;
extern const uint32_t GooglePlayTangle_Data_m2557246410_MetadataUsageId;
extern "C"  ByteU5BU5D_t3397334013* GooglePlayTangle_Data_m2557246410 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GooglePlayTangle_Data_m2557246410_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GooglePlayTangle_t2749524914_il2cpp_TypeInfo_var);
		ByteU5BU5D_t3397334013* L_0 = ((GooglePlayTangle_t2749524914_StaticFields*)GooglePlayTangle_t2749524914_il2cpp_TypeInfo_var->static_fields)->get_data_0();
		Int32U5BU5D_t3030399641* L_1 = ((GooglePlayTangle_t2749524914_StaticFields*)GooglePlayTangle_t2749524914_il2cpp_TypeInfo_var->static_fields)->get_order_1();
		int32_t L_2 = ((GooglePlayTangle_t2749524914_StaticFields*)GooglePlayTangle_t2749524914_il2cpp_TypeInfo_var->static_fields)->get_key_2();
		ByteU5BU5D_t3397334013* L_3 = Obfuscator_DeObfuscate_m4253116256(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
