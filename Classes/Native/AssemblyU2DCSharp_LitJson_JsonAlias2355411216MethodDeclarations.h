﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LitJson.JsonAlias
struct JsonAlias_t2355411216;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void LitJson.JsonAlias::.ctor(System.String,System.Boolean)
extern "C"  void JsonAlias__ctor_m974916411 (JsonAlias_t2355411216 * __this, String_t* ___aliasName0, bool ___acceptOriginalName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String LitJson.JsonAlias::get_Alias()
extern "C"  String_t* JsonAlias_get_Alias_m3219225392 (JsonAlias_t2355411216 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LitJson.JsonAlias::set_Alias(System.String)
extern "C"  void JsonAlias_set_Alias_m4228129623 (JsonAlias_t2355411216 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean LitJson.JsonAlias::get_AcceptOriginal()
extern "C"  bool JsonAlias_get_AcceptOriginal_m144626088 (JsonAlias_t2355411216 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LitJson.JsonAlias::set_AcceptOriginal(System.Boolean)
extern "C"  void JsonAlias_set_AcceptOriginal_m2350122197 (JsonAlias_t2355411216 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
