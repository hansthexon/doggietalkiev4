﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// System.Collections.Generic.IList`1<System.Collections.Generic.KeyValuePair`2<System.String,LitJson.JsonData>>
struct IList_1_t482332659;

#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_LitJson_JsonType3145703806.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LitJson.JsonData
struct  JsonData_t269267574  : public Il2CppObject
{
public:
	// System.Object LitJson.JsonData::val
	Il2CppObject * ___val_0;
	// System.String LitJson.JsonData::json
	String_t* ___json_1;
	// LitJson.JsonType LitJson.JsonData::type
	int32_t ___type_2;
	// System.Collections.Generic.IList`1<System.Collections.Generic.KeyValuePair`2<System.String,LitJson.JsonData>> LitJson.JsonData::list
	Il2CppObject* ___list_3;

public:
	inline static int32_t get_offset_of_val_0() { return static_cast<int32_t>(offsetof(JsonData_t269267574, ___val_0)); }
	inline Il2CppObject * get_val_0() const { return ___val_0; }
	inline Il2CppObject ** get_address_of_val_0() { return &___val_0; }
	inline void set_val_0(Il2CppObject * value)
	{
		___val_0 = value;
		Il2CppCodeGenWriteBarrier(&___val_0, value);
	}

	inline static int32_t get_offset_of_json_1() { return static_cast<int32_t>(offsetof(JsonData_t269267574, ___json_1)); }
	inline String_t* get_json_1() const { return ___json_1; }
	inline String_t** get_address_of_json_1() { return &___json_1; }
	inline void set_json_1(String_t* value)
	{
		___json_1 = value;
		Il2CppCodeGenWriteBarrier(&___json_1, value);
	}

	inline static int32_t get_offset_of_type_2() { return static_cast<int32_t>(offsetof(JsonData_t269267574, ___type_2)); }
	inline int32_t get_type_2() const { return ___type_2; }
	inline int32_t* get_address_of_type_2() { return &___type_2; }
	inline void set_type_2(int32_t value)
	{
		___type_2 = value;
	}

	inline static int32_t get_offset_of_list_3() { return static_cast<int32_t>(offsetof(JsonData_t269267574, ___list_3)); }
	inline Il2CppObject* get_list_3() const { return ___list_3; }
	inline Il2CppObject** get_address_of_list_3() { return &___list_3; }
	inline void set_list_3(Il2CppObject* value)
	{
		___list_3 = value;
		Il2CppCodeGenWriteBarrier(&___list_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
