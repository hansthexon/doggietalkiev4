﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Attribute542643598.h"
#include "AssemblyU2DCSharp_LitJson_JsonIgnoreWhen1352154806.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LitJson.JsonIgnore
struct  JsonIgnore_t466986466  : public Attribute_t542643598
{
public:
	// LitJson.JsonIgnoreWhen LitJson.JsonIgnore::<Usage>k__BackingField
	int32_t ___U3CUsageU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CUsageU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(JsonIgnore_t466986466, ___U3CUsageU3Ek__BackingField_0)); }
	inline int32_t get_U3CUsageU3Ek__BackingField_0() const { return ___U3CUsageU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3CUsageU3Ek__BackingField_0() { return &___U3CUsageU3Ek__BackingField_0; }
	inline void set_U3CUsageU3Ek__BackingField_0(int32_t value)
	{
		___U3CUsageU3Ek__BackingField_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
