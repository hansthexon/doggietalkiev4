﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LitJson.JsonIgnore
struct JsonIgnore_t466986466;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_LitJson_JsonIgnoreWhen1352154806.h"

// System.Void LitJson.JsonIgnore::.ctor()
extern "C"  void JsonIgnore__ctor_m1776444634 (JsonIgnore_t466986466 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LitJson.JsonIgnore::.ctor(LitJson.JsonIgnoreWhen)
extern "C"  void JsonIgnore__ctor_m3468778607 (JsonIgnore_t466986466 * __this, int32_t ___usage0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// LitJson.JsonIgnoreWhen LitJson.JsonIgnore::get_Usage()
extern "C"  int32_t JsonIgnore_get_Usage_m3643486788 (JsonIgnore_t466986466 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LitJson.JsonIgnore::set_Usage(LitJson.JsonIgnoreWhen)
extern "C"  void JsonIgnore_set_Usage_m1206864595 (JsonIgnore_t466986466 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
