﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SQLite4Unity3d.PrimaryKeyAttribute
struct PrimaryKeyAttribute_t1888986135;

#include "codegen/il2cpp-codegen.h"

// System.Void SQLite4Unity3d.PrimaryKeyAttribute::.ctor()
extern "C"  void PrimaryKeyAttribute__ctor_m4150556890 (PrimaryKeyAttribute_t1888986135 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
