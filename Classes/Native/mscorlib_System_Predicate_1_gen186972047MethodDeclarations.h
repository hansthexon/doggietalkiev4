﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Predicate_1_gen2159220209MethodDeclarations.h"

// System.Void System.Predicate`1<System.Collections.Generic.KeyValuePair`2<System.String,System.Int32>>::.ctor(System.Object,System.IntPtr)
#define Predicate_1__ctor_m3922308223(__this, ___object0, ___method1, method) ((  void (*) (Predicate_1_t186972047 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Predicate_1__ctor_m2453545459_gshared)(__this, ___object0, ___method1, method)
// System.Boolean System.Predicate`1<System.Collections.Generic.KeyValuePair`2<System.String,System.Int32>>::Invoke(T)
#define Predicate_1_Invoke_m3173165967(__this, ___obj0, method) ((  bool (*) (Predicate_1_t186972047 *, KeyValuePair_2_t1744001932 , const MethodInfo*))Predicate_1_Invoke_m1795385227_gshared)(__this, ___obj0, method)
// System.IAsyncResult System.Predicate`1<System.Collections.Generic.KeyValuePair`2<System.String,System.Int32>>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Predicate_1_BeginInvoke_m1910774148(__this, ___obj0, ___callback1, ___object2, method) ((  Il2CppObject * (*) (Predicate_1_t186972047 *, KeyValuePair_2_t1744001932 , AsyncCallback_t163412349 *, Il2CppObject *, const MethodInfo*))Predicate_1_BeginInvoke_m159570650_gshared)(__this, ___obj0, ___callback1, ___object2, method)
// System.Boolean System.Predicate`1<System.Collections.Generic.KeyValuePair`2<System.String,System.Int32>>::EndInvoke(System.IAsyncResult)
#define Predicate_1_EndInvoke_m316384973(__this, ___result0, method) ((  bool (*) (Predicate_1_t186972047 *, Il2CppObject *, const MethodInfo*))Predicate_1_EndInvoke_m3779329705_gshared)(__this, ___result0, method)
