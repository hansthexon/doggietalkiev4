﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LipingShare.LCLib.Asn1Processor.Oid
struct Oid_t113668572;
// System.String
struct String_t;
// System.Byte[]
struct ByteU5BU5D_t3397334013;
// System.IO.Stream
struct Stream_t3255436806;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_IO_Stream3255436806.h"

// System.Void LipingShare.LCLib.Asn1Processor.Oid::.ctor()
extern "C"  void Oid__ctor_m3384108428 (Oid_t113668572 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String LipingShare.LCLib.Asn1Processor.Oid::GetOidName(System.String)
extern "C"  String_t* Oid_GetOidName_m1983955852 (Oid_t113668572 * __this, String_t* ___inOidStr0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String LipingShare.LCLib.Asn1Processor.Oid::Decode(System.Byte[])
extern "C"  String_t* Oid_Decode_m2891545618 (Oid_t113668572 * __this, ByteU5BU5D_t3397334013* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String LipingShare.LCLib.Asn1Processor.Oid::Decode(System.IO.Stream)
extern "C"  String_t* Oid_Decode_m2891299360 (Oid_t113668572 * __this, Stream_t3255436806 * ___bt0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 LipingShare.LCLib.Asn1Processor.Oid::DecodeValue(System.IO.Stream,System.UInt64&)
extern "C"  int32_t Oid_DecodeValue_m1983536933 (Oid_t113668572 * __this, Stream_t3255436806 * ___bt0, uint64_t* ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LipingShare.LCLib.Asn1Processor.Oid::.cctor()
extern "C"  void Oid__cctor_m4076678605 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
