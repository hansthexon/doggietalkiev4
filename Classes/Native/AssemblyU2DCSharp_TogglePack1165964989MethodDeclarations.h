﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TogglePack
struct TogglePack_t1165964989;

#include "codegen/il2cpp-codegen.h"

// System.Void TogglePack::.ctor()
extern "C"  void TogglePack__ctor_m466750328 (TogglePack_t1165964989 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TogglePack::Start()
extern "C"  void TogglePack_Start_m510040744 (TogglePack_t1165964989 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TogglePack::SelectOneOption()
extern "C"  void TogglePack_SelectOneOption_m2635486935 (TogglePack_t1165964989 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TogglePack::ThreeSelected(System.Boolean)
extern "C"  void TogglePack_ThreeSelected_m1886047416 (TogglePack_t1165964989 * __this, bool ___val0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TogglePack::TwoSelected(System.Boolean)
extern "C"  void TogglePack_TwoSelected_m3663965810 (TogglePack_t1165964989 * __this, bool ___val0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
