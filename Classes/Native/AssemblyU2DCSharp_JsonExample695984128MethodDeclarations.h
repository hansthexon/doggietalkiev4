﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// JsonExample
struct JsonExample_t695984128;

#include "codegen/il2cpp-codegen.h"

// System.Void JsonExample::.ctor()
extern "C"  void JsonExample__ctor_m3515739639 (JsonExample_t695984128 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JsonExample::Start()
extern "C"  void JsonExample_Start_m197240155 (JsonExample_t695984128 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
