﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3866418389.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23007666127.h"

// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m49278889_gshared (InternalEnumerator_1_t3866418389 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m49278889(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t3866418389 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m49278889_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2516123773_gshared (InternalEnumerator_1_t3866418389 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2516123773(__this, method) ((  void (*) (InternalEnumerator_1_t3866418389 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2516123773_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m696238645_gshared (InternalEnumerator_1_t3866418389 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m696238645(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t3866418389 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m696238645_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3100772778_gshared (InternalEnumerator_1_t3866418389 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m3100772778(__this, method) ((  void (*) (InternalEnumerator_1_t3866418389 *, const MethodInfo*))InternalEnumerator_1_Dispose_m3100772778_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m507230133_gshared (InternalEnumerator_1_t3866418389 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m507230133(__this, method) ((  bool (*) (InternalEnumerator_1_t3866418389 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m507230133_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>>::get_Current()
extern "C"  KeyValuePair_2_t3007666127  InternalEnumerator_1_get_Current_m2132140880_gshared (InternalEnumerator_1_t3866418389 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m2132140880(__this, method) ((  KeyValuePair_2_t3007666127  (*) (InternalEnumerator_1_t3866418389 *, const MethodInfo*))InternalEnumerator_1_get_Current_m2132140880_gshared)(__this, method)
