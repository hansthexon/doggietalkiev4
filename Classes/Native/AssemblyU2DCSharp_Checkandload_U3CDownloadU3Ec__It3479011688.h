﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.WWW
struct WWW_t2919945039;
// CodeTest[]
struct CodeTestU5BU5D_t2090683080;
// CodeTest
struct CodeTest_t2096217477;
// System.Collections.Generic.IEnumerator`1<Code>
struct IEnumerator_1_t2456658634;
// Code
struct Code_t686167511;
// System.Object
struct Il2CppObject;
// Checkandload
struct Checkandload_t1170646197;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Checkandload/<Download>c__Iterator5
struct  U3CDownloadU3Ec__Iterator5_t3479011688  : public Il2CppObject
{
public:
	// UnityEngine.WWW Checkandload/<Download>c__Iterator5::<www>__0
	WWW_t2919945039 * ___U3CwwwU3E__0_0;
	// CodeTest[] Checkandload/<Download>c__Iterator5::<$s_17>__1
	CodeTestU5BU5D_t2090683080* ___U3CU24s_17U3E__1_1;
	// System.Int32 Checkandload/<Download>c__Iterator5::<$s_18>__2
	int32_t ___U3CU24s_18U3E__2_2;
	// CodeTest Checkandload/<Download>c__Iterator5::<v>__3
	CodeTest_t2096217477 * ___U3CvU3E__3_3;
	// System.Collections.Generic.IEnumerator`1<Code> Checkandload/<Download>c__Iterator5::<$s_19>__4
	Il2CppObject* ___U3CU24s_19U3E__4_4;
	// Code Checkandload/<Download>c__Iterator5::<c>__5
	Code_t686167511 * ___U3CcU3E__5_5;
	// System.Int32 Checkandload/<Download>c__Iterator5::$PC
	int32_t ___U24PC_6;
	// System.Object Checkandload/<Download>c__Iterator5::$current
	Il2CppObject * ___U24current_7;
	// Checkandload Checkandload/<Download>c__Iterator5::<>f__this
	Checkandload_t1170646197 * ___U3CU3Ef__this_8;

public:
	inline static int32_t get_offset_of_U3CwwwU3E__0_0() { return static_cast<int32_t>(offsetof(U3CDownloadU3Ec__Iterator5_t3479011688, ___U3CwwwU3E__0_0)); }
	inline WWW_t2919945039 * get_U3CwwwU3E__0_0() const { return ___U3CwwwU3E__0_0; }
	inline WWW_t2919945039 ** get_address_of_U3CwwwU3E__0_0() { return &___U3CwwwU3E__0_0; }
	inline void set_U3CwwwU3E__0_0(WWW_t2919945039 * value)
	{
		___U3CwwwU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CwwwU3E__0_0, value);
	}

	inline static int32_t get_offset_of_U3CU24s_17U3E__1_1() { return static_cast<int32_t>(offsetof(U3CDownloadU3Ec__Iterator5_t3479011688, ___U3CU24s_17U3E__1_1)); }
	inline CodeTestU5BU5D_t2090683080* get_U3CU24s_17U3E__1_1() const { return ___U3CU24s_17U3E__1_1; }
	inline CodeTestU5BU5D_t2090683080** get_address_of_U3CU24s_17U3E__1_1() { return &___U3CU24s_17U3E__1_1; }
	inline void set_U3CU24s_17U3E__1_1(CodeTestU5BU5D_t2090683080* value)
	{
		___U3CU24s_17U3E__1_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24s_17U3E__1_1, value);
	}

	inline static int32_t get_offset_of_U3CU24s_18U3E__2_2() { return static_cast<int32_t>(offsetof(U3CDownloadU3Ec__Iterator5_t3479011688, ___U3CU24s_18U3E__2_2)); }
	inline int32_t get_U3CU24s_18U3E__2_2() const { return ___U3CU24s_18U3E__2_2; }
	inline int32_t* get_address_of_U3CU24s_18U3E__2_2() { return &___U3CU24s_18U3E__2_2; }
	inline void set_U3CU24s_18U3E__2_2(int32_t value)
	{
		___U3CU24s_18U3E__2_2 = value;
	}

	inline static int32_t get_offset_of_U3CvU3E__3_3() { return static_cast<int32_t>(offsetof(U3CDownloadU3Ec__Iterator5_t3479011688, ___U3CvU3E__3_3)); }
	inline CodeTest_t2096217477 * get_U3CvU3E__3_3() const { return ___U3CvU3E__3_3; }
	inline CodeTest_t2096217477 ** get_address_of_U3CvU3E__3_3() { return &___U3CvU3E__3_3; }
	inline void set_U3CvU3E__3_3(CodeTest_t2096217477 * value)
	{
		___U3CvU3E__3_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CvU3E__3_3, value);
	}

	inline static int32_t get_offset_of_U3CU24s_19U3E__4_4() { return static_cast<int32_t>(offsetof(U3CDownloadU3Ec__Iterator5_t3479011688, ___U3CU24s_19U3E__4_4)); }
	inline Il2CppObject* get_U3CU24s_19U3E__4_4() const { return ___U3CU24s_19U3E__4_4; }
	inline Il2CppObject** get_address_of_U3CU24s_19U3E__4_4() { return &___U3CU24s_19U3E__4_4; }
	inline void set_U3CU24s_19U3E__4_4(Il2CppObject* value)
	{
		___U3CU24s_19U3E__4_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24s_19U3E__4_4, value);
	}

	inline static int32_t get_offset_of_U3CcU3E__5_5() { return static_cast<int32_t>(offsetof(U3CDownloadU3Ec__Iterator5_t3479011688, ___U3CcU3E__5_5)); }
	inline Code_t686167511 * get_U3CcU3E__5_5() const { return ___U3CcU3E__5_5; }
	inline Code_t686167511 ** get_address_of_U3CcU3E__5_5() { return &___U3CcU3E__5_5; }
	inline void set_U3CcU3E__5_5(Code_t686167511 * value)
	{
		___U3CcU3E__5_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CcU3E__5_5, value);
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CDownloadU3Ec__Iterator5_t3479011688, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}

	inline static int32_t get_offset_of_U24current_7() { return static_cast<int32_t>(offsetof(U3CDownloadU3Ec__Iterator5_t3479011688, ___U24current_7)); }
	inline Il2CppObject * get_U24current_7() const { return ___U24current_7; }
	inline Il2CppObject ** get_address_of_U24current_7() { return &___U24current_7; }
	inline void set_U24current_7(Il2CppObject * value)
	{
		___U24current_7 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_7, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_8() { return static_cast<int32_t>(offsetof(U3CDownloadU3Ec__Iterator5_t3479011688, ___U3CU3Ef__this_8)); }
	inline Checkandload_t1170646197 * get_U3CU3Ef__this_8() const { return ___U3CU3Ef__this_8; }
	inline Checkandload_t1170646197 ** get_address_of_U3CU3Ef__this_8() { return &___U3CU3Ef__this_8; }
	inline void set_U3CU3Ef__this_8(Checkandload_t1170646197 * value)
	{
		___U3CU3Ef__this_8 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
