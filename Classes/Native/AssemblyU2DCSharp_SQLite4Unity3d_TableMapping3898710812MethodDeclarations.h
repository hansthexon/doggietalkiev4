﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SQLite4Unity3d.TableMapping
struct TableMapping_t3898710812;
// System.Type
struct Type_t;
// System.String
struct String_t;
// SQLite4Unity3d.TableMapping/Column[]
struct ColumnU5BU5D_t1579265676;
// SQLite4Unity3d.TableMapping/Column
struct Column_t441055761;
// System.Object
struct Il2CppObject;
// SQLite4Unity3d.PreparedSqlLiteInsertCommand
struct PreparedSqlLiteInsertCommand_t794754523;
// SQLite4Unity3d.SQLiteConnection
struct SQLiteConnection_t3529499386;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Type1303803226.h"
#include "AssemblyU2DCSharp_SQLite4Unity3d_CreateFlags3110178347.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharp_SQLite4Unity3d_TableMapping_Colum441055761.h"
#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_SQLite4Unity3d_SQLiteConnection3529499386.h"

// System.Void SQLite4Unity3d.TableMapping::.ctor(System.Type,SQLite4Unity3d.CreateFlags)
extern "C"  void TableMapping__ctor_m3530488845 (TableMapping_t3898710812 * __this, Type_t * ___type0, int32_t ___createFlags1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type SQLite4Unity3d.TableMapping::get_MappedType()
extern "C"  Type_t * TableMapping_get_MappedType_m2186451513 (TableMapping_t3898710812 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SQLite4Unity3d.TableMapping::set_MappedType(System.Type)
extern "C"  void TableMapping_set_MappedType_m2420147212 (TableMapping_t3898710812 * __this, Type_t * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String SQLite4Unity3d.TableMapping::get_TableName()
extern "C"  String_t* TableMapping_get_TableName_m467523828 (TableMapping_t3898710812 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SQLite4Unity3d.TableMapping::set_TableName(System.String)
extern "C"  void TableMapping_set_TableName_m295433487 (TableMapping_t3898710812 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SQLite4Unity3d.TableMapping/Column[] SQLite4Unity3d.TableMapping::get_Columns()
extern "C"  ColumnU5BU5D_t1579265676* TableMapping_get_Columns_m2379574159 (TableMapping_t3898710812 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SQLite4Unity3d.TableMapping::set_Columns(SQLite4Unity3d.TableMapping/Column[])
extern "C"  void TableMapping_set_Columns_m881498362 (TableMapping_t3898710812 * __this, ColumnU5BU5D_t1579265676* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SQLite4Unity3d.TableMapping/Column SQLite4Unity3d.TableMapping::get_PK()
extern "C"  Column_t441055761 * TableMapping_get_PK_m4161391555 (TableMapping_t3898710812 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SQLite4Unity3d.TableMapping::set_PK(SQLite4Unity3d.TableMapping/Column)
extern "C"  void TableMapping_set_PK_m2675379410 (TableMapping_t3898710812 * __this, Column_t441055761 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String SQLite4Unity3d.TableMapping::get_GetByPrimaryKeySql()
extern "C"  String_t* TableMapping_get_GetByPrimaryKeySql_m4256871361 (TableMapping_t3898710812 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SQLite4Unity3d.TableMapping::set_GetByPrimaryKeySql(System.String)
extern "C"  void TableMapping_set_GetByPrimaryKeySql_m3820148062 (TableMapping_t3898710812 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SQLite4Unity3d.TableMapping::get_HasAutoIncPK()
extern "C"  bool TableMapping_get_HasAutoIncPK_m1900419174 (TableMapping_t3898710812 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SQLite4Unity3d.TableMapping::set_HasAutoIncPK(System.Boolean)
extern "C"  void TableMapping_set_HasAutoIncPK_m674978701 (TableMapping_t3898710812 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SQLite4Unity3d.TableMapping::SetAutoIncPK(System.Object,System.Int64)
extern "C"  void TableMapping_SetAutoIncPK_m1604148979 (TableMapping_t3898710812 * __this, Il2CppObject * ___obj0, int64_t ___id1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SQLite4Unity3d.TableMapping/Column[] SQLite4Unity3d.TableMapping::get_InsertColumns()
extern "C"  ColumnU5BU5D_t1579265676* TableMapping_get_InsertColumns_m3090643842 (TableMapping_t3898710812 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SQLite4Unity3d.TableMapping/Column[] SQLite4Unity3d.TableMapping::get_InsertOrReplaceColumns()
extern "C"  ColumnU5BU5D_t1579265676* TableMapping_get_InsertOrReplaceColumns_m1064191193 (TableMapping_t3898710812 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SQLite4Unity3d.TableMapping/Column SQLite4Unity3d.TableMapping::FindColumnWithPropertyName(System.String)
extern "C"  Column_t441055761 * TableMapping_FindColumnWithPropertyName_m3751516816 (TableMapping_t3898710812 * __this, String_t* ___propertyName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SQLite4Unity3d.TableMapping/Column SQLite4Unity3d.TableMapping::FindColumn(System.String)
extern "C"  Column_t441055761 * TableMapping_FindColumn_m1663565178 (TableMapping_t3898710812 * __this, String_t* ___columnName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SQLite4Unity3d.PreparedSqlLiteInsertCommand SQLite4Unity3d.TableMapping::GetInsertCommand(SQLite4Unity3d.SQLiteConnection,System.String)
extern "C"  PreparedSqlLiteInsertCommand_t794754523 * TableMapping_GetInsertCommand_m2842269149 (TableMapping_t3898710812 * __this, SQLiteConnection_t3529499386 * ___conn0, String_t* ___extra1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SQLite4Unity3d.PreparedSqlLiteInsertCommand SQLite4Unity3d.TableMapping::CreateInsertCommand(SQLite4Unity3d.SQLiteConnection,System.String)
extern "C"  PreparedSqlLiteInsertCommand_t794754523 * TableMapping_CreateInsertCommand_m3414442655 (TableMapping_t3898710812 * __this, SQLiteConnection_t3529499386 * ___conn0, String_t* ___extra1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SQLite4Unity3d.TableMapping::Dispose()
extern "C"  void TableMapping_Dispose_m2673328880 (TableMapping_t3898710812 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SQLite4Unity3d.TableMapping::<get_InsertColumns>m__C(SQLite4Unity3d.TableMapping/Column)
extern "C"  bool TableMapping_U3Cget_InsertColumnsU3Em__C_m23522193 (Il2CppObject * __this /* static, unused */, Column_t441055761 * ___c0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String SQLite4Unity3d.TableMapping::<CreateInsertCommand>m__F(SQLite4Unity3d.TableMapping/Column)
extern "C"  String_t* TableMapping_U3CCreateInsertCommandU3Em__F_m1223022334 (Il2CppObject * __this /* static, unused */, Column_t441055761 * ___c0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String SQLite4Unity3d.TableMapping::<CreateInsertCommand>m__10(SQLite4Unity3d.TableMapping/Column)
extern "C"  String_t* TableMapping_U3CCreateInsertCommandU3Em__10_m2880310577 (Il2CppObject * __this /* static, unused */, Column_t441055761 * ___c0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
