﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// IAPDemo
struct IAPDemo_t823941185;
// UnityEngine.Purchasing.IStoreController
struct IStoreController_t92554892;
// UnityEngine.Purchasing.IExtensionProvider
struct IExtensionProvider_t2460996543;
// UnityEngine.Purchasing.PurchaseEventArgs
struct PurchaseEventArgs_t547992434;
// UnityEngine.Purchasing.Product
struct Product_t1203687971;
// System.Collections.Generic.IEnumerable`1<UnityEngine.Purchasing.Product>
struct IEnumerable_1_t1495815016;
// UnityEngine.UI.Dropdown
struct Dropdown_t1985816271;
// UnityEngine.UI.Button
struct Button_t2872111280;
// UnityEngine.UI.Text
struct Text_t356221433;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Purc2407199463.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Purch547992434.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Prod1203687971.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Purc1322959839.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Init2954032642.h"

// System.Void IAPDemo::.ctor()
extern "C"  void IAPDemo__ctor_m3267573714 (IAPDemo_t823941185 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IAPDemo::OnInitialized(UnityEngine.Purchasing.IStoreController,UnityEngine.Purchasing.IExtensionProvider)
extern "C"  void IAPDemo_OnInitialized_m2628601748 (IAPDemo_t823941185 * __this, Il2CppObject * ___controller0, Il2CppObject * ___extensions1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Purchasing.PurchaseProcessingResult IAPDemo::ProcessPurchase(UnityEngine.Purchasing.PurchaseEventArgs)
extern "C"  int32_t IAPDemo_ProcessPurchase_m3949646312 (IAPDemo_t823941185 * __this, PurchaseEventArgs_t547992434 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IAPDemo::OnPurchaseFailed(UnityEngine.Purchasing.Product,UnityEngine.Purchasing.PurchaseFailureReason)
extern "C"  void IAPDemo_OnPurchaseFailed_m2053066303 (IAPDemo_t823941185 * __this, Product_t1203687971 * ___item0, int32_t ___r1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IAPDemo::OnInitializeFailed(UnityEngine.Purchasing.InitializationFailureReason)
extern "C"  void IAPDemo_OnInitializeFailed_m1037377075 (IAPDemo_t823941185 * __this, int32_t ___error0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IAPDemo::Awake()
extern "C"  void IAPDemo_Awake_m1872671099 (IAPDemo_t823941185 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IAPDemo::OnTransactionsRestored(System.Boolean)
extern "C"  void IAPDemo_OnTransactionsRestored_m2633548551 (IAPDemo_t823941185 * __this, bool ___success0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IAPDemo::OnDeferred(UnityEngine.Purchasing.Product)
extern "C"  void IAPDemo_OnDeferred_m801123048 (IAPDemo_t823941185 * __this, Product_t1203687971 * ___item0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IAPDemo::InitUI(System.Collections.Generic.IEnumerable`1<UnityEngine.Purchasing.Product>)
extern "C"  void IAPDemo_InitUI_m3350388155 (IAPDemo_t823941185 * __this, Il2CppObject* ___items0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IAPDemo::UpdateHistoryUI()
extern "C"  void IAPDemo_UpdateHistoryUI_m2607563357 (IAPDemo_t823941185 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IAPDemo::UpdateInteractable()
extern "C"  void IAPDemo_UpdateInteractable_m2228859499 (IAPDemo_t823941185 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IAPDemo::Update()
extern "C"  void IAPDemo_Update_m412123535 (IAPDemo_t823941185 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.UI.Dropdown IAPDemo::GetDropdown()
extern "C"  Dropdown_t1985816271 * IAPDemo_GetDropdown_m1658389538 (IAPDemo_t823941185 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.UI.Button IAPDemo::GetBuyButton()
extern "C"  Button_t2872111280 * IAPDemo_GetBuyButton_m514831370 (IAPDemo_t823941185 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.UI.Button IAPDemo::GetRestoreButton()
extern "C"  Button_t2872111280 * IAPDemo_GetRestoreButton_m3830435552 (IAPDemo_t823941185 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.UI.Text IAPDemo::GetText(System.Boolean)
extern "C"  Text_t356221433 * IAPDemo_GetText_m3382056873 (IAPDemo_t823941185 * __this, bool ___right0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IAPDemo::<InitUI>m__0(System.Int32)
extern "C"  void IAPDemo_U3CInitUIU3Em__0_m3848246332 (IAPDemo_t823941185 * __this, int32_t ___selectedItem0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IAPDemo::<InitUI>m__1()
extern "C"  void IAPDemo_U3CInitUIU3Em__1_m905417126 (IAPDemo_t823941185 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IAPDemo::<InitUI>m__2()
extern "C"  void IAPDemo_U3CInitUIU3Em__2_m905417093 (IAPDemo_t823941185 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
