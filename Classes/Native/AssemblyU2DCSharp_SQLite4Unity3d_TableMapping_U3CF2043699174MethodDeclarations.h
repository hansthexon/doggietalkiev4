﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SQLite4Unity3d.TableMapping/<FindColumnWithPropertyName>c__AnonStorey16
struct U3CFindColumnWithPropertyNameU3Ec__AnonStorey16_t2043699174;
// SQLite4Unity3d.TableMapping/Column
struct Column_t441055761;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_SQLite4Unity3d_TableMapping_Colum441055761.h"

// System.Void SQLite4Unity3d.TableMapping/<FindColumnWithPropertyName>c__AnonStorey16::.ctor()
extern "C"  void U3CFindColumnWithPropertyNameU3Ec__AnonStorey16__ctor_m3419584889 (U3CFindColumnWithPropertyNameU3Ec__AnonStorey16_t2043699174 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SQLite4Unity3d.TableMapping/<FindColumnWithPropertyName>c__AnonStorey16::<>m__D(SQLite4Unity3d.TableMapping/Column)
extern "C"  bool U3CFindColumnWithPropertyNameU3Ec__AnonStorey16_U3CU3Em__D_m1532080701 (U3CFindColumnWithPropertyNameU3Ec__AnonStorey16_t2043699174 * __this, Column_t441055761 * ___c0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
