﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.ReadOnlyCollection`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>
struct ReadOnlyCollection_1_t859945680;
// System.Collections.Generic.IList`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>
struct IList_1_t1215100589;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;
// SQLite4Unity3d.SQLiteConnection/IndexedColumn[]
struct IndexedColumnU5BU5D_t4219656765;
// System.Collections.Generic.IEnumerator`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>
struct IEnumerator_1_t2444651111;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_SQLite4Unity3d_SQLiteConnection_I674159988.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::.ctor(System.Collections.Generic.IList`1<T>)
extern "C"  void ReadOnlyCollection_1__ctor_m3227264132_gshared (ReadOnlyCollection_1_t859945680 * __this, Il2CppObject* ___list0, const MethodInfo* method);
#define ReadOnlyCollection_1__ctor_m3227264132(__this, ___list0, method) ((  void (*) (ReadOnlyCollection_1_t859945680 *, Il2CppObject*, const MethodInfo*))ReadOnlyCollection_1__ctor_m3227264132_gshared)(__this, ___list0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::System.Collections.Generic.ICollection<T>.Add(T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m2258850560_gshared (ReadOnlyCollection_1_t859945680 * __this, IndexedColumn_t674159988  ___item0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m2258850560(__this, ___item0, method) ((  void (*) (ReadOnlyCollection_1_t859945680 *, IndexedColumn_t674159988 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m2258850560_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::System.Collections.Generic.ICollection<T>.Clear()
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m2078716380_gshared (ReadOnlyCollection_1_t859945680 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m2078716380(__this, method) ((  void (*) (ReadOnlyCollection_1_t859945680 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m2078716380_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::System.Collections.Generic.IList<T>.Insert(System.Int32,T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m768762563_gshared (ReadOnlyCollection_1_t859945680 * __this, int32_t ___index0, IndexedColumn_t674159988  ___item1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m768762563(__this, ___index0, ___item1, method) ((  void (*) (ReadOnlyCollection_1_t859945680 *, int32_t, IndexedColumn_t674159988 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m768762563_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::System.Collections.Generic.ICollection<T>.Remove(T)
extern "C"  bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m1145708939_gshared (ReadOnlyCollection_1_t859945680 * __this, IndexedColumn_t674159988  ___item0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m1145708939(__this, ___item0, method) ((  bool (*) (ReadOnlyCollection_1_t859945680 *, IndexedColumn_t674159988 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m1145708939_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::System.Collections.Generic.IList<T>.RemoveAt(System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m2531084583_gshared (ReadOnlyCollection_1_t859945680 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m2531084583(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t859945680 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m2531084583_gshared)(__this, ___index0, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::System.Collections.Generic.IList<T>.get_Item(System.Int32)
extern "C"  IndexedColumn_t674159988  ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m1944603263_gshared (ReadOnlyCollection_1_t859945680 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m1944603263(__this, ___index0, method) ((  IndexedColumn_t674159988  (*) (ReadOnlyCollection_1_t859945680 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m1944603263_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::System.Collections.Generic.IList<T>.set_Item(System.Int32,T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m859518866_gshared (ReadOnlyCollection_1_t859945680 * __this, int32_t ___index0, IndexedColumn_t674159988  ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m859518866(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t859945680 *, int32_t, IndexedColumn_t674159988 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m859518866_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3723163402_gshared (ReadOnlyCollection_1_t859945680 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3723163402(__this, method) ((  bool (*) (ReadOnlyCollection_1_t859945680 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3723163402_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m2171244493_gshared (ReadOnlyCollection_1_t859945680 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m2171244493(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t859945680 *, Il2CppArray *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m2171244493_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.ReadOnlyCollection`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m3288698822_gshared (ReadOnlyCollection_1_t859945680 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m3288698822(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t859945680 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m3288698822_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_IList_Add_m90316617_gshared (ReadOnlyCollection_1_t859945680 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Add_m90316617(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t859945680 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Add_m90316617_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::System.Collections.IList.Clear()
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Clear_m567885717_gshared (ReadOnlyCollection_1_t859945680 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Clear_m567885717(__this, method) ((  void (*) (ReadOnlyCollection_1_t859945680 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Clear_m567885717_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::System.Collections.IList.Contains(System.Object)
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_Contains_m2692845293_gshared (ReadOnlyCollection_1_t859945680 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Contains_m2692845293(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t859945680 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Contains_m2692845293_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_IList_IndexOf_m717262319_gshared (ReadOnlyCollection_1_t859945680 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_IndexOf_m717262319(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t859945680 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_IndexOf_m717262319_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Insert_m2245012808_gshared (ReadOnlyCollection_1_t859945680 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Insert_m2245012808(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t859945680 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Insert_m2245012808_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::System.Collections.IList.Remove(System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Remove_m2305347306_gshared (ReadOnlyCollection_1_t859945680 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Remove_m2305347306(__this, ___value0, method) ((  void (*) (ReadOnlyCollection_1_t859945680 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Remove_m2305347306_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::System.Collections.IList.RemoveAt(System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m60868542_gshared (ReadOnlyCollection_1_t859945680 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m60868542(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t859945680 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m60868542_gshared)(__this, ___index0, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m3459146953_gshared (ReadOnlyCollection_1_t859945680 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m3459146953(__this, method) ((  bool (*) (ReadOnlyCollection_1_t859945680 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m3459146953_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m4135887149_gshared (ReadOnlyCollection_1_t859945680 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m4135887149(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t859945680 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m4135887149_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m2789034968_gshared (ReadOnlyCollection_1_t859945680 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m2789034968(__this, method) ((  bool (*) (ReadOnlyCollection_1_t859945680 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m2789034968_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m2172718005_gshared (ReadOnlyCollection_1_t859945680 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m2172718005(__this, method) ((  bool (*) (ReadOnlyCollection_1_t859945680 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m2172718005_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_IList_get_Item_m199878820_gshared (ReadOnlyCollection_1_t859945680 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_Item_m199878820(__this, ___index0, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t859945680 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_Item_m199878820_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_set_Item_m938762303_gshared (ReadOnlyCollection_1_t859945680 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_set_Item_m938762303(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t859945680 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_set_Item_m938762303_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::Contains(T)
extern "C"  bool ReadOnlyCollection_1_Contains_m1023318798_gshared (ReadOnlyCollection_1_t859945680 * __this, IndexedColumn_t674159988  ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_Contains_m1023318798(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t859945680 *, IndexedColumn_t674159988 , const MethodInfo*))ReadOnlyCollection_1_Contains_m1023318798_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::CopyTo(T[],System.Int32)
extern "C"  void ReadOnlyCollection_1_CopyTo_m3956176012_gshared (ReadOnlyCollection_1_t859945680 * __this, IndexedColumnU5BU5D_t4219656765* ___array0, int32_t ___index1, const MethodInfo* method);
#define ReadOnlyCollection_1_CopyTo_m3956176012(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t859945680 *, IndexedColumnU5BU5D_t4219656765*, int32_t, const MethodInfo*))ReadOnlyCollection_1_CopyTo_m3956176012_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::GetEnumerator()
extern "C"  Il2CppObject* ReadOnlyCollection_1_GetEnumerator_m1750560953_gshared (ReadOnlyCollection_1_t859945680 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_GetEnumerator_m1750560953(__this, method) ((  Il2CppObject* (*) (ReadOnlyCollection_1_t859945680 *, const MethodInfo*))ReadOnlyCollection_1_GetEnumerator_m1750560953_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::IndexOf(T)
extern "C"  int32_t ReadOnlyCollection_1_IndexOf_m3852524110_gshared (ReadOnlyCollection_1_t859945680 * __this, IndexedColumn_t674159988  ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_IndexOf_m3852524110(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t859945680 *, IndexedColumn_t674159988 , const MethodInfo*))ReadOnlyCollection_1_IndexOf_m3852524110_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::get_Count()
extern "C"  int32_t ReadOnlyCollection_1_get_Count_m1072322953_gshared (ReadOnlyCollection_1_t859945680 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Count_m1072322953(__this, method) ((  int32_t (*) (ReadOnlyCollection_1_t859945680 *, const MethodInfo*))ReadOnlyCollection_1_get_Count_m1072322953_gshared)(__this, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::get_Item(System.Int32)
extern "C"  IndexedColumn_t674159988  ReadOnlyCollection_1_get_Item_m36366027_gshared (ReadOnlyCollection_1_t859945680 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Item_m36366027(__this, ___index0, method) ((  IndexedColumn_t674159988  (*) (ReadOnlyCollection_1_t859945680 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_get_Item_m36366027_gshared)(__this, ___index0, method)
