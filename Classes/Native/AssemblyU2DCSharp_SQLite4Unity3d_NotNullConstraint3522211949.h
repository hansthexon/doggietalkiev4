﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.IEnumerable`1<SQLite4Unity3d.TableMapping/Column>
struct IEnumerable_1_t733182806;

#include "AssemblyU2DCSharp_SQLite4Unity3d_SQLiteException827126597.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SQLite4Unity3d.NotNullConstraintViolationException
struct  NotNullConstraintViolationException_t3522211949  : public SQLiteException_t827126597
{
public:
	// System.Collections.Generic.IEnumerable`1<SQLite4Unity3d.TableMapping/Column> SQLite4Unity3d.NotNullConstraintViolationException::<Columns>k__BackingField
	Il2CppObject* ___U3CColumnsU3Ek__BackingField_12;

public:
	inline static int32_t get_offset_of_U3CColumnsU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(NotNullConstraintViolationException_t3522211949, ___U3CColumnsU3Ek__BackingField_12)); }
	inline Il2CppObject* get_U3CColumnsU3Ek__BackingField_12() const { return ___U3CColumnsU3Ek__BackingField_12; }
	inline Il2CppObject** get_address_of_U3CColumnsU3Ek__BackingField_12() { return &___U3CColumnsU3Ek__BackingField_12; }
	inline void set_U3CColumnsU3Ek__BackingField_12(Il2CppObject* value)
	{
		___U3CColumnsU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier(&___U3CColumnsU3Ek__BackingField_12, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
