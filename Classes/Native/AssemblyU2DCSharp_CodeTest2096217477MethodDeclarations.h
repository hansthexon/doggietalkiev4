﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CodeTest
struct CodeTest_t2096217477;

#include "codegen/il2cpp-codegen.h"

// System.Void CodeTest::.ctor()
extern "C"  void CodeTest__ctor_m2589481016 (CodeTest_t2096217477 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
