﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LitJson.ExporterFunc`1<UnityEngine.Bounds>
struct ExporterFunc_1_t4270476650;
// System.Object
struct Il2CppObject;
// LitJson.JsonWriter
struct JsonWriter_t1927598499;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "UnityEngine_UnityEngine_Bounds3033363703.h"
#include "AssemblyU2DCSharp_LitJson_JsonWriter1927598499.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void LitJson.ExporterFunc`1<UnityEngine.Bounds>::.ctor(System.Object,System.IntPtr)
extern "C"  void ExporterFunc_1__ctor_m732779106_gshared (ExporterFunc_1_t4270476650 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method);
#define ExporterFunc_1__ctor_m732779106(__this, ___object0, ___method1, method) ((  void (*) (ExporterFunc_1_t4270476650 *, Il2CppObject *, IntPtr_t, const MethodInfo*))ExporterFunc_1__ctor_m732779106_gshared)(__this, ___object0, ___method1, method)
// System.Void LitJson.ExporterFunc`1<UnityEngine.Bounds>::Invoke(T,LitJson.JsonWriter)
extern "C"  void ExporterFunc_1_Invoke_m361856306_gshared (ExporterFunc_1_t4270476650 * __this, Bounds_t3033363703  ___obj0, JsonWriter_t1927598499 * ___writer1, const MethodInfo* method);
#define ExporterFunc_1_Invoke_m361856306(__this, ___obj0, ___writer1, method) ((  void (*) (ExporterFunc_1_t4270476650 *, Bounds_t3033363703 , JsonWriter_t1927598499 *, const MethodInfo*))ExporterFunc_1_Invoke_m361856306_gshared)(__this, ___obj0, ___writer1, method)
// System.IAsyncResult LitJson.ExporterFunc`1<UnityEngine.Bounds>::BeginInvoke(T,LitJson.JsonWriter,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * ExporterFunc_1_BeginInvoke_m2799634367_gshared (ExporterFunc_1_t4270476650 * __this, Bounds_t3033363703  ___obj0, JsonWriter_t1927598499 * ___writer1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method);
#define ExporterFunc_1_BeginInvoke_m2799634367(__this, ___obj0, ___writer1, ___callback2, ___object3, method) ((  Il2CppObject * (*) (ExporterFunc_1_t4270476650 *, Bounds_t3033363703 , JsonWriter_t1927598499 *, AsyncCallback_t163412349 *, Il2CppObject *, const MethodInfo*))ExporterFunc_1_BeginInvoke_m2799634367_gshared)(__this, ___obj0, ___writer1, ___callback2, ___object3, method)
// System.Void LitJson.ExporterFunc`1<UnityEngine.Bounds>::EndInvoke(System.IAsyncResult)
extern "C"  void ExporterFunc_1_EndInvoke_m2748156268_gshared (ExporterFunc_1_t4270476650 * __this, Il2CppObject * ___result0, const MethodInfo* method);
#define ExporterFunc_1_EndInvoke_m2748156268(__this, ___result0, method) ((  void (*) (ExporterFunc_1_t4270476650 *, Il2CppObject *, const MethodInfo*))ExporterFunc_1_EndInvoke_m2748156268_gshared)(__this, ___result0, method)
