﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SQLite4Unity3d.SQLiteCommand/<ExecuteDeferredQuery>c__IteratorD`1<System.Object>
struct U3CExecuteDeferredQueryU3Ec__IteratorD_1_t863295320;
// System.Object
struct Il2CppObject;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t164973122;

#include "codegen/il2cpp-codegen.h"

// System.Void SQLite4Unity3d.SQLiteCommand/<ExecuteDeferredQuery>c__IteratorD`1<System.Object>::.ctor()
extern "C"  void U3CExecuteDeferredQueryU3Ec__IteratorD_1__ctor_m4037391597_gshared (U3CExecuteDeferredQueryU3Ec__IteratorD_1_t863295320 * __this, const MethodInfo* method);
#define U3CExecuteDeferredQueryU3Ec__IteratorD_1__ctor_m4037391597(__this, method) ((  void (*) (U3CExecuteDeferredQueryU3Ec__IteratorD_1_t863295320 *, const MethodInfo*))U3CExecuteDeferredQueryU3Ec__IteratorD_1__ctor_m4037391597_gshared)(__this, method)
// T SQLite4Unity3d.SQLiteCommand/<ExecuteDeferredQuery>c__IteratorD`1<System.Object>::System.Collections.Generic.IEnumerator<T>.get_Current()
extern "C"  Il2CppObject * U3CExecuteDeferredQueryU3Ec__IteratorD_1_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m4117338172_gshared (U3CExecuteDeferredQueryU3Ec__IteratorD_1_t863295320 * __this, const MethodInfo* method);
#define U3CExecuteDeferredQueryU3Ec__IteratorD_1_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m4117338172(__this, method) ((  Il2CppObject * (*) (U3CExecuteDeferredQueryU3Ec__IteratorD_1_t863295320 *, const MethodInfo*))U3CExecuteDeferredQueryU3Ec__IteratorD_1_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m4117338172_gshared)(__this, method)
// System.Object SQLite4Unity3d.SQLiteCommand/<ExecuteDeferredQuery>c__IteratorD`1<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CExecuteDeferredQueryU3Ec__IteratorD_1_System_Collections_IEnumerator_get_Current_m3364605971_gshared (U3CExecuteDeferredQueryU3Ec__IteratorD_1_t863295320 * __this, const MethodInfo* method);
#define U3CExecuteDeferredQueryU3Ec__IteratorD_1_System_Collections_IEnumerator_get_Current_m3364605971(__this, method) ((  Il2CppObject * (*) (U3CExecuteDeferredQueryU3Ec__IteratorD_1_t863295320 *, const MethodInfo*))U3CExecuteDeferredQueryU3Ec__IteratorD_1_System_Collections_IEnumerator_get_Current_m3364605971_gshared)(__this, method)
// System.Collections.IEnumerator SQLite4Unity3d.SQLiteCommand/<ExecuteDeferredQuery>c__IteratorD`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CExecuteDeferredQueryU3Ec__IteratorD_1_System_Collections_IEnumerable_GetEnumerator_m51386002_gshared (U3CExecuteDeferredQueryU3Ec__IteratorD_1_t863295320 * __this, const MethodInfo* method);
#define U3CExecuteDeferredQueryU3Ec__IteratorD_1_System_Collections_IEnumerable_GetEnumerator_m51386002(__this, method) ((  Il2CppObject * (*) (U3CExecuteDeferredQueryU3Ec__IteratorD_1_t863295320 *, const MethodInfo*))U3CExecuteDeferredQueryU3Ec__IteratorD_1_System_Collections_IEnumerable_GetEnumerator_m51386002_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<T> SQLite4Unity3d.SQLiteCommand/<ExecuteDeferredQuery>c__IteratorD`1<System.Object>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C"  Il2CppObject* U3CExecuteDeferredQueryU3Ec__IteratorD_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m2727291681_gshared (U3CExecuteDeferredQueryU3Ec__IteratorD_1_t863295320 * __this, const MethodInfo* method);
#define U3CExecuteDeferredQueryU3Ec__IteratorD_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m2727291681(__this, method) ((  Il2CppObject* (*) (U3CExecuteDeferredQueryU3Ec__IteratorD_1_t863295320 *, const MethodInfo*))U3CExecuteDeferredQueryU3Ec__IteratorD_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m2727291681_gshared)(__this, method)
// System.Boolean SQLite4Unity3d.SQLiteCommand/<ExecuteDeferredQuery>c__IteratorD`1<System.Object>::MoveNext()
extern "C"  bool U3CExecuteDeferredQueryU3Ec__IteratorD_1_MoveNext_m728710123_gshared (U3CExecuteDeferredQueryU3Ec__IteratorD_1_t863295320 * __this, const MethodInfo* method);
#define U3CExecuteDeferredQueryU3Ec__IteratorD_1_MoveNext_m728710123(__this, method) ((  bool (*) (U3CExecuteDeferredQueryU3Ec__IteratorD_1_t863295320 *, const MethodInfo*))U3CExecuteDeferredQueryU3Ec__IteratorD_1_MoveNext_m728710123_gshared)(__this, method)
// System.Void SQLite4Unity3d.SQLiteCommand/<ExecuteDeferredQuery>c__IteratorD`1<System.Object>::Dispose()
extern "C"  void U3CExecuteDeferredQueryU3Ec__IteratorD_1_Dispose_m849591818_gshared (U3CExecuteDeferredQueryU3Ec__IteratorD_1_t863295320 * __this, const MethodInfo* method);
#define U3CExecuteDeferredQueryU3Ec__IteratorD_1_Dispose_m849591818(__this, method) ((  void (*) (U3CExecuteDeferredQueryU3Ec__IteratorD_1_t863295320 *, const MethodInfo*))U3CExecuteDeferredQueryU3Ec__IteratorD_1_Dispose_m849591818_gshared)(__this, method)
// System.Void SQLite4Unity3d.SQLiteCommand/<ExecuteDeferredQuery>c__IteratorD`1<System.Object>::Reset()
extern "C"  void U3CExecuteDeferredQueryU3Ec__IteratorD_1_Reset_m1423757092_gshared (U3CExecuteDeferredQueryU3Ec__IteratorD_1_t863295320 * __this, const MethodInfo* method);
#define U3CExecuteDeferredQueryU3Ec__IteratorD_1_Reset_m1423757092(__this, method) ((  void (*) (U3CExecuteDeferredQueryU3Ec__IteratorD_1_t863295320 *, const MethodInfo*))U3CExecuteDeferredQueryU3Ec__IteratorD_1_Reset_m1423757092_gshared)(__this, method)
// System.Void SQLite4Unity3d.SQLiteCommand/<ExecuteDeferredQuery>c__IteratorD`1<System.Object>::<>__Finally0()
extern "C"  void U3CExecuteDeferredQueryU3Ec__IteratorD_1_U3CU3E__Finally0_m1741729942_gshared (U3CExecuteDeferredQueryU3Ec__IteratorD_1_t863295320 * __this, const MethodInfo* method);
#define U3CExecuteDeferredQueryU3Ec__IteratorD_1_U3CU3E__Finally0_m1741729942(__this, method) ((  void (*) (U3CExecuteDeferredQueryU3Ec__IteratorD_1_t863295320 *, const MethodInfo*))U3CExecuteDeferredQueryU3Ec__IteratorD_1_U3CU3E__Finally0_m1741729942_gshared)(__this, method)
