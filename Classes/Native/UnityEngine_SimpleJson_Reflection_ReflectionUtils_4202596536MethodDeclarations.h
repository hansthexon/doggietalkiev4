﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SimpleJson.Reflection.ReflectionUtils/<GetSetMethodByReflection>c__AnonStorey6
struct U3CGetSetMethodByReflectionU3Ec__AnonStorey6_t4202596536;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void SimpleJson.Reflection.ReflectionUtils/<GetSetMethodByReflection>c__AnonStorey6::.ctor()
extern "C"  void U3CGetSetMethodByReflectionU3Ec__AnonStorey6__ctor_m757389080 (U3CGetSetMethodByReflectionU3Ec__AnonStorey6_t4202596536 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SimpleJson.Reflection.ReflectionUtils/<GetSetMethodByReflection>c__AnonStorey6::<>m__A(System.Object,System.Object)
extern "C"  void U3CGetSetMethodByReflectionU3Ec__AnonStorey6_U3CU3Em__A_m2382901294 (U3CGetSetMethodByReflectionU3Ec__AnonStorey6_t4202596536 * __this, Il2CppObject * ___source0, Il2CppObject * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
