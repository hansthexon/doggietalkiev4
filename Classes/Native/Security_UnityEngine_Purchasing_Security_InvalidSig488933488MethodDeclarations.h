﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Purchasing.Security.InvalidSignatureException
struct InvalidSignatureException_t488933488;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine.Purchasing.Security.InvalidSignatureException::.ctor()
extern "C"  void InvalidSignatureException__ctor_m883303240 (InvalidSignatureException_t488933488 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
