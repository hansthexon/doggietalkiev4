﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// UnityEngine.RectOffset
struct RectOffset_t3387826427;
// System.Int32[]
struct Int32U5BU5D_t3030399641;
// UnityEngine.Transform
struct Transform_t3275118058;

#include "mscorlib_System_Object2689449295.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "UnityEngine_UnityEngine_Vector42243707581.h"
#include "UnityEngine_UnityEngine_Quaternion4030073918.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"
#include "UnityEngine_UnityEngine_Color32874517518.h"
#include "UnityEngine_UnityEngine_Bounds3033363703.h"
#include "UnityEngine_UnityEngine_Rect3681755626.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// JsonExample/ExampleSerializedClass
struct  ExampleSerializedClass_t1787974205  : public Il2CppObject
{
public:
	// System.Int32 JsonExample/ExampleSerializedClass::myInt
	int32_t ___myInt_0;
	// System.String JsonExample/ExampleSerializedClass::myString
	String_t* ___myString_1;
	// System.Single JsonExample/ExampleSerializedClass::myFloat
	float ___myFloat_2;
	// System.Boolean JsonExample/ExampleSerializedClass::myBool
	bool ___myBool_3;
	// UnityEngine.Vector2 JsonExample/ExampleSerializedClass::myVector2
	Vector2_t2243707579  ___myVector2_4;
	// UnityEngine.Vector3 JsonExample/ExampleSerializedClass::myVector3
	Vector3_t2243707580  ___myVector3_5;
	// UnityEngine.Vector4 JsonExample/ExampleSerializedClass::myVector4
	Vector4_t2243707581  ___myVector4_6;
	// UnityEngine.Quaternion JsonExample/ExampleSerializedClass::myQuaternion
	Quaternion_t4030073918  ___myQuaternion_7;
	// UnityEngine.Color JsonExample/ExampleSerializedClass::myColor
	Color_t2020392075  ___myColor_8;
	// UnityEngine.Color32 JsonExample/ExampleSerializedClass::myColor32
	Color32_t874517518  ___myColor32_9;
	// UnityEngine.Bounds JsonExample/ExampleSerializedClass::myBounds
	Bounds_t3033363703  ___myBounds_10;
	// UnityEngine.Rect JsonExample/ExampleSerializedClass::myRect
	Rect_t3681755626  ___myRect_11;
	// UnityEngine.RectOffset JsonExample/ExampleSerializedClass::myRectOffset
	RectOffset_t3387826427 * ___myRectOffset_12;
	// System.Int32[] JsonExample/ExampleSerializedClass::myIntArray
	Int32U5BU5D_t3030399641* ___myIntArray_13;
	// UnityEngine.Transform JsonExample/ExampleSerializedClass::myIgnoredTransform
	Transform_t3275118058 * ___myIgnoredTransform_14;

public:
	inline static int32_t get_offset_of_myInt_0() { return static_cast<int32_t>(offsetof(ExampleSerializedClass_t1787974205, ___myInt_0)); }
	inline int32_t get_myInt_0() const { return ___myInt_0; }
	inline int32_t* get_address_of_myInt_0() { return &___myInt_0; }
	inline void set_myInt_0(int32_t value)
	{
		___myInt_0 = value;
	}

	inline static int32_t get_offset_of_myString_1() { return static_cast<int32_t>(offsetof(ExampleSerializedClass_t1787974205, ___myString_1)); }
	inline String_t* get_myString_1() const { return ___myString_1; }
	inline String_t** get_address_of_myString_1() { return &___myString_1; }
	inline void set_myString_1(String_t* value)
	{
		___myString_1 = value;
		Il2CppCodeGenWriteBarrier(&___myString_1, value);
	}

	inline static int32_t get_offset_of_myFloat_2() { return static_cast<int32_t>(offsetof(ExampleSerializedClass_t1787974205, ___myFloat_2)); }
	inline float get_myFloat_2() const { return ___myFloat_2; }
	inline float* get_address_of_myFloat_2() { return &___myFloat_2; }
	inline void set_myFloat_2(float value)
	{
		___myFloat_2 = value;
	}

	inline static int32_t get_offset_of_myBool_3() { return static_cast<int32_t>(offsetof(ExampleSerializedClass_t1787974205, ___myBool_3)); }
	inline bool get_myBool_3() const { return ___myBool_3; }
	inline bool* get_address_of_myBool_3() { return &___myBool_3; }
	inline void set_myBool_3(bool value)
	{
		___myBool_3 = value;
	}

	inline static int32_t get_offset_of_myVector2_4() { return static_cast<int32_t>(offsetof(ExampleSerializedClass_t1787974205, ___myVector2_4)); }
	inline Vector2_t2243707579  get_myVector2_4() const { return ___myVector2_4; }
	inline Vector2_t2243707579 * get_address_of_myVector2_4() { return &___myVector2_4; }
	inline void set_myVector2_4(Vector2_t2243707579  value)
	{
		___myVector2_4 = value;
	}

	inline static int32_t get_offset_of_myVector3_5() { return static_cast<int32_t>(offsetof(ExampleSerializedClass_t1787974205, ___myVector3_5)); }
	inline Vector3_t2243707580  get_myVector3_5() const { return ___myVector3_5; }
	inline Vector3_t2243707580 * get_address_of_myVector3_5() { return &___myVector3_5; }
	inline void set_myVector3_5(Vector3_t2243707580  value)
	{
		___myVector3_5 = value;
	}

	inline static int32_t get_offset_of_myVector4_6() { return static_cast<int32_t>(offsetof(ExampleSerializedClass_t1787974205, ___myVector4_6)); }
	inline Vector4_t2243707581  get_myVector4_6() const { return ___myVector4_6; }
	inline Vector4_t2243707581 * get_address_of_myVector4_6() { return &___myVector4_6; }
	inline void set_myVector4_6(Vector4_t2243707581  value)
	{
		___myVector4_6 = value;
	}

	inline static int32_t get_offset_of_myQuaternion_7() { return static_cast<int32_t>(offsetof(ExampleSerializedClass_t1787974205, ___myQuaternion_7)); }
	inline Quaternion_t4030073918  get_myQuaternion_7() const { return ___myQuaternion_7; }
	inline Quaternion_t4030073918 * get_address_of_myQuaternion_7() { return &___myQuaternion_7; }
	inline void set_myQuaternion_7(Quaternion_t4030073918  value)
	{
		___myQuaternion_7 = value;
	}

	inline static int32_t get_offset_of_myColor_8() { return static_cast<int32_t>(offsetof(ExampleSerializedClass_t1787974205, ___myColor_8)); }
	inline Color_t2020392075  get_myColor_8() const { return ___myColor_8; }
	inline Color_t2020392075 * get_address_of_myColor_8() { return &___myColor_8; }
	inline void set_myColor_8(Color_t2020392075  value)
	{
		___myColor_8 = value;
	}

	inline static int32_t get_offset_of_myColor32_9() { return static_cast<int32_t>(offsetof(ExampleSerializedClass_t1787974205, ___myColor32_9)); }
	inline Color32_t874517518  get_myColor32_9() const { return ___myColor32_9; }
	inline Color32_t874517518 * get_address_of_myColor32_9() { return &___myColor32_9; }
	inline void set_myColor32_9(Color32_t874517518  value)
	{
		___myColor32_9 = value;
	}

	inline static int32_t get_offset_of_myBounds_10() { return static_cast<int32_t>(offsetof(ExampleSerializedClass_t1787974205, ___myBounds_10)); }
	inline Bounds_t3033363703  get_myBounds_10() const { return ___myBounds_10; }
	inline Bounds_t3033363703 * get_address_of_myBounds_10() { return &___myBounds_10; }
	inline void set_myBounds_10(Bounds_t3033363703  value)
	{
		___myBounds_10 = value;
	}

	inline static int32_t get_offset_of_myRect_11() { return static_cast<int32_t>(offsetof(ExampleSerializedClass_t1787974205, ___myRect_11)); }
	inline Rect_t3681755626  get_myRect_11() const { return ___myRect_11; }
	inline Rect_t3681755626 * get_address_of_myRect_11() { return &___myRect_11; }
	inline void set_myRect_11(Rect_t3681755626  value)
	{
		___myRect_11 = value;
	}

	inline static int32_t get_offset_of_myRectOffset_12() { return static_cast<int32_t>(offsetof(ExampleSerializedClass_t1787974205, ___myRectOffset_12)); }
	inline RectOffset_t3387826427 * get_myRectOffset_12() const { return ___myRectOffset_12; }
	inline RectOffset_t3387826427 ** get_address_of_myRectOffset_12() { return &___myRectOffset_12; }
	inline void set_myRectOffset_12(RectOffset_t3387826427 * value)
	{
		___myRectOffset_12 = value;
		Il2CppCodeGenWriteBarrier(&___myRectOffset_12, value);
	}

	inline static int32_t get_offset_of_myIntArray_13() { return static_cast<int32_t>(offsetof(ExampleSerializedClass_t1787974205, ___myIntArray_13)); }
	inline Int32U5BU5D_t3030399641* get_myIntArray_13() const { return ___myIntArray_13; }
	inline Int32U5BU5D_t3030399641** get_address_of_myIntArray_13() { return &___myIntArray_13; }
	inline void set_myIntArray_13(Int32U5BU5D_t3030399641* value)
	{
		___myIntArray_13 = value;
		Il2CppCodeGenWriteBarrier(&___myIntArray_13, value);
	}

	inline static int32_t get_offset_of_myIgnoredTransform_14() { return static_cast<int32_t>(offsetof(ExampleSerializedClass_t1787974205, ___myIgnoredTransform_14)); }
	inline Transform_t3275118058 * get_myIgnoredTransform_14() const { return ___myIgnoredTransform_14; }
	inline Transform_t3275118058 ** get_address_of_myIgnoredTransform_14() { return &___myIgnoredTransform_14; }
	inline void set_myIgnoredTransform_14(Transform_t3275118058 * value)
	{
		___myIgnoredTransform_14 = value;
		Il2CppCodeGenWriteBarrier(&___myIgnoredTransform_14, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
