﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DataService/<getVideo>c__AnonStoreyF
struct U3CgetVideoU3Ec__AnonStoreyF_t3098235058;

#include "codegen/il2cpp-codegen.h"

// System.Void DataService/<getVideo>c__AnonStoreyF::.ctor()
extern "C"  void U3CgetVideoU3Ec__AnonStoreyF__ctor_m806084961 (U3CgetVideoU3Ec__AnonStoreyF_t3098235058 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
