﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_ValueType3507792607.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>{C30DAF88-821F-4C93-A2B7-DEA1CE3CF3BB}/__StaticArrayInitTypeSize=24
struct  __StaticArrayInitTypeSizeU3D24_t2759170530 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D24_t2759170530__padding[24];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for marshalling of: <PrivateImplementationDetails>{C30DAF88-821F-4C93-A2B7-DEA1CE3CF3BB}/__StaticArrayInitTypeSize=24
struct __StaticArrayInitTypeSizeU3D24_t2759170530_marshaled_pinvoke
{
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D24_t2759170530__padding[24];
	};
};
// Native definition for marshalling of: <PrivateImplementationDetails>{C30DAF88-821F-4C93-A2B7-DEA1CE3CF3BB}/__StaticArrayInitTypeSize=24
struct __StaticArrayInitTypeSizeU3D24_t2759170530_marshaled_com
{
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D24_t2759170530__padding[24];
	};
};
