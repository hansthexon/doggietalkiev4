﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Purchasing.Security.X509Cert
struct X509Cert_t481809278;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.Security.AppleValidator
struct  AppleValidator_t3837389912  : public Il2CppObject
{
public:
	// UnityEngine.Purchasing.Security.X509Cert UnityEngine.Purchasing.Security.AppleValidator::cert
	X509Cert_t481809278 * ___cert_0;

public:
	inline static int32_t get_offset_of_cert_0() { return static_cast<int32_t>(offsetof(AppleValidator_t3837389912, ___cert_0)); }
	inline X509Cert_t481809278 * get_cert_0() const { return ___cert_0; }
	inline X509Cert_t481809278 ** get_address_of_cert_0() { return &___cert_0; }
	inline void set_cert_0(X509Cert_t481809278 * value)
	{
		___cert_0 = value;
		Il2CppCodeGenWriteBarrier(&___cert_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
