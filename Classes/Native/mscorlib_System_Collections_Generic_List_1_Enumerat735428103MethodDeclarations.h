﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1593300101MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<SQLite4Unity3d.SQLiteCommand/Binding>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m355163290(__this, ___l0, method) ((  void (*) (Enumerator_t735428103 *, List_1_t1200698429 *, const MethodInfo*))Enumerator__ctor_m3769601633_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<SQLite4Unity3d.SQLiteCommand/Binding>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m299609824(__this, method) ((  void (*) (Enumerator_t735428103 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3440386353_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<SQLite4Unity3d.SQLiteCommand/Binding>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m3390000150(__this, method) ((  Il2CppObject * (*) (Enumerator_t735428103 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2853089017_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<SQLite4Unity3d.SQLiteCommand/Binding>::Dispose()
#define Enumerator_Dispose_m1618582513(__this, method) ((  void (*) (Enumerator_t735428103 *, const MethodInfo*))Enumerator_Dispose_m3736175406_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<SQLite4Unity3d.SQLiteCommand/Binding>::VerifyState()
#define Enumerator_VerifyState_m53027264(__this, method) ((  void (*) (Enumerator_t735428103 *, const MethodInfo*))Enumerator_VerifyState_m825848279_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<SQLite4Unity3d.SQLiteCommand/Binding>::MoveNext()
#define Enumerator_MoveNext_m3935582223(__this, method) ((  bool (*) (Enumerator_t735428103 *, const MethodInfo*))Enumerator_MoveNext_m4021114635_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<SQLite4Unity3d.SQLiteCommand/Binding>::get_Current()
#define Enumerator_get_Current_m698157331(__this, method) ((  Binding_t1831577297 * (*) (Enumerator_t735428103 *, const MethodInfo*))Enumerator_get_Current_m1222341175_gshared)(__this, method)
