﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// UnityEngine.Purchasing.Default.IWindowsIAP
struct IWindowsIAP_t818184396;
// UnityEngine.Purchasing.Default.WinProductDescription
struct WinProductDescription_t1075111405;
// System.String
struct String_t;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "winrt_U3CModuleU3E3783534214.h"
#include "winrt_U3CModuleU3E3783534214MethodDeclarations.h"
#include "winrt_UnityEngine_Purchasing_Default_Factory1430638288.h"
#include "winrt_UnityEngine_Purchasing_Default_Factory1430638288MethodDeclarations.h"
#include "mscorlib_System_Boolean3825574718.h"
#include "mscorlib_System_NotImplementedException2785117854MethodDeclarations.h"
#include "mscorlib_System_NotImplementedException2785117854.h"
#include "mscorlib_System_Void1841601450.h"
#include "winrt_UnityEngine_Purchasing_Default_WinProductDes1075111405.h"
#include "winrt_UnityEngine_Purchasing_Default_WinProductDes1075111405MethodDeclarations.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Decimal724701077.h"
#include "mscorlib_System_Object2689449295MethodDeclarations.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// UnityEngine.Purchasing.Default.IWindowsIAP UnityEngine.Purchasing.Default.Factory::Create(System.Boolean)
extern Il2CppClass* NotImplementedException_t2785117854_il2cpp_TypeInfo_var;
extern const uint32_t Factory_Create_m2565271259_MetadataUsageId;
extern "C"  Il2CppObject * Factory_Create_m2565271259 (Il2CppObject * __this /* static, unused */, bool ___mocked0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Factory_Create_m2565271259_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t2785117854 * L_0 = (NotImplementedException_t2785117854 *)il2cpp_codegen_object_new(NotImplementedException_t2785117854_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m808189835(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void UnityEngine.Purchasing.Default.WinProductDescription::set_platformSpecificID(System.String)
extern "C"  void WinProductDescription_set_platformSpecificID_m980536207 (WinProductDescription_t1075111405 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CplatformSpecificIDU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.Void UnityEngine.Purchasing.Default.WinProductDescription::set_price(System.String)
extern "C"  void WinProductDescription_set_price_m4107400774 (WinProductDescription_t1075111405 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CpriceU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.Void UnityEngine.Purchasing.Default.WinProductDescription::set_title(System.String)
extern "C"  void WinProductDescription_set_title_m3980127113 (WinProductDescription_t1075111405 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CtitleU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.Void UnityEngine.Purchasing.Default.WinProductDescription::set_description(System.String)
extern "C"  void WinProductDescription_set_description_m1643279079 (WinProductDescription_t1075111405 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CdescriptionU3Ek__BackingField_3(L_0);
		return;
	}
}
// System.Void UnityEngine.Purchasing.Default.WinProductDescription::set_ISOCurrencyCode(System.String)
extern "C"  void WinProductDescription_set_ISOCurrencyCode_m3982539180 (WinProductDescription_t1075111405 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CISOCurrencyCodeU3Ek__BackingField_4(L_0);
		return;
	}
}
// System.Void UnityEngine.Purchasing.Default.WinProductDescription::set_priceDecimal(System.Decimal)
extern "C"  void WinProductDescription_set_priceDecimal_m758852465 (WinProductDescription_t1075111405 * __this, Decimal_t724701077  ___value0, const MethodInfo* method)
{
	{
		Decimal_t724701077  L_0 = ___value0;
		__this->set_U3CpriceDecimalU3Ek__BackingField_5(L_0);
		return;
	}
}
// System.Void UnityEngine.Purchasing.Default.WinProductDescription::set_receipt(System.String)
extern "C"  void WinProductDescription_set_receipt_m3386771729 (WinProductDescription_t1075111405 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CreceiptU3Ek__BackingField_6(L_0);
		return;
	}
}
// System.Void UnityEngine.Purchasing.Default.WinProductDescription::set_transactionID(System.String)
extern "C"  void WinProductDescription_set_transactionID_m1405895190 (WinProductDescription_t1075111405 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CtransactionIDU3Ek__BackingField_7(L_0);
		return;
	}
}
// System.Void UnityEngine.Purchasing.Default.WinProductDescription::set_consumable(System.Boolean)
extern "C"  void WinProductDescription_set_consumable_m356800619 (WinProductDescription_t1075111405 * __this, bool ___value0, const MethodInfo* method)
{
	{
		bool L_0 = ___value0;
		__this->set_U3CconsumableU3Ek__BackingField_8(L_0);
		return;
	}
}
// System.Void UnityEngine.Purchasing.Default.WinProductDescription::.ctor(System.String,System.String,System.String,System.String,System.String,System.Decimal,System.String,System.String,System.Boolean)
extern "C"  void WinProductDescription__ctor_m3122598843 (WinProductDescription_t1075111405 * __this, String_t* ___id0, String_t* ___price1, String_t* ___title2, String_t* ___description3, String_t* ___isoCode4, Decimal_t724701077  ___priceD5, String_t* ___receipt6, String_t* ___transactionId7, bool ___consumable8, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___id0;
		WinProductDescription_set_platformSpecificID_m980536207(__this, L_0, /*hidden argument*/NULL);
		String_t* L_1 = ___price1;
		WinProductDescription_set_price_m4107400774(__this, L_1, /*hidden argument*/NULL);
		String_t* L_2 = ___title2;
		WinProductDescription_set_title_m3980127113(__this, L_2, /*hidden argument*/NULL);
		String_t* L_3 = ___description3;
		WinProductDescription_set_description_m1643279079(__this, L_3, /*hidden argument*/NULL);
		String_t* L_4 = ___isoCode4;
		WinProductDescription_set_ISOCurrencyCode_m3982539180(__this, L_4, /*hidden argument*/NULL);
		Decimal_t724701077  L_5 = ___priceD5;
		WinProductDescription_set_priceDecimal_m758852465(__this, L_5, /*hidden argument*/NULL);
		String_t* L_6 = ___receipt6;
		WinProductDescription_set_receipt_m3386771729(__this, L_6, /*hidden argument*/NULL);
		String_t* L_7 = ___transactionId7;
		WinProductDescription_set_transactionID_m1405895190(__this, L_7, /*hidden argument*/NULL);
		bool L_8 = ___consumable8;
		WinProductDescription_set_consumable_m356800619(__this, L_8, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
