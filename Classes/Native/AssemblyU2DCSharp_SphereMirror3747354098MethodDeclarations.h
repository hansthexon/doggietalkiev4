﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SphereMirror
struct SphereMirror_t3747354098;

#include "codegen/il2cpp-codegen.h"

// System.Void SphereMirror::.ctor()
extern "C"  void SphereMirror__ctor_m3597784247 (SphereMirror_t3747354098 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SphereMirror::Start()
extern "C"  void SphereMirror_Start_m4000549011 (SphereMirror_t3747354098 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SphereMirror::Update()
extern "C"  void SphereMirror_Update_m298469408 (SphereMirror_t3747354098 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
