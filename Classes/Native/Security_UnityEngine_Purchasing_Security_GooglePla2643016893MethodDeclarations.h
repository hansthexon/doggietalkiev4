﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Purchasing.Security.GooglePlayReceipt
struct GooglePlayReceipt_t2643016893;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_DateTime693205669.h"
#include "Security_UnityEngine_Purchasing_Security_GooglePur2057805495.h"

// System.Void UnityEngine.Purchasing.Security.GooglePlayReceipt::.ctor(System.String,System.String,System.String,System.String,System.DateTime,UnityEngine.Purchasing.Security.GooglePurchaseState)
extern "C"  void GooglePlayReceipt__ctor_m2219689979 (GooglePlayReceipt_t2643016893 * __this, String_t* ___productID0, String_t* ___transactionID1, String_t* ___packageName2, String_t* ___purchaseToken3, DateTime_t693205669  ___purchaseTime4, int32_t ___purchaseState5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Purchasing.Security.GooglePlayReceipt::get_productID()
extern "C"  String_t* GooglePlayReceipt_get_productID_m2031306133 (GooglePlayReceipt_t2643016893 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Purchasing.Security.GooglePlayReceipt::set_productID(System.String)
extern "C"  void GooglePlayReceipt_set_productID_m3807487566 (GooglePlayReceipt_t2643016893 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Purchasing.Security.GooglePlayReceipt::get_transactionID()
extern "C"  String_t* GooglePlayReceipt_get_transactionID_m2766075234 (GooglePlayReceipt_t2643016893 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Purchasing.Security.GooglePlayReceipt::set_transactionID(System.String)
extern "C"  void GooglePlayReceipt_set_transactionID_m1591410145 (GooglePlayReceipt_t2643016893 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Purchasing.Security.GooglePlayReceipt::get_packageName()
extern "C"  String_t* GooglePlayReceipt_get_packageName_m2144183668 (GooglePlayReceipt_t2643016893 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Purchasing.Security.GooglePlayReceipt::set_packageName(System.String)
extern "C"  void GooglePlayReceipt_set_packageName_m1507892305 (GooglePlayReceipt_t2643016893 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Purchasing.Security.GooglePlayReceipt::get_purchaseToken()
extern "C"  String_t* GooglePlayReceipt_get_purchaseToken_m144480463 (GooglePlayReceipt_t2643016893 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Purchasing.Security.GooglePlayReceipt::set_purchaseToken(System.String)
extern "C"  void GooglePlayReceipt_set_purchaseToken_m3110755600 (GooglePlayReceipt_t2643016893 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime UnityEngine.Purchasing.Security.GooglePlayReceipt::get_purchaseDate()
extern "C"  DateTime_t693205669  GooglePlayReceipt_get_purchaseDate_m1942217228 (GooglePlayReceipt_t2643016893 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Purchasing.Security.GooglePlayReceipt::set_purchaseDate(System.DateTime)
extern "C"  void GooglePlayReceipt_set_purchaseDate_m3851393431 (GooglePlayReceipt_t2643016893 * __this, DateTime_t693205669  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Purchasing.Security.GooglePurchaseState UnityEngine.Purchasing.Security.GooglePlayReceipt::get_purchaseState()
extern "C"  int32_t GooglePlayReceipt_get_purchaseState_m3274356139 (GooglePlayReceipt_t2643016893 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Purchasing.Security.GooglePlayReceipt::set_purchaseState(UnityEngine.Purchasing.Security.GooglePurchaseState)
extern "C"  void GooglePlayReceipt_set_purchaseState_m146393314 (GooglePlayReceipt_t2643016893 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
