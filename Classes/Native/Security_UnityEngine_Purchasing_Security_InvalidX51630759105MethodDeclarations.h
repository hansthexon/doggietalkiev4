﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Purchasing.Security.InvalidX509Data
struct InvalidX509Data_t1630759105;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine.Purchasing.Security.InvalidX509Data::.ctor()
extern "C"  void InvalidX509Data__ctor_m1976349147 (InvalidX509Data_t1630759105 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
