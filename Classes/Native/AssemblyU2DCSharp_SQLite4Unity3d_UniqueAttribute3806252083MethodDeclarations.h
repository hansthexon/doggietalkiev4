﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SQLite4Unity3d.UniqueAttribute
struct UniqueAttribute_t3806252083;

#include "codegen/il2cpp-codegen.h"

// System.Void SQLite4Unity3d.UniqueAttribute::.ctor()
extern "C"  void UniqueAttribute__ctor_m559344060 (UniqueAttribute_t3806252083 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SQLite4Unity3d.UniqueAttribute::get_Unique()
extern "C"  bool UniqueAttribute_get_Unique_m3106816514 (UniqueAttribute_t3806252083 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SQLite4Unity3d.UniqueAttribute::set_Unique(System.Boolean)
extern "C"  void UniqueAttribute_set_Unique_m863324745 (UniqueAttribute_t3806252083 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
