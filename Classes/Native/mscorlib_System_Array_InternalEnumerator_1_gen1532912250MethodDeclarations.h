﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1532912250.h"
#include "mscorlib_System_Array3829468939.h"
#include "AssemblyU2DCSharp_SQLite4Unity3d_SQLiteConnection_I674159988.h"

// System.Void System.Array/InternalEnumerator`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3483542273_gshared (InternalEnumerator_1_t1532912250 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m3483542273(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t1532912250 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m3483542273_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3599613109_gshared (InternalEnumerator_1_t1532912250 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3599613109(__this, method) ((  void (*) (InternalEnumerator_1_t1532912250 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3599613109_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3253925289_gshared (InternalEnumerator_1_t1532912250 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3253925289(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t1532912250 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3253925289_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m121797580_gshared (InternalEnumerator_1_t1532912250 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m121797580(__this, method) ((  void (*) (InternalEnumerator_1_t1532912250 *, const MethodInfo*))InternalEnumerator_1_Dispose_m121797580_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2090093981_gshared (InternalEnumerator_1_t1532912250 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m2090093981(__this, method) ((  bool (*) (InternalEnumerator_1_t1532912250 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m2090093981_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::get_Current()
extern "C"  IndexedColumn_t674159988  InternalEnumerator_1_get_Current_m3065343396_gshared (InternalEnumerator_1_t1532912250 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m3065343396(__this, method) ((  IndexedColumn_t674159988  (*) (InternalEnumerator_1_t1532912250 *, const MethodInfo*))InternalEnumerator_1_get_Current_m3065343396_gshared)(__this, method)
