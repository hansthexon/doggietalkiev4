﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.CanvasGroup
struct CanvasGroup_t3296560743;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HandController
struct  HandController_t317030225  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject HandController::codeBit
	GameObject_t1756533147 * ___codeBit_2;
	// UnityEngine.CanvasGroup HandController::cg
	CanvasGroup_t3296560743 * ___cg_3;

public:
	inline static int32_t get_offset_of_codeBit_2() { return static_cast<int32_t>(offsetof(HandController_t317030225, ___codeBit_2)); }
	inline GameObject_t1756533147 * get_codeBit_2() const { return ___codeBit_2; }
	inline GameObject_t1756533147 ** get_address_of_codeBit_2() { return &___codeBit_2; }
	inline void set_codeBit_2(GameObject_t1756533147 * value)
	{
		___codeBit_2 = value;
		Il2CppCodeGenWriteBarrier(&___codeBit_2, value);
	}

	inline static int32_t get_offset_of_cg_3() { return static_cast<int32_t>(offsetof(HandController_t317030225, ___cg_3)); }
	inline CanvasGroup_t3296560743 * get_cg_3() const { return ___cg_3; }
	inline CanvasGroup_t3296560743 ** get_address_of_cg_3() { return &___cg_3; }
	inline void set_cg_3(CanvasGroup_t3296560743 * value)
	{
		___cg_3 = value;
		Il2CppCodeGenWriteBarrier(&___cg_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
