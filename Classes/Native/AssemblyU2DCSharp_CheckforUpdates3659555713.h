﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.UI.Text
struct Text_t356221433;
// System.String
struct String_t;
// System.Collections.Generic.IEnumerable`1<Code>
struct IEnumerable_1_t978294556;
// Code[]
struct CodeU5BU5D_t1855765742;
// DataService
struct DataService_t2602786891;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CheckforUpdates
struct  CheckforUpdates_t3659555713  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject CheckforUpdates::nextlevel
	GameObject_t1756533147 * ___nextlevel_2;
	// System.Int32 CheckforUpdates::i
	int32_t ___i_3;
	// UnityEngine.UI.Text CheckforUpdates::updatetext
	Text_t356221433 * ___updatetext_4;
	// System.String CheckforUpdates::savepath
	String_t* ___savepath_5;
	// System.Collections.Generic.IEnumerable`1<Code> CheckforUpdates::purchased
	Il2CppObject* ___purchased_6;
	// Code[] CheckforUpdates::cr
	CodeU5BU5D_t1855765742* ___cr_7;
	// DataService CheckforUpdates::ds
	DataService_t2602786891 * ___ds_8;

public:
	inline static int32_t get_offset_of_nextlevel_2() { return static_cast<int32_t>(offsetof(CheckforUpdates_t3659555713, ___nextlevel_2)); }
	inline GameObject_t1756533147 * get_nextlevel_2() const { return ___nextlevel_2; }
	inline GameObject_t1756533147 ** get_address_of_nextlevel_2() { return &___nextlevel_2; }
	inline void set_nextlevel_2(GameObject_t1756533147 * value)
	{
		___nextlevel_2 = value;
		Il2CppCodeGenWriteBarrier(&___nextlevel_2, value);
	}

	inline static int32_t get_offset_of_i_3() { return static_cast<int32_t>(offsetof(CheckforUpdates_t3659555713, ___i_3)); }
	inline int32_t get_i_3() const { return ___i_3; }
	inline int32_t* get_address_of_i_3() { return &___i_3; }
	inline void set_i_3(int32_t value)
	{
		___i_3 = value;
	}

	inline static int32_t get_offset_of_updatetext_4() { return static_cast<int32_t>(offsetof(CheckforUpdates_t3659555713, ___updatetext_4)); }
	inline Text_t356221433 * get_updatetext_4() const { return ___updatetext_4; }
	inline Text_t356221433 ** get_address_of_updatetext_4() { return &___updatetext_4; }
	inline void set_updatetext_4(Text_t356221433 * value)
	{
		___updatetext_4 = value;
		Il2CppCodeGenWriteBarrier(&___updatetext_4, value);
	}

	inline static int32_t get_offset_of_savepath_5() { return static_cast<int32_t>(offsetof(CheckforUpdates_t3659555713, ___savepath_5)); }
	inline String_t* get_savepath_5() const { return ___savepath_5; }
	inline String_t** get_address_of_savepath_5() { return &___savepath_5; }
	inline void set_savepath_5(String_t* value)
	{
		___savepath_5 = value;
		Il2CppCodeGenWriteBarrier(&___savepath_5, value);
	}

	inline static int32_t get_offset_of_purchased_6() { return static_cast<int32_t>(offsetof(CheckforUpdates_t3659555713, ___purchased_6)); }
	inline Il2CppObject* get_purchased_6() const { return ___purchased_6; }
	inline Il2CppObject** get_address_of_purchased_6() { return &___purchased_6; }
	inline void set_purchased_6(Il2CppObject* value)
	{
		___purchased_6 = value;
		Il2CppCodeGenWriteBarrier(&___purchased_6, value);
	}

	inline static int32_t get_offset_of_cr_7() { return static_cast<int32_t>(offsetof(CheckforUpdates_t3659555713, ___cr_7)); }
	inline CodeU5BU5D_t1855765742* get_cr_7() const { return ___cr_7; }
	inline CodeU5BU5D_t1855765742** get_address_of_cr_7() { return &___cr_7; }
	inline void set_cr_7(CodeU5BU5D_t1855765742* value)
	{
		___cr_7 = value;
		Il2CppCodeGenWriteBarrier(&___cr_7, value);
	}

	inline static int32_t get_offset_of_ds_8() { return static_cast<int32_t>(offsetof(CheckforUpdates_t3659555713, ___ds_8)); }
	inline DataService_t2602786891 * get_ds_8() const { return ___ds_8; }
	inline DataService_t2602786891 ** get_address_of_ds_8() { return &___ds_8; }
	inline void set_ds_8(DataService_t2602786891 * value)
	{
		___ds_8 = value;
		Il2CppCodeGenWriteBarrier(&___ds_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
