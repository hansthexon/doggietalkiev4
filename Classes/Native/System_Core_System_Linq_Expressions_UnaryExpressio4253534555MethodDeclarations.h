﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Linq.Expressions.UnaryExpression
struct UnaryExpression_t4253534555;
// System.Linq.Expressions.Expression
struct Expression_t114864668;
// System.Type
struct Type_t;

#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Linq_Expressions_ExpressionType1567188220.h"
#include "System_Core_System_Linq_Expressions_Expression114864668.h"
#include "mscorlib_System_Type1303803226.h"

// System.Void System.Linq.Expressions.UnaryExpression::.ctor(System.Linq.Expressions.ExpressionType,System.Linq.Expressions.Expression,System.Type)
extern "C"  void UnaryExpression__ctor_m2271998747 (UnaryExpression_t4253534555 * __this, int32_t ___node_type0, Expression_t114864668 * ___operand1, Type_t * ___type2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Linq.Expressions.Expression System.Linq.Expressions.UnaryExpression::get_Operand()
extern "C"  Expression_t114864668 * UnaryExpression_get_Operand_m2082944639 (UnaryExpression_t4253534555 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
