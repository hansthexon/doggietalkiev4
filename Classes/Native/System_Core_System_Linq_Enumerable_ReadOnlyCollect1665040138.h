﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.ObjectModel.ReadOnlyCollection`1<System.Object>
struct ReadOnlyCollection_1_t2875234987;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Enumerable/ReadOnlyCollectionOf`1<System.Object>
struct  ReadOnlyCollectionOf_1_t1665040138  : public Il2CppObject
{
public:

public:
};

struct ReadOnlyCollectionOf_1_t1665040138_StaticFields
{
public:
	// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Linq.Enumerable/ReadOnlyCollectionOf`1::Empty
	ReadOnlyCollection_1_t2875234987 * ___Empty_0;

public:
	inline static int32_t get_offset_of_Empty_0() { return static_cast<int32_t>(offsetof(ReadOnlyCollectionOf_1_t1665040138_StaticFields, ___Empty_0)); }
	inline ReadOnlyCollection_1_t2875234987 * get_Empty_0() const { return ___Empty_0; }
	inline ReadOnlyCollection_1_t2875234987 ** get_address_of_Empty_0() { return &___Empty_0; }
	inline void set_Empty_0(ReadOnlyCollection_1_t2875234987 * value)
	{
		___Empty_0 = value;
		Il2CppCodeGenWriteBarrier(&___Empty_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
