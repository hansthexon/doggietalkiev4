﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Purchaser
struct Purchaser_t1507804203;
// UnityEngine.UI.Toggle
struct Toggle_t3976754468;
// UnityEngine.UI.Toggle[]
struct ToggleU5BU5D_t1971649997;
// UnityEngine.UI.Image
struct Image_t2042527209;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TogglePack
struct  TogglePack_t1165964989  : public MonoBehaviour_t1158329972
{
public:
	// Purchaser TogglePack::purchaser
	Purchaser_t1507804203 * ___purchaser_2;
	// UnityEngine.UI.Toggle TogglePack::two
	Toggle_t3976754468 * ___two_3;
	// UnityEngine.UI.Toggle TogglePack::one
	Toggle_t3976754468 * ___one_4;
	// UnityEngine.UI.Toggle TogglePack::three
	Toggle_t3976754468 * ___three_5;
	// UnityEngine.UI.Toggle[] TogglePack::togg
	ToggleU5BU5D_t1971649997* ___togg_6;
	// UnityEngine.UI.Image TogglePack::img3
	Image_t2042527209 * ___img3_7;
	// UnityEngine.UI.Image TogglePack::img2
	Image_t2042527209 * ___img2_8;
	// UnityEngine.UI.Image TogglePack::img1
	Image_t2042527209 * ___img1_9;
	// System.Int32 TogglePack::togcheck
	int32_t ___togcheck_10;

public:
	inline static int32_t get_offset_of_purchaser_2() { return static_cast<int32_t>(offsetof(TogglePack_t1165964989, ___purchaser_2)); }
	inline Purchaser_t1507804203 * get_purchaser_2() const { return ___purchaser_2; }
	inline Purchaser_t1507804203 ** get_address_of_purchaser_2() { return &___purchaser_2; }
	inline void set_purchaser_2(Purchaser_t1507804203 * value)
	{
		___purchaser_2 = value;
		Il2CppCodeGenWriteBarrier(&___purchaser_2, value);
	}

	inline static int32_t get_offset_of_two_3() { return static_cast<int32_t>(offsetof(TogglePack_t1165964989, ___two_3)); }
	inline Toggle_t3976754468 * get_two_3() const { return ___two_3; }
	inline Toggle_t3976754468 ** get_address_of_two_3() { return &___two_3; }
	inline void set_two_3(Toggle_t3976754468 * value)
	{
		___two_3 = value;
		Il2CppCodeGenWriteBarrier(&___two_3, value);
	}

	inline static int32_t get_offset_of_one_4() { return static_cast<int32_t>(offsetof(TogglePack_t1165964989, ___one_4)); }
	inline Toggle_t3976754468 * get_one_4() const { return ___one_4; }
	inline Toggle_t3976754468 ** get_address_of_one_4() { return &___one_4; }
	inline void set_one_4(Toggle_t3976754468 * value)
	{
		___one_4 = value;
		Il2CppCodeGenWriteBarrier(&___one_4, value);
	}

	inline static int32_t get_offset_of_three_5() { return static_cast<int32_t>(offsetof(TogglePack_t1165964989, ___three_5)); }
	inline Toggle_t3976754468 * get_three_5() const { return ___three_5; }
	inline Toggle_t3976754468 ** get_address_of_three_5() { return &___three_5; }
	inline void set_three_5(Toggle_t3976754468 * value)
	{
		___three_5 = value;
		Il2CppCodeGenWriteBarrier(&___three_5, value);
	}

	inline static int32_t get_offset_of_togg_6() { return static_cast<int32_t>(offsetof(TogglePack_t1165964989, ___togg_6)); }
	inline ToggleU5BU5D_t1971649997* get_togg_6() const { return ___togg_6; }
	inline ToggleU5BU5D_t1971649997** get_address_of_togg_6() { return &___togg_6; }
	inline void set_togg_6(ToggleU5BU5D_t1971649997* value)
	{
		___togg_6 = value;
		Il2CppCodeGenWriteBarrier(&___togg_6, value);
	}

	inline static int32_t get_offset_of_img3_7() { return static_cast<int32_t>(offsetof(TogglePack_t1165964989, ___img3_7)); }
	inline Image_t2042527209 * get_img3_7() const { return ___img3_7; }
	inline Image_t2042527209 ** get_address_of_img3_7() { return &___img3_7; }
	inline void set_img3_7(Image_t2042527209 * value)
	{
		___img3_7 = value;
		Il2CppCodeGenWriteBarrier(&___img3_7, value);
	}

	inline static int32_t get_offset_of_img2_8() { return static_cast<int32_t>(offsetof(TogglePack_t1165964989, ___img2_8)); }
	inline Image_t2042527209 * get_img2_8() const { return ___img2_8; }
	inline Image_t2042527209 ** get_address_of_img2_8() { return &___img2_8; }
	inline void set_img2_8(Image_t2042527209 * value)
	{
		___img2_8 = value;
		Il2CppCodeGenWriteBarrier(&___img2_8, value);
	}

	inline static int32_t get_offset_of_img1_9() { return static_cast<int32_t>(offsetof(TogglePack_t1165964989, ___img1_9)); }
	inline Image_t2042527209 * get_img1_9() const { return ___img1_9; }
	inline Image_t2042527209 ** get_address_of_img1_9() { return &___img1_9; }
	inline void set_img1_9(Image_t2042527209 * value)
	{
		___img1_9 = value;
		Il2CppCodeGenWriteBarrier(&___img1_9, value);
	}

	inline static int32_t get_offset_of_togcheck_10() { return static_cast<int32_t>(offsetof(TogglePack_t1165964989, ___togcheck_10)); }
	inline int32_t get_togcheck_10() const { return ___togcheck_10; }
	inline int32_t* get_address_of_togcheck_10() { return &___togcheck_10; }
	inline void set_togcheck_10(int32_t value)
	{
		___togcheck_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
