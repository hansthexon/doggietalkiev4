﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Vuforia.PropAbstractBehaviour
struct PropAbstractBehaviour_t1047177596;
// Vuforia.Prop
struct Prop_t444071959;
// UnityEngine.MeshFilter
struct MeshFilter_t3026937449;
// UnityEngine.MeshCollider
struct MeshCollider_t2718867283;
// UnityEngine.BoxCollider
struct BoxCollider_t22920061;
// UnityEngine.Transform
struct Transform_t3275118058;
// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_MeshFilter3026937449.h"
#include "UnityEngine_UnityEngine_MeshCollider2718867283.h"
#include "UnityEngine_UnityEngine_BoxCollider22920061.h"

// Vuforia.Prop Vuforia.PropAbstractBehaviour::get_Prop()
extern "C"  Il2CppObject * PropAbstractBehaviour_get_Prop_m1610298944 (PropAbstractBehaviour_t1047177596 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.PropAbstractBehaviour::UpdateMeshAndColliders()
extern "C"  void PropAbstractBehaviour_UpdateMeshAndColliders_m3053797708 (PropAbstractBehaviour_t1047177596 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.PropAbstractBehaviour::Start()
extern "C"  void PropAbstractBehaviour_Start_m2122559776 (PropAbstractBehaviour_t1047177596 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.PropAbstractBehaviour::InternalUnregisterTrackable()
extern "C"  void PropAbstractBehaviour_InternalUnregisterTrackable_m3524041866 (PropAbstractBehaviour_t1047177596 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.PropAbstractBehaviour::Vuforia.IEditorPropBehaviour.InitializeProp(Vuforia.Prop)
extern "C"  void PropAbstractBehaviour_Vuforia_IEditorPropBehaviour_InitializeProp_m218517900 (PropAbstractBehaviour_t1047177596 * __this, Il2CppObject * ___prop0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.PropAbstractBehaviour::Vuforia.IEditorPropBehaviour.SetMeshFilterToUpdate(UnityEngine.MeshFilter)
extern "C"  void PropAbstractBehaviour_Vuforia_IEditorPropBehaviour_SetMeshFilterToUpdate_m2735627517 (PropAbstractBehaviour_t1047177596 * __this, MeshFilter_t3026937449 * ___meshFilterToUpdate0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.MeshFilter Vuforia.PropAbstractBehaviour::Vuforia.IEditorPropBehaviour.get_MeshFilterToUpdate()
extern "C"  MeshFilter_t3026937449 * PropAbstractBehaviour_Vuforia_IEditorPropBehaviour_get_MeshFilterToUpdate_m3501024913 (PropAbstractBehaviour_t1047177596 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.PropAbstractBehaviour::Vuforia.IEditorPropBehaviour.SetMeshColliderToUpdate(UnityEngine.MeshCollider)
extern "C"  void PropAbstractBehaviour_Vuforia_IEditorPropBehaviour_SetMeshColliderToUpdate_m3407600113 (PropAbstractBehaviour_t1047177596 * __this, MeshCollider_t2718867283 * ___meshColliderToUpdate0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.MeshCollider Vuforia.PropAbstractBehaviour::Vuforia.IEditorPropBehaviour.get_MeshColliderToUpdate()
extern "C"  MeshCollider_t2718867283 * PropAbstractBehaviour_Vuforia_IEditorPropBehaviour_get_MeshColliderToUpdate_m3633722805 (PropAbstractBehaviour_t1047177596 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.PropAbstractBehaviour::Vuforia.IEditorPropBehaviour.SetBoxColliderToUpdate(UnityEngine.BoxCollider)
extern "C"  void PropAbstractBehaviour_Vuforia_IEditorPropBehaviour_SetBoxColliderToUpdate_m3593252065 (PropAbstractBehaviour_t1047177596 * __this, BoxCollider_t22920061 * ___boxColliderToUpdate0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.BoxCollider Vuforia.PropAbstractBehaviour::Vuforia.IEditorPropBehaviour.get_BoxColliderToUpdate()
extern "C"  BoxCollider_t22920061 * PropAbstractBehaviour_Vuforia_IEditorPropBehaviour_get_BoxColliderToUpdate_m3311938481 (PropAbstractBehaviour_t1047177596 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.PropAbstractBehaviour::.ctor()
extern "C"  void PropAbstractBehaviour__ctor_m1260771068 (PropAbstractBehaviour_t1047177596 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.PropAbstractBehaviour::Vuforia.IEditorTrackableBehaviour.get_enabled()
extern "C"  bool PropAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_enabled_m3553387256 (PropAbstractBehaviour_t1047177596 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.PropAbstractBehaviour::Vuforia.IEditorTrackableBehaviour.set_enabled(System.Boolean)
extern "C"  void PropAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_set_enabled_m921463721 (PropAbstractBehaviour_t1047177596 * __this, bool p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform Vuforia.PropAbstractBehaviour::Vuforia.IEditorTrackableBehaviour.get_transform()
extern "C"  Transform_t3275118058 * PropAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_transform_m685385407 (PropAbstractBehaviour_t1047177596 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject Vuforia.PropAbstractBehaviour::Vuforia.IEditorTrackableBehaviour.get_gameObject()
extern "C"  GameObject_t1756533147 * PropAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_gameObject_m2784170763 (PropAbstractBehaviour_t1047177596 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
