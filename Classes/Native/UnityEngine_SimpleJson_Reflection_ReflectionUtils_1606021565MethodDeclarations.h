﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SimpleJson.Reflection.ReflectionUtils/<GetConstructorByReflection>c__AnonStorey2
struct U3CGetConstructorByReflectionU3Ec__AnonStorey2_t1606021565;
// System.Object
struct Il2CppObject;
// System.Object[]
struct ObjectU5BU5D_t3614634134;

#include "codegen/il2cpp-codegen.h"

// System.Void SimpleJson.Reflection.ReflectionUtils/<GetConstructorByReflection>c__AnonStorey2::.ctor()
extern "C"  void U3CGetConstructorByReflectionU3Ec__AnonStorey2__ctor_m3579372293 (U3CGetConstructorByReflectionU3Ec__AnonStorey2_t1606021565 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object SimpleJson.Reflection.ReflectionUtils/<GetConstructorByReflection>c__AnonStorey2::<>m__6(System.Object[])
extern "C"  Il2CppObject * U3CGetConstructorByReflectionU3Ec__AnonStorey2_U3CU3Em__6_m3432422969 (U3CGetConstructorByReflectionU3Ec__AnonStorey2_t1606021565 * __this, ObjectU5BU5D_t3614634134* ___args0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
