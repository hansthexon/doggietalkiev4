﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Linq.Enumerable/<CreateSkipIterator>c__Iterator16`1<System.Byte>
struct U3CCreateSkipIteratorU3Ec__Iterator16_1_t4110212133;
// System.Object
struct Il2CppObject;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Collections.Generic.IEnumerator`1<System.Byte>
struct IEnumerator_1_t1158628263;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Linq.Enumerable/<CreateSkipIterator>c__Iterator16`1<System.Byte>::.ctor()
extern "C"  void U3CCreateSkipIteratorU3Ec__Iterator16_1__ctor_m2289567308_gshared (U3CCreateSkipIteratorU3Ec__Iterator16_1_t4110212133 * __this, const MethodInfo* method);
#define U3CCreateSkipIteratorU3Ec__Iterator16_1__ctor_m2289567308(__this, method) ((  void (*) (U3CCreateSkipIteratorU3Ec__Iterator16_1_t4110212133 *, const MethodInfo*))U3CCreateSkipIteratorU3Ec__Iterator16_1__ctor_m2289567308_gshared)(__this, method)
// TSource System.Linq.Enumerable/<CreateSkipIterator>c__Iterator16`1<System.Byte>::System.Collections.Generic.IEnumerator<TSource>.get_Current()
extern "C"  uint8_t U3CCreateSkipIteratorU3Ec__Iterator16_1_System_Collections_Generic_IEnumeratorU3CTSourceU3E_get_Current_m1116040841_gshared (U3CCreateSkipIteratorU3Ec__Iterator16_1_t4110212133 * __this, const MethodInfo* method);
#define U3CCreateSkipIteratorU3Ec__Iterator16_1_System_Collections_Generic_IEnumeratorU3CTSourceU3E_get_Current_m1116040841(__this, method) ((  uint8_t (*) (U3CCreateSkipIteratorU3Ec__Iterator16_1_t4110212133 *, const MethodInfo*))U3CCreateSkipIteratorU3Ec__Iterator16_1_System_Collections_Generic_IEnumeratorU3CTSourceU3E_get_Current_m1116040841_gshared)(__this, method)
// System.Object System.Linq.Enumerable/<CreateSkipIterator>c__Iterator16`1<System.Byte>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CCreateSkipIteratorU3Ec__Iterator16_1_System_Collections_IEnumerator_get_Current_m2585175094_gshared (U3CCreateSkipIteratorU3Ec__Iterator16_1_t4110212133 * __this, const MethodInfo* method);
#define U3CCreateSkipIteratorU3Ec__Iterator16_1_System_Collections_IEnumerator_get_Current_m2585175094(__this, method) ((  Il2CppObject * (*) (U3CCreateSkipIteratorU3Ec__Iterator16_1_t4110212133 *, const MethodInfo*))U3CCreateSkipIteratorU3Ec__Iterator16_1_System_Collections_IEnumerator_get_Current_m2585175094_gshared)(__this, method)
// System.Collections.IEnumerator System.Linq.Enumerable/<CreateSkipIterator>c__Iterator16`1<System.Byte>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CCreateSkipIteratorU3Ec__Iterator16_1_System_Collections_IEnumerable_GetEnumerator_m2969639421_gshared (U3CCreateSkipIteratorU3Ec__Iterator16_1_t4110212133 * __this, const MethodInfo* method);
#define U3CCreateSkipIteratorU3Ec__Iterator16_1_System_Collections_IEnumerable_GetEnumerator_m2969639421(__this, method) ((  Il2CppObject * (*) (U3CCreateSkipIteratorU3Ec__Iterator16_1_t4110212133 *, const MethodInfo*))U3CCreateSkipIteratorU3Ec__Iterator16_1_System_Collections_IEnumerable_GetEnumerator_m2969639421_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable/<CreateSkipIterator>c__Iterator16`1<System.Byte>::System.Collections.Generic.IEnumerable<TSource>.GetEnumerator()
extern "C"  Il2CppObject* U3CCreateSkipIteratorU3Ec__Iterator16_1_System_Collections_Generic_IEnumerableU3CTSourceU3E_GetEnumerator_m35805784_gshared (U3CCreateSkipIteratorU3Ec__Iterator16_1_t4110212133 * __this, const MethodInfo* method);
#define U3CCreateSkipIteratorU3Ec__Iterator16_1_System_Collections_Generic_IEnumerableU3CTSourceU3E_GetEnumerator_m35805784(__this, method) ((  Il2CppObject* (*) (U3CCreateSkipIteratorU3Ec__Iterator16_1_t4110212133 *, const MethodInfo*))U3CCreateSkipIteratorU3Ec__Iterator16_1_System_Collections_Generic_IEnumerableU3CTSourceU3E_GetEnumerator_m35805784_gshared)(__this, method)
// System.Boolean System.Linq.Enumerable/<CreateSkipIterator>c__Iterator16`1<System.Byte>::MoveNext()
extern "C"  bool U3CCreateSkipIteratorU3Ec__Iterator16_1_MoveNext_m1270305416_gshared (U3CCreateSkipIteratorU3Ec__Iterator16_1_t4110212133 * __this, const MethodInfo* method);
#define U3CCreateSkipIteratorU3Ec__Iterator16_1_MoveNext_m1270305416(__this, method) ((  bool (*) (U3CCreateSkipIteratorU3Ec__Iterator16_1_t4110212133 *, const MethodInfo*))U3CCreateSkipIteratorU3Ec__Iterator16_1_MoveNext_m1270305416_gshared)(__this, method)
// System.Void System.Linq.Enumerable/<CreateSkipIterator>c__Iterator16`1<System.Byte>::Dispose()
extern "C"  void U3CCreateSkipIteratorU3Ec__Iterator16_1_Dispose_m4119129207_gshared (U3CCreateSkipIteratorU3Ec__Iterator16_1_t4110212133 * __this, const MethodInfo* method);
#define U3CCreateSkipIteratorU3Ec__Iterator16_1_Dispose_m4119129207(__this, method) ((  void (*) (U3CCreateSkipIteratorU3Ec__Iterator16_1_t4110212133 *, const MethodInfo*))U3CCreateSkipIteratorU3Ec__Iterator16_1_Dispose_m4119129207_gshared)(__this, method)
// System.Void System.Linq.Enumerable/<CreateSkipIterator>c__Iterator16`1<System.Byte>::Reset()
extern "C"  void U3CCreateSkipIteratorU3Ec__Iterator16_1_Reset_m3005396553_gshared (U3CCreateSkipIteratorU3Ec__Iterator16_1_t4110212133 * __this, const MethodInfo* method);
#define U3CCreateSkipIteratorU3Ec__Iterator16_1_Reset_m3005396553(__this, method) ((  void (*) (U3CCreateSkipIteratorU3Ec__Iterator16_1_t4110212133 *, const MethodInfo*))U3CCreateSkipIteratorU3Ec__Iterator16_1_Reset_m3005396553_gshared)(__this, method)
// System.Void System.Linq.Enumerable/<CreateSkipIterator>c__Iterator16`1<System.Byte>::<>__Finally0()
extern "C"  void U3CCreateSkipIteratorU3Ec__Iterator16_1_U3CU3E__Finally0_m1846418611_gshared (U3CCreateSkipIteratorU3Ec__Iterator16_1_t4110212133 * __this, const MethodInfo* method);
#define U3CCreateSkipIteratorU3Ec__Iterator16_1_U3CU3E__Finally0_m1846418611(__this, method) ((  void (*) (U3CCreateSkipIteratorU3Ec__Iterator16_1_t4110212133 *, const MethodInfo*))U3CCreateSkipIteratorU3Ec__Iterator16_1_U3CU3E__Finally0_m1846418611_gshared)(__this, method)
