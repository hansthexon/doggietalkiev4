﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_DateTime693205669.h"
#include "Security_UnityEngine_Purchasing_Security_GooglePur2057805495.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.Security.GooglePlayReceipt
struct  GooglePlayReceipt_t2643016893  : public Il2CppObject
{
public:
	// System.String UnityEngine.Purchasing.Security.GooglePlayReceipt::<productID>k__BackingField
	String_t* ___U3CproductIDU3Ek__BackingField_0;
	// System.String UnityEngine.Purchasing.Security.GooglePlayReceipt::<transactionID>k__BackingField
	String_t* ___U3CtransactionIDU3Ek__BackingField_1;
	// System.String UnityEngine.Purchasing.Security.GooglePlayReceipt::<packageName>k__BackingField
	String_t* ___U3CpackageNameU3Ek__BackingField_2;
	// System.String UnityEngine.Purchasing.Security.GooglePlayReceipt::<purchaseToken>k__BackingField
	String_t* ___U3CpurchaseTokenU3Ek__BackingField_3;
	// System.DateTime UnityEngine.Purchasing.Security.GooglePlayReceipt::<purchaseDate>k__BackingField
	DateTime_t693205669  ___U3CpurchaseDateU3Ek__BackingField_4;
	// UnityEngine.Purchasing.Security.GooglePurchaseState UnityEngine.Purchasing.Security.GooglePlayReceipt::<purchaseState>k__BackingField
	int32_t ___U3CpurchaseStateU3Ek__BackingField_5;

public:
	inline static int32_t get_offset_of_U3CproductIDU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(GooglePlayReceipt_t2643016893, ___U3CproductIDU3Ek__BackingField_0)); }
	inline String_t* get_U3CproductIDU3Ek__BackingField_0() const { return ___U3CproductIDU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CproductIDU3Ek__BackingField_0() { return &___U3CproductIDU3Ek__BackingField_0; }
	inline void set_U3CproductIDU3Ek__BackingField_0(String_t* value)
	{
		___U3CproductIDU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CproductIDU3Ek__BackingField_0, value);
	}

	inline static int32_t get_offset_of_U3CtransactionIDU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(GooglePlayReceipt_t2643016893, ___U3CtransactionIDU3Ek__BackingField_1)); }
	inline String_t* get_U3CtransactionIDU3Ek__BackingField_1() const { return ___U3CtransactionIDU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CtransactionIDU3Ek__BackingField_1() { return &___U3CtransactionIDU3Ek__BackingField_1; }
	inline void set_U3CtransactionIDU3Ek__BackingField_1(String_t* value)
	{
		___U3CtransactionIDU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CtransactionIDU3Ek__BackingField_1, value);
	}

	inline static int32_t get_offset_of_U3CpackageNameU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(GooglePlayReceipt_t2643016893, ___U3CpackageNameU3Ek__BackingField_2)); }
	inline String_t* get_U3CpackageNameU3Ek__BackingField_2() const { return ___U3CpackageNameU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CpackageNameU3Ek__BackingField_2() { return &___U3CpackageNameU3Ek__BackingField_2; }
	inline void set_U3CpackageNameU3Ek__BackingField_2(String_t* value)
	{
		___U3CpackageNameU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CpackageNameU3Ek__BackingField_2, value);
	}

	inline static int32_t get_offset_of_U3CpurchaseTokenU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(GooglePlayReceipt_t2643016893, ___U3CpurchaseTokenU3Ek__BackingField_3)); }
	inline String_t* get_U3CpurchaseTokenU3Ek__BackingField_3() const { return ___U3CpurchaseTokenU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3CpurchaseTokenU3Ek__BackingField_3() { return &___U3CpurchaseTokenU3Ek__BackingField_3; }
	inline void set_U3CpurchaseTokenU3Ek__BackingField_3(String_t* value)
	{
		___U3CpurchaseTokenU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CpurchaseTokenU3Ek__BackingField_3, value);
	}

	inline static int32_t get_offset_of_U3CpurchaseDateU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(GooglePlayReceipt_t2643016893, ___U3CpurchaseDateU3Ek__BackingField_4)); }
	inline DateTime_t693205669  get_U3CpurchaseDateU3Ek__BackingField_4() const { return ___U3CpurchaseDateU3Ek__BackingField_4; }
	inline DateTime_t693205669 * get_address_of_U3CpurchaseDateU3Ek__BackingField_4() { return &___U3CpurchaseDateU3Ek__BackingField_4; }
	inline void set_U3CpurchaseDateU3Ek__BackingField_4(DateTime_t693205669  value)
	{
		___U3CpurchaseDateU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3CpurchaseStateU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(GooglePlayReceipt_t2643016893, ___U3CpurchaseStateU3Ek__BackingField_5)); }
	inline int32_t get_U3CpurchaseStateU3Ek__BackingField_5() const { return ___U3CpurchaseStateU3Ek__BackingField_5; }
	inline int32_t* get_address_of_U3CpurchaseStateU3Ek__BackingField_5() { return &___U3CpurchaseStateU3Ek__BackingField_5; }
	inline void set_U3CpurchaseStateU3Ek__BackingField_5(int32_t value)
	{
		___U3CpurchaseStateU3Ek__BackingField_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
