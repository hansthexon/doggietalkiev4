﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SQLite4Unity3d.SQLiteConnection/<UpdateAll>c__AnonStorey15
struct U3CUpdateAllU3Ec__AnonStorey15_t304063104;

#include "codegen/il2cpp-codegen.h"

// System.Void SQLite4Unity3d.SQLiteConnection/<UpdateAll>c__AnonStorey15::.ctor()
extern "C"  void U3CUpdateAllU3Ec__AnonStorey15__ctor_m3579265697 (U3CUpdateAllU3Ec__AnonStorey15_t304063104 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SQLite4Unity3d.SQLiteConnection/<UpdateAll>c__AnonStorey15::<>m__B()
extern "C"  void U3CUpdateAllU3Ec__AnonStorey15_U3CU3Em__B_m171804700 (U3CUpdateAllU3Ec__AnonStorey15_t304063104 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
