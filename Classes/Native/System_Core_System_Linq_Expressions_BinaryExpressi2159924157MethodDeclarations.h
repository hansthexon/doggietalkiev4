﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Linq.Expressions.BinaryExpression
struct BinaryExpression_t2159924157;
// System.Type
struct Type_t;
// System.Linq.Expressions.Expression
struct Expression_t114864668;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Linq.Expressions.LambdaExpression
struct LambdaExpression_t2811402413;

#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Linq_Expressions_ExpressionType1567188220.h"
#include "mscorlib_System_Type1303803226.h"
#include "System_Core_System_Linq_Expressions_Expression114864668.h"
#include "mscorlib_System_Reflection_MethodInfo3330546337.h"
#include "System_Core_System_Linq_Expressions_LambdaExpressi2811402413.h"

// System.Void System.Linq.Expressions.BinaryExpression::.ctor(System.Linq.Expressions.ExpressionType,System.Type,System.Linq.Expressions.Expression,System.Linq.Expressions.Expression,System.Boolean,System.Boolean,System.Reflection.MethodInfo,System.Linq.Expressions.LambdaExpression)
extern "C"  void BinaryExpression__ctor_m2887677801 (BinaryExpression_t2159924157 * __this, int32_t ___node_type0, Type_t * ___type1, Expression_t114864668 * ___left2, Expression_t114864668 * ___right3, bool ___lift_to_null4, bool ___is_lifted5, MethodInfo_t * ___method6, LambdaExpression_t2811402413 * ___conversion7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Linq.Expressions.Expression System.Linq.Expressions.BinaryExpression::get_Left()
extern "C"  Expression_t114864668 * BinaryExpression_get_Left_m2831265245 (BinaryExpression_t2159924157 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Linq.Expressions.Expression System.Linq.Expressions.BinaryExpression::get_Right()
extern "C"  Expression_t114864668 * BinaryExpression_get_Right_m1785810714 (BinaryExpression_t2159924157 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Linq.Expressions.LambdaExpression System.Linq.Expressions.BinaryExpression::get_Conversion()
extern "C"  LambdaExpression_t2811402413 * BinaryExpression_get_Conversion_m2986757001 (BinaryExpression_t2159924157 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
