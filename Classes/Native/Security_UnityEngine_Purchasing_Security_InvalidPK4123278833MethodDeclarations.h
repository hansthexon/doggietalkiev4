﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Purchasing.Security.InvalidPKCS7Data
struct InvalidPKCS7Data_t4123278833;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine.Purchasing.Security.InvalidPKCS7Data::.ctor()
extern "C"  void InvalidPKCS7Data__ctor_m1217025319 (InvalidPKCS7Data_t4123278833 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
