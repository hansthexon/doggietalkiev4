﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>
struct Dictionary_2_t955353609;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V2641886373.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl1947527974.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m1890809706_gshared (Enumerator_t2641886373 * __this, Dictionary_2_t955353609 * ___host0, const MethodInfo* method);
#define Enumerator__ctor_m1890809706(__this, ___host0, method) ((  void (*) (Enumerator_t2641886373 *, Dictionary_2_t955353609 *, const MethodInfo*))Enumerator__ctor_m1890809706_gshared)(__this, ___host0, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2383967835_gshared (Enumerator_t2641886373 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m2383967835(__this, method) ((  Il2CppObject * (*) (Enumerator_t2641886373 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2383967835_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1768747399_gshared (Enumerator_t2641886373 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m1768747399(__this, method) ((  void (*) (Enumerator_t2641886373 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m1768747399_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::Dispose()
extern "C"  void Enumerator_Dispose_m3234185590_gshared (Enumerator_t2641886373 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m3234185590(__this, method) ((  void (*) (Enumerator_t2641886373 *, const MethodInfo*))Enumerator_Dispose_m3234185590_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m2712720291_gshared (Enumerator_t2641886373 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m2712720291(__this, method) ((  bool (*) (Enumerator_t2641886373 *, const MethodInfo*))Enumerator_MoveNext_m2712720291_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::get_Current()
extern "C"  TrackableResultData_t1947527974  Enumerator_get_Current_m249448441_gshared (Enumerator_t2641886373 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m249448441(__this, method) ((  TrackableResultData_t1947527974  (*) (Enumerator_t2641886373 *, const MethodInfo*))Enumerator_get_Current_m249448441_gshared)(__this, method)
