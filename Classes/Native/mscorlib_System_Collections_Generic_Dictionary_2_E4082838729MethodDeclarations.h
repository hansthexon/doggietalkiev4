﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E1760119595MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,SQLite4Unity3d.SQLiteConnection/IndexInfo>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m2305481560(__this, ___dictionary0, method) ((  void (*) (Enumerator_t4082838729 *, Dictionary_2_t2762814027 *, const MethodInfo*))Enumerator__ctor_m811214818_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,SQLite4Unity3d.SQLiteConnection/IndexInfo>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m1158496167(__this, method) ((  Il2CppObject * (*) (Enumerator_t4082838729 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m4203850179_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,SQLite4Unity3d.SQLiteConnection/IndexInfo>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m2551838755(__this, method) ((  void (*) (Enumerator_t4082838729 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m24795391_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.String,SQLite4Unity3d.SQLiteConnection/IndexInfo>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m2818148942(__this, method) ((  DictionaryEntry_t3048875398  (*) (Enumerator_t4082838729 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m2251395880_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,SQLite4Unity3d.SQLiteConnection/IndexInfo>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1422164749(__this, method) ((  Il2CppObject * (*) (Enumerator_t4082838729 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m4171739561_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,SQLite4Unity3d.SQLiteConnection/IndexInfo>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2677453029(__this, method) ((  Il2CppObject * (*) (Enumerator_t4082838729 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2176216665_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.String,SQLite4Unity3d.SQLiteConnection/IndexInfo>::MoveNext()
#define Enumerator_MoveNext_m1392909495(__this, method) ((  bool (*) (Enumerator_t4082838729 *, const MethodInfo*))Enumerator_MoveNext_m3944863691_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.String,SQLite4Unity3d.SQLiteConnection/IndexInfo>::get_Current()
#define Enumerator_get_Current_m4287614303(__this, method) ((  KeyValuePair_2_t520159249  (*) (Enumerator_t4082838729 *, const MethodInfo*))Enumerator_get_Current_m505501491_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.String,SQLite4Unity3d.SQLiteConnection/IndexInfo>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m2101160736(__this, method) ((  String_t* (*) (Enumerator_t4082838729 *, const MethodInfo*))Enumerator_get_CurrentKey_m2900523578_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.String,SQLite4Unity3d.SQLiteConnection/IndexInfo>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m3296851744(__this, method) ((  IndexInfo_t848034765  (*) (Enumerator_t4082838729 *, const MethodInfo*))Enumerator_get_CurrentValue_m2282140538_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,SQLite4Unity3d.SQLiteConnection/IndexInfo>::Reset()
#define Enumerator_Reset_m387298202(__this, method) ((  void (*) (Enumerator_t4082838729 *, const MethodInfo*))Enumerator_Reset_m2958379748_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,SQLite4Unity3d.SQLiteConnection/IndexInfo>::VerifyState()
#define Enumerator_VerifyState_m187850545(__this, method) ((  void (*) (Enumerator_t4082838729 *, const MethodInfo*))Enumerator_VerifyState_m2402519621_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,SQLite4Unity3d.SQLiteConnection/IndexInfo>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m1204776147(__this, method) ((  void (*) (Enumerator_t4082838729 *, const MethodInfo*))Enumerator_VerifyCurrent_m207879567_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,SQLite4Unity3d.SQLiteConnection/IndexInfo>::Dispose()
#define Enumerator_Dispose_m3548107444(__this, method) ((  void (*) (Enumerator_t4082838729 *, const MethodInfo*))Enumerator_Dispose_m1427874974_gshared)(__this, method)
