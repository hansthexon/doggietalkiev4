﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_LitJson_ImporterFunc_2_gen2548729379MethodDeclarations.h"

// System.Void LitJson.ImporterFunc`2<System.String,System.Type>::.ctor(System.Object,System.IntPtr)
#define ImporterFunc_2__ctor_m3114694053(__this, ___object0, ___method1, method) ((  void (*) (ImporterFunc_2_t3485802444 *, Il2CppObject *, IntPtr_t, const MethodInfo*))ImporterFunc_2__ctor_m2910045588_gshared)(__this, ___object0, ___method1, method)
// TValue LitJson.ImporterFunc`2<System.String,System.Type>::Invoke(TJson)
#define ImporterFunc_2_Invoke_m416575689(__this, ___input0, method) ((  Type_t * (*) (ImporterFunc_2_t3485802444 *, String_t*, const MethodInfo*))ImporterFunc_2_Invoke_m3373032134_gshared)(__this, ___input0, method)
// System.IAsyncResult LitJson.ImporterFunc`2<System.String,System.Type>::BeginInvoke(TJson,System.AsyncCallback,System.Object)
#define ImporterFunc_2_BeginInvoke_m969799506(__this, ___input0, ___callback1, ___object2, method) ((  Il2CppObject * (*) (ImporterFunc_2_t3485802444 *, String_t*, AsyncCallback_t163412349 *, Il2CppObject *, const MethodInfo*))ImporterFunc_2_BeginInvoke_m2065437887_gshared)(__this, ___input0, ___callback1, ___object2, method)
// TValue LitJson.ImporterFunc`2<System.String,System.Type>::EndInvoke(System.IAsyncResult)
#define ImporterFunc_2_EndInvoke_m2639825599(__this, ___result0, method) ((  Type_t * (*) (ImporterFunc_2_t3485802444 *, Il2CppObject *, const MethodInfo*))ImporterFunc_2_EndInvoke_m2375822358_gshared)(__this, ___result0, method)
