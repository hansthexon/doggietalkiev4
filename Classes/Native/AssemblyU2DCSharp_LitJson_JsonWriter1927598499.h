﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Globalization.NumberFormatInfo
struct NumberFormatInfo_t104580544;
// LitJson.WriterContext
struct WriterContext_t4137194742;
// System.Collections.Generic.Stack`1<LitJson.WriterContext>
struct Stack_1_t929955600;
// System.Char[]
struct CharU5BU5D_t1328083999;
// System.Text.StringBuilder
struct StringBuilder_t1221177846;
// System.String
struct String_t;
// System.IO.TextWriter
struct TextWriter_t4027217640;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LitJson.JsonWriter
struct  JsonWriter_t1927598499  : public Il2CppObject
{
public:
	// LitJson.WriterContext LitJson.JsonWriter::context
	WriterContext_t4137194742 * ___context_1;
	// System.Collections.Generic.Stack`1<LitJson.WriterContext> LitJson.JsonWriter::ctxStack
	Stack_1_t929955600 * ___ctxStack_2;
	// System.Boolean LitJson.JsonWriter::hasReachedEnd
	bool ___hasReachedEnd_3;
	// System.Char[] LitJson.JsonWriter::hexSeq
	CharU5BU5D_t1328083999* ___hexSeq_4;
	// System.Int32 LitJson.JsonWriter::indentation
	int32_t ___indentation_5;
	// System.Int32 LitJson.JsonWriter::indentValue
	int32_t ___indentValue_6;
	// System.Text.StringBuilder LitJson.JsonWriter::stringBuilder
	StringBuilder_t1221177846 * ___stringBuilder_7;
	// System.Boolean LitJson.JsonWriter::<PrettyPrint>k__BackingField
	bool ___U3CPrettyPrintU3Ek__BackingField_8;
	// System.Boolean LitJson.JsonWriter::<Validate>k__BackingField
	bool ___U3CValidateU3Ek__BackingField_9;
	// System.Boolean LitJson.JsonWriter::<TypeHinting>k__BackingField
	bool ___U3CTypeHintingU3Ek__BackingField_10;
	// System.String LitJson.JsonWriter::<HintTypeName>k__BackingField
	String_t* ___U3CHintTypeNameU3Ek__BackingField_11;
	// System.String LitJson.JsonWriter::<HintValueName>k__BackingField
	String_t* ___U3CHintValueNameU3Ek__BackingField_12;
	// System.IO.TextWriter LitJson.JsonWriter::<TextWriter>k__BackingField
	TextWriter_t4027217640 * ___U3CTextWriterU3Ek__BackingField_13;

public:
	inline static int32_t get_offset_of_context_1() { return static_cast<int32_t>(offsetof(JsonWriter_t1927598499, ___context_1)); }
	inline WriterContext_t4137194742 * get_context_1() const { return ___context_1; }
	inline WriterContext_t4137194742 ** get_address_of_context_1() { return &___context_1; }
	inline void set_context_1(WriterContext_t4137194742 * value)
	{
		___context_1 = value;
		Il2CppCodeGenWriteBarrier(&___context_1, value);
	}

	inline static int32_t get_offset_of_ctxStack_2() { return static_cast<int32_t>(offsetof(JsonWriter_t1927598499, ___ctxStack_2)); }
	inline Stack_1_t929955600 * get_ctxStack_2() const { return ___ctxStack_2; }
	inline Stack_1_t929955600 ** get_address_of_ctxStack_2() { return &___ctxStack_2; }
	inline void set_ctxStack_2(Stack_1_t929955600 * value)
	{
		___ctxStack_2 = value;
		Il2CppCodeGenWriteBarrier(&___ctxStack_2, value);
	}

	inline static int32_t get_offset_of_hasReachedEnd_3() { return static_cast<int32_t>(offsetof(JsonWriter_t1927598499, ___hasReachedEnd_3)); }
	inline bool get_hasReachedEnd_3() const { return ___hasReachedEnd_3; }
	inline bool* get_address_of_hasReachedEnd_3() { return &___hasReachedEnd_3; }
	inline void set_hasReachedEnd_3(bool value)
	{
		___hasReachedEnd_3 = value;
	}

	inline static int32_t get_offset_of_hexSeq_4() { return static_cast<int32_t>(offsetof(JsonWriter_t1927598499, ___hexSeq_4)); }
	inline CharU5BU5D_t1328083999* get_hexSeq_4() const { return ___hexSeq_4; }
	inline CharU5BU5D_t1328083999** get_address_of_hexSeq_4() { return &___hexSeq_4; }
	inline void set_hexSeq_4(CharU5BU5D_t1328083999* value)
	{
		___hexSeq_4 = value;
		Il2CppCodeGenWriteBarrier(&___hexSeq_4, value);
	}

	inline static int32_t get_offset_of_indentation_5() { return static_cast<int32_t>(offsetof(JsonWriter_t1927598499, ___indentation_5)); }
	inline int32_t get_indentation_5() const { return ___indentation_5; }
	inline int32_t* get_address_of_indentation_5() { return &___indentation_5; }
	inline void set_indentation_5(int32_t value)
	{
		___indentation_5 = value;
	}

	inline static int32_t get_offset_of_indentValue_6() { return static_cast<int32_t>(offsetof(JsonWriter_t1927598499, ___indentValue_6)); }
	inline int32_t get_indentValue_6() const { return ___indentValue_6; }
	inline int32_t* get_address_of_indentValue_6() { return &___indentValue_6; }
	inline void set_indentValue_6(int32_t value)
	{
		___indentValue_6 = value;
	}

	inline static int32_t get_offset_of_stringBuilder_7() { return static_cast<int32_t>(offsetof(JsonWriter_t1927598499, ___stringBuilder_7)); }
	inline StringBuilder_t1221177846 * get_stringBuilder_7() const { return ___stringBuilder_7; }
	inline StringBuilder_t1221177846 ** get_address_of_stringBuilder_7() { return &___stringBuilder_7; }
	inline void set_stringBuilder_7(StringBuilder_t1221177846 * value)
	{
		___stringBuilder_7 = value;
		Il2CppCodeGenWriteBarrier(&___stringBuilder_7, value);
	}

	inline static int32_t get_offset_of_U3CPrettyPrintU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(JsonWriter_t1927598499, ___U3CPrettyPrintU3Ek__BackingField_8)); }
	inline bool get_U3CPrettyPrintU3Ek__BackingField_8() const { return ___U3CPrettyPrintU3Ek__BackingField_8; }
	inline bool* get_address_of_U3CPrettyPrintU3Ek__BackingField_8() { return &___U3CPrettyPrintU3Ek__BackingField_8; }
	inline void set_U3CPrettyPrintU3Ek__BackingField_8(bool value)
	{
		___U3CPrettyPrintU3Ek__BackingField_8 = value;
	}

	inline static int32_t get_offset_of_U3CValidateU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(JsonWriter_t1927598499, ___U3CValidateU3Ek__BackingField_9)); }
	inline bool get_U3CValidateU3Ek__BackingField_9() const { return ___U3CValidateU3Ek__BackingField_9; }
	inline bool* get_address_of_U3CValidateU3Ek__BackingField_9() { return &___U3CValidateU3Ek__BackingField_9; }
	inline void set_U3CValidateU3Ek__BackingField_9(bool value)
	{
		___U3CValidateU3Ek__BackingField_9 = value;
	}

	inline static int32_t get_offset_of_U3CTypeHintingU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(JsonWriter_t1927598499, ___U3CTypeHintingU3Ek__BackingField_10)); }
	inline bool get_U3CTypeHintingU3Ek__BackingField_10() const { return ___U3CTypeHintingU3Ek__BackingField_10; }
	inline bool* get_address_of_U3CTypeHintingU3Ek__BackingField_10() { return &___U3CTypeHintingU3Ek__BackingField_10; }
	inline void set_U3CTypeHintingU3Ek__BackingField_10(bool value)
	{
		___U3CTypeHintingU3Ek__BackingField_10 = value;
	}

	inline static int32_t get_offset_of_U3CHintTypeNameU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(JsonWriter_t1927598499, ___U3CHintTypeNameU3Ek__BackingField_11)); }
	inline String_t* get_U3CHintTypeNameU3Ek__BackingField_11() const { return ___U3CHintTypeNameU3Ek__BackingField_11; }
	inline String_t** get_address_of_U3CHintTypeNameU3Ek__BackingField_11() { return &___U3CHintTypeNameU3Ek__BackingField_11; }
	inline void set_U3CHintTypeNameU3Ek__BackingField_11(String_t* value)
	{
		___U3CHintTypeNameU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier(&___U3CHintTypeNameU3Ek__BackingField_11, value);
	}

	inline static int32_t get_offset_of_U3CHintValueNameU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(JsonWriter_t1927598499, ___U3CHintValueNameU3Ek__BackingField_12)); }
	inline String_t* get_U3CHintValueNameU3Ek__BackingField_12() const { return ___U3CHintValueNameU3Ek__BackingField_12; }
	inline String_t** get_address_of_U3CHintValueNameU3Ek__BackingField_12() { return &___U3CHintValueNameU3Ek__BackingField_12; }
	inline void set_U3CHintValueNameU3Ek__BackingField_12(String_t* value)
	{
		___U3CHintValueNameU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier(&___U3CHintValueNameU3Ek__BackingField_12, value);
	}

	inline static int32_t get_offset_of_U3CTextWriterU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(JsonWriter_t1927598499, ___U3CTextWriterU3Ek__BackingField_13)); }
	inline TextWriter_t4027217640 * get_U3CTextWriterU3Ek__BackingField_13() const { return ___U3CTextWriterU3Ek__BackingField_13; }
	inline TextWriter_t4027217640 ** get_address_of_U3CTextWriterU3Ek__BackingField_13() { return &___U3CTextWriterU3Ek__BackingField_13; }
	inline void set_U3CTextWriterU3Ek__BackingField_13(TextWriter_t4027217640 * value)
	{
		___U3CTextWriterU3Ek__BackingField_13 = value;
		Il2CppCodeGenWriteBarrier(&___U3CTextWriterU3Ek__BackingField_13, value);
	}
};

struct JsonWriter_t1927598499_StaticFields
{
public:
	// System.Globalization.NumberFormatInfo LitJson.JsonWriter::numberFormat
	NumberFormatInfo_t104580544 * ___numberFormat_0;

public:
	inline static int32_t get_offset_of_numberFormat_0() { return static_cast<int32_t>(offsetof(JsonWriter_t1927598499_StaticFields, ___numberFormat_0)); }
	inline NumberFormatInfo_t104580544 * get_numberFormat_0() const { return ___numberFormat_0; }
	inline NumberFormatInfo_t104580544 ** get_address_of_numberFormat_0() { return &___numberFormat_0; }
	inline void set_numberFormat_0(NumberFormatInfo_t104580544 * value)
	{
		___numberFormat_0 = value;
		Il2CppCodeGenWriteBarrier(&___numberFormat_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
