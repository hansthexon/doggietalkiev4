﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Linq.Expressions.ConstantExpression
struct ConstantExpression_t305952364;
// System.Object
struct Il2CppObject;
// System.Type
struct Type_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Type1303803226.h"

// System.Void System.Linq.Expressions.ConstantExpression::.ctor(System.Object,System.Type)
extern "C"  void ConstantExpression__ctor_m1469913530 (ConstantExpression_t305952364 * __this, Il2CppObject * ___value0, Type_t * ___type1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Linq.Expressions.ConstantExpression::get_Value()
extern "C"  Il2CppObject * ConstantExpression_get_Value_m176732788 (ConstantExpression_t305952364 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
