﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MediaPlayerFullScreenCtrl
struct MediaPlayerFullScreenCtrl_t2458194295;

#include "codegen/il2cpp-codegen.h"

// System.Void MediaPlayerFullScreenCtrl::.ctor()
extern "C"  void MediaPlayerFullScreenCtrl__ctor_m3723294164 (MediaPlayerFullScreenCtrl_t2458194295 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MediaPlayerFullScreenCtrl::Start()
extern "C"  void MediaPlayerFullScreenCtrl_Start_m2620462952 (MediaPlayerFullScreenCtrl_t2458194295 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MediaPlayerFullScreenCtrl::Update()
extern "C"  void MediaPlayerFullScreenCtrl_Update_m3880956639 (MediaPlayerFullScreenCtrl_t2458194295 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MediaPlayerFullScreenCtrl::Resize()
extern "C"  void MediaPlayerFullScreenCtrl_Resize_m2709454366 (MediaPlayerFullScreenCtrl_t2458194295 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
