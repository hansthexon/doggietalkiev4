﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PolitePanelScript/<disabelle>c__Iterator2
struct U3CdisabelleU3Ec__Iterator2_t3251192131;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void PolitePanelScript/<disabelle>c__Iterator2::.ctor()
extern "C"  void U3CdisabelleU3Ec__Iterator2__ctor_m906354076 (U3CdisabelleU3Ec__Iterator2_t3251192131 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PolitePanelScript/<disabelle>c__Iterator2::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CdisabelleU3Ec__Iterator2_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m4102766078 (U3CdisabelleU3Ec__Iterator2_t3251192131 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PolitePanelScript/<disabelle>c__Iterator2::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CdisabelleU3Ec__Iterator2_System_Collections_IEnumerator_get_Current_m1262864662 (U3CdisabelleU3Ec__Iterator2_t3251192131 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PolitePanelScript/<disabelle>c__Iterator2::MoveNext()
extern "C"  bool U3CdisabelleU3Ec__Iterator2_MoveNext_m4256429104 (U3CdisabelleU3Ec__Iterator2_t3251192131 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PolitePanelScript/<disabelle>c__Iterator2::Dispose()
extern "C"  void U3CdisabelleU3Ec__Iterator2_Dispose_m2664885973 (U3CdisabelleU3Ec__Iterator2_t3251192131 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PolitePanelScript/<disabelle>c__Iterator2::Reset()
extern "C"  void U3CdisabelleU3Ec__Iterator2_Reset_m1567836487 (U3CdisabelleU3Ec__Iterator2_t3251192131 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
